			1. сборка проекта
1. для сборки проекта используем gulp. Запускать из директироии bin. Таски разделены для удобного просмотра
2. по умолчанию для запуска используем стандартную команду gulp [default]
3. таски по умолчанию - build, watch, webserver, node
4. необходимые приложения: postgresql, node.js, gulp global(npm install gulp -g), firefox(таск настроен для просмотра в лисе, но можно поменять
   в таске bin/task/webserver) ["google chrome", "firefox"]

			2. сервер
1. для загрузки в переменные среды используется плагин dotenv, который подгружает переменные из файла .env, который должен находиться в корневой директории
2. для аутентификации используется jwt - аутентификация.
3. Для хранения паролей используем модуль Bcrypt
4. проверка входных данных на соответствие формату модулем joi