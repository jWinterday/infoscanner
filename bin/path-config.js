module.exports = {
  "clean": {
    "dest": "../build/"
  },
  
  
  //public
  "views": {
    "src": ["../src/**/*.pug"],
    "dest": "../build/"
  },
  
  "styles": {
    "src": ["../src/public/**/*.css"],
    "dest": "../build/public/"
  },
  
  "sass": {
    "src": ["../src/public/**/*.scss"],
    "dest": "../src/public"
    //"dest": "../build/public/"
  },
  
  "browserify": {
    "dir": "../src/public",
    "src": "/app.jsx",
    //"watch": "/**/*.{jsx,css}",
    "watch": "/**/*.jsx",
    "dest": "../build/public"
  },
  
  "images": {
    "src": ["../src/public/**/*.{png,jpg,gif}"],
    "dest": "../build/public/"
  },

  "fonts": {
    "src": ["../src/**/*.ttf"],
    "dest": "../build/"
  },

  //server
  "server": {
    "src": ["../src/**/*.js",
      "!../src/public/**/*.*"],
    "entryPoint": "../build/server.js",
    "dest": "../build/"
  },
  
  "sql": {
    "src": ["../src/db/sql/**/*.sql"],
    "dest": "../build/db/sql/"
  }
};