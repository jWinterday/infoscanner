const gulp = require('gulp');
//const requireDir = require('require-dir');
//requireDir('./tasks', { recurse: true });

//build
require('./tasks/view');
require('./tasks/sass');
require('./tasks/browserify');
require('./tasks/image');
require('./tasks/font');
require('./tasks/server');
require('./tasks/sql');

require('./tasks/watch');
require('./tasks/webserver');
require('./tasks/node');

gulp.task('default', gulp.series(
  gulp.parallel(
    'view:build',
    'sass:build',
    'browserify:build',
    'image:build',
    'font:build',
    'server:build',
    'sql:build'
  ),

  gulp.series(
    'node',
    'webserver',
    'watch'
  )
));

gulp.task('onlywatch', gulp.series(
  gulp.parallel(
    'view:build',
    'sass:build',
    'browserify:build',
    'image:build',
    'font:build',
    'server:build',
    'sql:build'
  ),

  gulp.series(
    'node',
    //'webserver',
    'watch'
  )
));