const debounce = (fn, delay=300) => {
  let timeoutID = null;

  return () => {
    clearTimeout(timeoutID);

    timeoutID = setTimeout(() => {
      fn.apply(this, arguments);
    }, delay);
  };
}

module.exports = debounce;