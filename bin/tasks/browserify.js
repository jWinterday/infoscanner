const gulp = require('gulp');
const path = require('path');
const chalk = require('chalk');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const glob = require('glob');
const rename = require('gulp-rename');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('browserify task...'));

    const files = pathConfig.browserify.dir + pathConfig.browserify.src;

    glob.sync(files).map((file) => {
      const dirRelative = path.dirname(file).replace(pathConfig.browserify.dir, '');
      const fileName = path.basename(file);
      const params = {
        entries: file,
        extensions: ['.jsx'],
        debug: false,
        transform: [['browserify-css']]
      };

      return browserify(params)
        .transform('babelify', {
          presets: ['es2015', 'react'],
          plugins: ['transform-class-properties']
        })
        .bundle()
        .on('error', (err) => {
          console.info(chalk.green('error browserify task: ', err));
          reject(err);
        })
        .pipe(source(fileName))
        .pipe(rename(path.basename(fileName, '.jsx') + '.min.js'))
        .pipe(gulp.dest(pathConfig.browserify.dest + dirRelative))
        .pipe(reload({stream: true}));
    });

    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(chalk.green('browserify task...OK\n'));

    resolve();
  });
};

const task = gulp.task('browserify:build', func);

module.exports = {
  func,
  task
};