const gulp = require('gulp');
const uglifyes = require('gulp-uglify-es').default;
const rename = require('gulp-rename');
const jshint = require('gulp-jshint');
const rigger = require('gulp-rigger');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

const plumber = require('gulp-plumber');

const pathConfig = require('../path-config');

const javascript = () => {
  gulp.src(pathConfig.javascripts.src)
      //.pipe(plumber())
      .pipe(rigger())
      //.pipe(uglifyes())
      //.pipe(jshint({indent: 2}))
      //.pipe(jshint.reporter('default'))
      //.pipe(rename((path) => {
      //  path.basename = path.basename + '.min';
      //}))
      .pipe(gulp.dest(pathConfig.javascripts.dest))
      .pipe(reload({stream: true}));
};

gulp.task('javascript:build', javascript);
module.exports = javascript;