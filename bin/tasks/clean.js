const gulp = require('gulp');
const rimraf = require('rimraf');

const pathConfig = require('../path-config');

const clean = (cb) => {
  rimraf(pathConfig.clean.dest, cb);
};

gulp.task('clean', clean);
module.exports = clean;