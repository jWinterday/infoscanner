const gulp = require('gulp');
const chalk = require('chalk');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('font task...'));

    gulp.src(pathConfig.fonts.src)
      .pipe(gulp.dest(pathConfig.fonts.dest))
      .pipe(reload({stream: true}));

    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(chalk.green('font task...OK\n'));

    resolve();
  });
};

const task = gulp.task('font:build', func);

module.exports = {
  func,
  task
};