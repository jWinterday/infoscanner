const gulp = require('gulp');

gulp.task('build', [
  'view:build',
  'sass:build',
  'browserify:build',
  'image:build',
  'font:build',
  
  'server:build',
  'sql:build'
]);