const gulp = require('gulp');
const watch = require('gulp-watch');
const events = require('../events');
const chalk = require('chalk');

const debounce = require('../debounce');
const pathConfig = require('../path-config');

const sql = require('./sql');
const view = require('./view');
const sass = require('./sass');
const br = require('./browserify');
const image = require('./image');
const font = require('./font');
const server = require('./server');

const brReload = debounce(() => {
  br.func();
});
const backendReload = debounce(() => {
    events.emit('server', 'restart');
});


const w = gulp.task('watch', () => {
  //frontend
  watch(pathConfig.views.src, debounce(() => {
    view.func();
  }));
  watch(pathConfig.sass.src, () => {
    sass.func().then(data => {
      brReload();
    });
  });
  watch(pathConfig.browserify.dir + pathConfig.browserify.watch, () => {
    brReload();
  });
  watch(pathConfig.images.src, debounce(() => {
    image.func();
  }));
  watch(pathConfig.fonts.src, debounce(() => {
    font.func();
  }));

  //backend
  watch(pathConfig.server.src, debounce(() => {
    server.func().then(data => {
      backendReload();
    });
  }));
  watch(pathConfig.sql.src, debounce(() => {
    sql.func().then(data => {
      backendReload();
    });
  }));
});

module.exports = w;