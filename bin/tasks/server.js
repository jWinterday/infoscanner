const gulp = require('gulp');
const chalk = require('chalk');
const uglifyes = require('gulp-uglify-es').default;
//const rename = require('gulp-rename');
//const jshint = require('gulp-jshint');
const rigger = require('gulp-rigger');
const plumber = require('gulp-plumber');
//const sourcemaps = require('gulp-sourcemaps');

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('server task...'));

    gulp.src(pathConfig.server.src)
      .pipe(plumber())
      .pipe(rigger())
      .pipe(uglifyes())
      /*.pipe(jshint({
        esversion: 6,
        maxparams: 6,
        varstmt: true,
        boss: true
        //undef: true,
        //unused: true
      }))
      .pipe(jshint.reporter('default'))*/
      //.pipe(rename((path) => {
      //  path.basename = path.basename + '.min';
      //}))
      .pipe(gulp.dest(pathConfig.server.dest));

    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(chalk.green('server task...OK\n'));

    resolve();
  });
};

const task = gulp.task('server:build', func);

module.exports = {
  func,
  task
};