const gulp = require('gulp');
const cssmin = require('gulp-minify-css');
const prefixer = require('gulp-autoprefixer');
const rigger = require('gulp-rigger');
const browserSync = require("browser-sync");
const reload = browserSync.reload;
const rename = require('gulp-rename');

const pathConfig = require('../path-config');

const css = () => {
  gulp.src(pathConfig.styles.src)
      .pipe(rigger())
      .pipe(prefixer())//вендорные префиксы
      .pipe(cssmin())//сожмем
      .pipe(rename((path) => {
        path.basename = path.basename + '.min';
      }))
      .pipe(gulp.dest(pathConfig.styles.dest))
      .pipe(reload({stream: true}));
};

gulp.task('css:build', css);
module.exports = css;