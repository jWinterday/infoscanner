const gulp = require('gulp');
const sass = require('gulp-sass');
const chalk = require('chalk');
//const browserSync = require("browser-sync");
//const reload = browserSync.reload;
const rename = require('gulp-rename');

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('sass task...'));

    gulp.src(pathConfig.sass.src)
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      //.pipe(sass().on('error', sass.logError))
      .pipe(rename((path) => {
        path.basename = path.basename + '.min';
      }))
      .pipe(gulp.dest(pathConfig.sass.dest));

    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(chalk.green('sass task...OK\n'));

    resolve();
  });
};

const task = gulp.task('sass:build', func);

module.exports = {
  func,
  task
};