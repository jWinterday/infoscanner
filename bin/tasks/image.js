const gulp = require('gulp');
const chalk = require('chalk');
const imagemin = require('gulp-imagemin');
const browserSync = require("browser-sync");
const reload = browserSync.reload;
const rename = require('gulp-rename');

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('image task...'));

    gulp.src(pathConfig.images.src)
      .pipe(imagemin([imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.gifsicle({interlaced: true})
      ]))
      .pipe(rename((path) => {
        path.basename = path.basename + '.min';
      }))
      .pipe(gulp.dest(pathConfig.images.dest))
      .pipe(reload({stream: true}));

    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(chalk.green('image task...OK\n'));

    resolve();
  });
};

const task = gulp.task('image:build', func);

module.exports = {
  task,
  func
};