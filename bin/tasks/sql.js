const gulp = require('gulp');
const chalk = require('chalk');
//const browserSync = require("browser-sync");

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('sql task...'));

    gulp.src(pathConfig.sql.src)
      .pipe(gulp.dest(pathConfig.sql.dest));

    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(chalk.green('sql task...OK\n'));

    resolve();
  });
}

const task = gulp.task('sql:build', func);

module.exports = {
  func,
  task
};