const gulp = require('gulp');
const chalk = require('chalk');
const browserSync = require('browser-sync');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('webserver task...'));

    browserSync.init(null, {
      proxy: 'localhost:3001',
      port: 4234,
      browser: 'firefox',//["chrome", "firefox"]
      logLevel: 'info'//'debug',
    });

    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(chalk.green('webserver task...OK\n'));

    resolve();
  });
};

const task = gulp.task('webserver', func);

module.exports = {
  func,
  task
};