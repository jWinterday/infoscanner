const gulp = require('gulp');
const chalk = require('chalk');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

const pathConfig = require('../path-config');

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('view task...'));

    gulp.src(pathConfig.views.src)
      .pipe(gulp.dest(pathConfig.views.dest))
      .pipe(reload({stream: true}));

    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    process.stdout.write(chalk.green('view task...OK\n'));

    resolve();
  });
};

const task = gulp.task('view:build', func);

module.exports = {
  func,
  task
};