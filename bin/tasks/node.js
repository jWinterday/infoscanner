const gulp = require('gulp');
const chalk = require('chalk');
const nodemon = require('gulp-nodemon');
//const notify = require('gulp-notify');
//const livereload = require('gulp-livereload');
const browserSync = require('browser-sync');
const reload = browserSync.reload;

const debounce = require('../debounce');
const pathConfig = require('../path-config');
const events = require('../events');

let nm;

events.on('server', (m) => {
  nm.emit('restart');
});

const func = () => {
  return new Promise((resolve, reject) => {
    process.stdout.write(chalk.green('node task...\n'));
    //livereload.listen();

    nm = nodemon({
      script: pathConfig.server.entryPoint,
      ext: 'js sql',
      //watch: [pathConfig.server.entryPoint, pathConfig.sql.dest]
    }).on('restart', debounce(() => {
      process.stdout.write(chalk.green('node task...\n'));
      //reload({stream: true});
    }))
    .on('start', debounce(() => {
      setTimeout(() => {
        gulp.src(pathConfig.server.entryPoint)
          .pipe(reload({stream: true}));

        process.stdout.clearLine()
        process.stdout.cursorTo(0)
        process.stdout.write(chalk.green('node task...OK\n'));

        resolve();
      }, 2000);

    }));
  });
};

const task = gulp.task('node', func);

module.exports = {
  func,
  task
};