const bunyan = require('bunyan');

const log = bunyan.createLogger({
  name: 'InfoScanner',
  streams: [
    {
      level: 'info',
      stream: process.stdout// log INFO and above to stdout
    },
    {
      level: 'error',
      path: '../tmp/server.log'//log ERROR and above to a file
    }
  ],
  serializers: {
    req: bunyan.stdSerializers.req,
    res: bunyan.stdSerializers.res
  }
});

module.exports = log;