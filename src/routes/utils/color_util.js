const Jimp = require('jimp');


const optimalDeltaE = 2.3;

const getDeltaE = (lab1, lab2) => {
  return Math.sqrt((lab1.L-lab2.L)**2 + (lab1.A-lab2.A)**2 + (lab1.B-lab2.B)**2);
};

const findNearestPaletteItem = (lab, palette) => {
  let row;
  let deltaE;
  let bestDeltaE = 1000000;

  for (let id in palette) {
    deltaE = getDeltaE(lab, palette[id].lab);

    if (deltaE < bestDeltaE) {
      bestDeltaE = deltaE;
      row = palette[id];
    }

    if (bestDeltaE <= optimalDeltaE) {
      break;
    }
  }

  return row;
};

const convertRGBAtoLAB = (rgba, bg={R: 255, G: 255, B: 255, A: 255}) => {
  //RGBA -> RGB
  let pAlpha = (rgba.A ? rgba.A : 255) / 255;
  let pR = rgba.R;
  let pG = rgba.G;
  let pB = rgba.B;

  pR = (1 - pAlpha) * bg.R + pR * pAlpha;
  pG = (1 - pAlpha) * bg.G + pG * pAlpha;
  pB = (1 - pAlpha) * bg.B + pB * pAlpha;

  //normalize colors
  pR = pR / 255.0;
  pG = pG / 255.0;
  pB = pB / 255.0;

  //convert to xyz
  pR = (pR > 0.04045) ? ((pR + 0.055) / 1.055)**2.4 : pR / 12.92;
  pG = (pG > 0.04045) ? ((pG + 0.055) / 1.055)**2.4 : pG / 12.92;
  pB = (pB > 0.04045) ? ((pB + 0.055) / 1.055)**2.4 : pB / 12.92;

  let x = (pR * 0.4124 + pG * 0.3576 + pB * 0.1805) * 100;
  let y = (pR * 0.2126 + pG * 0.7152 + pB * 0.0722) * 100;
  let z = (pR * 0.0193 + pG * 0.1192 + pB * 0.9505) * 100;

  //convert to lab
  const deltaLab = 0.008856;

  x = (x / 95.047);
  y = (y / 100.0);
  z = (z / 108.883);

  x = (x > deltaLab) ? Math.cbrt(x) : 7.787 * x + (16.0 / 116.0);
  y = (y > deltaLab) ? Math.cbrt(y) : 7.787 * y + (16.0 / 116.0);
  z = (z > deltaLab) ? Math.cbrt(z) : 7.787 * z + (16.0 / 116.0);

  return {
    L: ((116.0 * y) - 16.0).toFixed(3),
    A: (500.0 * (x - y)).toFixed(3),
    B: (200.0 * (y - z)).toFixed(3)
  };
};

const getAvgColor = (x0, y0, w, h, image) => {
  let R = 0;
  let G = 0;
  let B = 0;
  let A = 0;

  image.scan(x0, y0, w, h, (x, y, idx) => {
    R += image.bitmap.data[idx + 0];
    G += image.bitmap.data[idx + 1];
    B += image.bitmap.data[idx + 2];
    A += image.bitmap.data[idx + 3];
  });

  const cnt = w * h;
  R = Math.ceil(R / cnt);
  G = Math.ceil(G / cnt);
  B = Math.ceil(B / cnt);
  A = Math.ceil(A / cnt);

  return {
    R: R,
    G: G,
    B: B,
    A: A
  };
};

const setAvgColor = (x0, y0, w, h, image, isFull=true, color={R:0, G:0, B:0, A:255}) => {
  const { R, G, B, A } = color;

  const colorInt = Jimp.rgbaToInt(R, G, B, A || 255, () => {});

  const nextX0 = x0;//isFull ? x0 : x0 + 1;
  const nextY0 = y0;//isFull ? y0 : y0 + 1;
  const nextW = isFull ? w : w - 1;
  const nextH = isFull ? h : h - 1;

  return new Promise((resolve, reject) => {
    image.scan(nextX0, nextY0, nextW, nextH, (x, y, idx) => {
      image.setPixelColor(colorInt, x, y);
      resolve(image);
    });
  });
  /*image.scan(x0, y0, w, h, (x, y, idx) => {
    image.setPixelColor(colorInt, x, y);
    return image;
  });*/
};

const createImage = (w, h, coordinates=[], color=0x0) => {
  return new Promise((resolve, reject) => {
    new Jimp(w, h, color, (err, img) => {
      if (err) { reject(err); }

      resolve(img);
    });
  });
}

const calcPreviewImageAsync = (image, compress, palette) => {
  const coordinatesInfo = {};

  return new Promise(async (resolve, reject) => {
    try {
      for (let y = 0; y < image.bitmap.height - compress; y += compress) {
        for (let x = 0; x < image.bitmap.width - compress; x += compress) {
          const avgColor = getAvgColor(x, y, compress, compress, image);
          const lab = convertRGBAtoLAB(avgColor);
          const nearest_dmc = findNearestPaletteItem(lab, palette);
          const diyResourceId = nearest_dmc.diy_resource_id;

          let info;
          if (coordinatesInfo.hasOwnProperty(diyResourceId)) {
            const item = coordinatesInfo[diyResourceId];
            const coordinates = item.coordinates;
            const posArr = [...coordinates, ...[{ x: x, y: y }]];
            info = {
              coordinates: posArr,
              cnt: posArr.length,
              color: nearest_dmc.color,
              diy_resource_id: nearest_dmc.diy_resource_id,
              no: nearest_dmc.no,
              name: nearest_dmc.name
            };
          } else {
            info = {
              coordinates: [{ x: x, y: y }],
              cnt: 1,
              color: nearest_dmc.color,
              no: nearest_dmc.no,
              name: nearest_dmc.name
            };
          }
          coordinatesInfo[diyResourceId] = info;

          await setAvgColor(x, y, compress, compress, image, true, nearest_dmc.color);
        }
      }

      await createGridAsync(image, compress);

      const nextImage64 = await image.getBase64Async(Jimp.MIME_JPEG);//MIME_PNG);//Jimp.AUTO);

      resolve({
        nextImage64,
        coordinatesInfo
      });
    } catch (err) {
      reject(err);
    }
  });
}

const createSchemaAsync = (image, colors, compress) => {
  let imagesInfo = {};

  return new Promise(async (resolve, reject) => {
    try {
      for (let index in  colors) {
        const color = colors[index];
        const {diy_resource_id, coordinates} = color;

        const img = await createImage(image.bitmap.width, image.bitmap.height);
        coordinates.map(item => {
          setAvgColor(item.x, item.y, compress, compress, img, false);
        });
        //await img.background(0xffffff);
        //img.quality(1);
        //img.background(0x00000000);
        //img.background(0xFFFFFFFF);

        //await createGridAsync(img, compress);

        const img64 = await img.getBase64Async(Jimp.MIME_PNG);//MIME_JPEG);//MIME_PNG);//Jimp.AUTO);
        imagesInfo[diy_resource_id] = img64;
      }

      resolve(imagesInfo);
    } catch (err) {
      reject(err);
    }
  });
}

const createGridAsync = (image, compress, color={R:255,G:0,B:0}, count=10) => {
  return new Promise(async (resolve, reject) => {
    try {
      for (let y = 0; y < image.bitmap.height - compress; y += compress) {
        for (let x = 0; x < image.bitmap.width - compress; x += compress) {
          if (x % count === 0) {
            await setAvgColor(x * compress - 1, 0, 1, image.bitmap.height, image, true, color);
          }
        }
        if (y % count === 0) {
          await setAvgColor(0, y * compress - 1, image.bitmap.width, 1, image, true, color);
        }
      }

      resolve();
    } catch(err) {
      reject(err);
    }
  });
}

module.exports = {
  optimalDeltaE,
  getDeltaE,
  findNearestPaletteItem,
  convertRGBAtoLAB,
  getAvgColor,
  setAvgColor,

  calcPreviewImageAsync,
  createSchemaAsync
};