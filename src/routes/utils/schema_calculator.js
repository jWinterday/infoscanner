const Jimp = require('jimp');

const db = require('../../db');
const targetDir = process.env.TARGET_DIR || '../build';
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});

const colorUtil = require('../utils/color_util');

const createPreview = (params={}) => {
  const method = params.calculate_method;
  const compress = params.compress_value;
  const imageData64 = params.image_data;
  //const userId = params.user_id;
  //const projectId = params.project_id;
  const resourceFileId = params.resource_file_id;
  //const countId = params.schema_tune_id;
  //const realWidth = params.real_width;
  //const realHeight = params.real_height;

  let algTimeStart;
  let algTimeEnd;

  return new Promise(async (resolve, reject) => {
    if (!['compress', 'size'].includes(method)) { return reject('wrong method'); };
    if (!imageData64) { return reject('image data is null'); };
    if (compress <= 1) { return reject('compress value must be > 1'); }

    const imageDataUrl = imageData64.replace(/^data:image\/\w+;base64,/, '');
    const imageBuffer = Buffer.from(imageDataUrl, 'base64');

    try {
      const dmcColors = await db.query(sql.getDMCColorsJSON({}));//_getPaletteAsync();//, _getImageAsync(imageData64)]);
      const palette = (((dmcColors || {}).rows || [])[0] || {}).colors;
      const image = await Jimp.read(imageBuffer);

      algTimeStart = process.hrtime();
      const calcResult = await colorUtil.calcPreviewImageAsync(image, compress, palette);
      algTimeEnd = process.hrtime(algTimeStart);

      resolve({
        algTime: (algTimeEnd[0] + algTimeEnd[1]/1000000/1000).toFixed(1),
        nextImage64: calcResult.nextImage64,
        coordinatesInfo: calcResult.coordinatesInfo
      });
    } catch(err) {
      reject((err || '').toString());
    }
  });
};

const createSchema = async (params={}) => {
  const { img64, colors, compress } = params;

  let algTimeStart;
  let algTimeEnd;

  return new Promise(async (resolve, reject) => {
    if (!img64) { return reject('image data is null'); };
    if (!Number.isInteger(compress)) { return reject('compress value is not valid'); }

    algTimeStart = process.hrtime();

    const imageDataUrl = img64.replace(/^data:image\/\w+;base64,/, '');
    const imageBuffer = Buffer.from(imageDataUrl, 'base64');

    const image = await Jimp.read(imageBuffer);
    const calcResult = await colorUtil.createSchemaAsync(image, colors, compress);

    algTimeEnd = process.hrtime(algTimeStart);

    resolve({
      algTime: (algTimeEnd[0] + algTimeEnd[1]/1000000/1000).toFixed(1),
      calcResult
    });
  });
}

module.exports = {
  createPreview,
  createSchema
};