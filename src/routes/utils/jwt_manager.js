const jwt = require('jsonwebtoken');

const token = (payload, ttl = process.env.JWT_TOKEN_TTL_MS, secret = process.env.SECRET_KEY) => {
  return jwt.sign(payload, secret, {
    expiresIn: ttl
  });
};

const refreshToken = (payload) => {
  return token(payload,
               process.env.JWT_REFRESH_TOKEN_TTL_MS || 259200,
               process.env.REFRESH_TOKEN_SECRET_KEY);
};

const decode = (token) => {
  return jwt.decode(token);
};

const setClientData = (req, res, next, token) => {
  const settings = {
    maxAge: parseInt(process.env.COOKIE_TTL_MS),
    httpOnly: true//httpOnly = true -> can't read with document.cookie
  };
  
  
  //res.header('authorization', resJSON.token);
  res.cookie("token", token, settings);
};


module.exports = {
  token: token,
  refreshToken: refreshToken,
  decode: decode,
  setClientData: setClientData
};