const joi = require('joi');

//minimum six characters, at least one uppercase letter, one lowercase letter, one number and one special character
const emailSchema = joi.string()
        .email({ minDomainAtoms: 2 })
        .required();

const passwordWithoutCheckSchema = joi.string()
        .required()
        .error(new Error('Enter password'));

const passwordSchema = joi.string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}/)
        .required()
        .error(new Error('Password must contains minimum six characters, at least one uppercase letter, one lowercase letter, one number and one special character'));

const confirmPasswordSchema = joi.string()
        .required()
        .valid(joi.ref('password'))
        .error(new Error('Passwords do not match'));
        //.options({ language: { any: { allowOnly: 'Passwords do not match' } } });

const recaptchaSchema = joi.string();
        //.min(1)
        //.required();

//update user
const userName = joi.string().alphanum().max(15);

//----
const loginSchema = joi.object().keys({
  email: emailSchema,
  password: passwordWithoutCheckSchema
});

const registerSchema = joi.object().keys({
  email: emailSchema,
  password: passwordSchema,
  confirm_password: confirmPasswordSchema,
  g_recaptcha_response: recaptchaSchema
});

const updateUserSchema = joi.object().keys({
  first_name: userName,
  last_name: userName
});

module.exports = {
  loginSchema: loginSchema,
  registerSchema: registerSchema,
  updateUserSchema: updateUserSchema
};