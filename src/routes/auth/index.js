const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const targetDir = process.env.TARGET_DIR || '../build';

const db = require('../../db');
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});
const jwtManager = require('../utils/jwt_manager');
const { checkLogin, checkRegister, checkUpdateUser } = require('../../middlewares/checkDataFormat');
const { captchaCheck, tokenCheck, refreshTokenCheck } = require('../../middlewares/authenticate');


const emulateLongResponse = async () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, 2000);
  });
};

router.post('/ajax/login/', [checkLogin, captchaCheck], async (req, res) => {
  console.info(req.body);
  const { email, password } = req.body;

  const queryParams = {
    email: email
  };

  try {
    const data = await db.query(sql.getUserByEmail(queryParams));
    if (!data.rowCount) { return res.status(404).send('user not found'); }

    const dbUser = data.rows[0];
    const matchUser = await bcrypt.compare(password, dbUser.password_hash);
    if (!matchUser) { return res.status(403).send('wrong password'); }

    const refreshToken = jwtManager.refreshToken({ email: email });
    delete dbUser.password_hash;
    delete dbUser.refresh_token;
    const token = jwtManager.token(dbUser);
    const updateParams = {
      rt: refreshToken,
      id: dbUser.user_id,
      lastIP: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    };
    const updateData = await db.query(sql.updateUserLogin(updateParams));
    if (!updateData.rowCount) { return res.json(500).send('can not connect to the DB'); }

    await emulateLongResponse();

    return res.json({
      success: true,
      message: 'successful login',
      token: token,
      refreshToken: refreshToken
    });
  } catch (err) {
    return res.status(500).send(err.toString());
  }
});

router.post('/ajax/refreshtoken/', [refreshTokenCheck, captchaCheck], async (req, res) => {
  const { payload, refreshToken } = req;
  const email = payload.email;

  const queryParams = {
    email: email,
    rt: refreshToken
  };

  try {
    const data = await db.query(sql.compareRefreshTokens(queryParams));
    const dbUser = data.rows[0];
    if (!dbUser.is_compared) { return res.status(401).send('different refresh tokens'); }

    const refreshToken = jwtManager.refreshToken({ email: email });
    const token = jwtManager.token(dbUser);
    const updateParams = {
      rt: refreshToken,
      id: dbUser.user_id,
      lastIP: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    };
    const updateData = await db.query(sql.updateUserLogin(updateParams));

    return res.json({
      success: true,
      message: 'successful update refresh token',
      token: token,
      refreshToken: refreshToken
    });
  } catch (err) {
    return res.status(500).send(err.toString());
  }
});

router.post('/ajax/register/', [checkRegister, captchaCheck], async (req, res) => {
  const { email, password } = req.body;

  const saltRounds = 10;

  try {
    const hash = await bcrypt.hash(password, saltRounds);
    const refreshToken = jwtManager.refreshToken({});
    const queryParams = {
      email: email,
      hash: hash,
      rt: refreshToken,
      ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    };
    const data = await db.query(sql.create(queryParams));
    const token = jwtManager.token(data.rows[0]);
    return res.json({
      success: true,
      message: 'user successfully created',
      token: token,
      refreshToken: refreshToken
    });
  } catch (err) {
    console.info(err);

    if (Number(err.code) === 23505) {
      return res.status(409).send('user with this email already exists');
    }
    return res.status(500).send('server error');
  }
});

router.post('/ajax/updateuser/', [tokenCheck(''), checkUpdateUser], async (req, res) => {
  const queryParams = {
    firstName: req.body.first_name,
    lastName: req.body.last_name,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.updateUserInfo(queryParams));
    const token = jwtManager.token(data.rows[0]);
    return res.json({
      success: true,
      message: 'user info successfully updated',
      token: token
    });
  } catch (err) {
    console.info(err);
    return res.status(500).send(err.toString());
  }
});

module.exports = router;