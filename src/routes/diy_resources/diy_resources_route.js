const express = require('express');
const router = express.Router();

const targetDir = process.env.TARGET_DIR || '../build';
const db = require('../../db');
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});
const { tokenCheck } = require('../../middlewares/authenticate');

router.post('/ajax/list', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    userId: req.userId,
    pageNo: req.body.page,
    pageSize: req.body.pageSize,
    isAll: req.body.isAll
  };

  try {
    const data = await db.query(sql.getDiyResourcesList(queryParams));
    return res.json(data);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/resources_list', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    val: req.body.val,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.getResourcesList(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/amount_type_list', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    val: req.body.val
  };

  try {
    const data = await db.query(sql.getAmountTypeList(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/get_amount_types', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = { };

  try {
    const data = await db.query(sql.getAmountTypes(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/update_amount_type', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    userId: req.userId,
    diyResourceId: req.body.diy_resource_id,
    amountTypeId: req.body.amount_type_id,
    usersDiyResourcesId: req.body.users_diy_resources_id
  };

  try {
    const data = await db.query(sql.updateAmountType(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/get_all_palette', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.getDMCColors(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/get_all_dmc', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    resourceTypeId: 1,
    searchVal: req.body.val
  };

  try {
    const data = await db.query(sql.getAllColorsByType(queryParams));
    return res.json(data.rows);
  } catch(e) {
    console.info(e);
    return res.status(500).send(e);
  }
});

router.post('/ajax/add_user_resource', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    userId: req.userId,
    diyResourceId: req.body.diy_resource_id,
    amountTypeId: req.body.amount_type_id
  };

  try {
    const data = await db.query(sql.addUserResource(queryParams));
    return res.json(data.rows[0]);
  } catch(err) {
    const message = (err.code === '23505') ? 'You have got already this color' : err;
    console.info(err);
    return res.status(500).send(message);
  }
});

module.exports = router;