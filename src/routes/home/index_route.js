const express = require('express');
const router = express.Router();

const logger = require('../../logger');

router.get('/', (req, res, next) => {
  res.render('index');
  //logger.error({a: [1,2,3,4]}, 'yo');
});

module.exports = router;