module.exports = {
  '/': './routes/home/index_route',
  '/auth': './routes/auth/',
  '/project': './routes/project/project_route',
  '/diy_resources': './routes/diy_resources/diy_resources_route',
  '/image_schema': './routes/image_schema/image_schema_route'
};