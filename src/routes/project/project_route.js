const express = require('express');
const router = express.Router();

const targetDir = process.env.TARGET_DIR || '../build';
const db = require('../../db');
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});
const { tokenCheck } = require('../../middlewares/authenticate');

/**
 * search format:
 * { page: 1, pageSize: 3, sorted: [ { id: 'begin_date', desc: true } ], filtered: [ { id: 'name', value: 'fff' } ] }
 */
//
router.post('/ajax/list', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    userId: req.userId,
    pageNo: req.body.page,
    pageSize: req.body.pageSize
  };

  try {
    const data = await db.query(sql.getAllProjectsByUserId(queryParams));
    return res.json(data);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

//***next generation***
router.post('/ajax/project_data', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.getProjectData(queryParams));
    return res.json(data.rows[0]);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

//***next generation***
router.post('/ajax/load_form_data', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.getLoadFormData(queryParams));
    return res.json(data.rows[0]);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

router.post('/ajax/info', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId,
    //isOriginal: req.body.isOriginal || false
  };

  try {
    const data = await db.query(sql.getProjectInfo(queryParams));

    return res.json(data.rows[0]);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

router.post('/ajax/update', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    projectId: req.body.project_id,
    name: req.body.name,
    note: req.body.note,
    active: req.body.active
  };

  try {
    const data = await db.query(sql.updateProject(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

router.post('/ajax/create', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    name: req.body.name,
    note: req.body.note,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.createProject(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});


module.exports = router;