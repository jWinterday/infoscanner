const express = require('express');
const router = express.Router();

const targetDir = process.env.TARGET_DIR || '../build';
const db = require('../../db');
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});
const { tokenCheck } = require('../../middlewares/authenticate');
const schemaCalculator = require('../utils/schema_calculator');


//***next generation***
router.post('/ajax/upload', [tokenCheck('project_edit')], async (req, res) => {
  const queryParams = {
    projectId: Number(req.body.projectId),
    userId: req.userId,
    originalName: req.body.originalName,
    imageData64: req.body.imageData64
  };

  try {
    const data = await db.query(sql.addResourceFile(queryParams));
    return res.json(data.rows[0]);
  } catch (err) {
    console.info('db err: ', err);
    return res.status(500).send(err.toString());
  }
});

//***next generation***
router.post('/ajax/image_schema_data', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId,
    withConvertedImage: true
  };

  try {
    const data = await db.query(sql.getImageSchemaData(queryParams));
    return res.json(data.rows[0]);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);//'create error. see log');
  }
});

//***next generation***
router.post('/ajax/schema_count', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = { };

  try {
    const data = await db.query(sql.getSchemaCount(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);
  }
});

//***next generation***
router.post('/ajax/calculate_preview_schema', tokenCheck('project_edit'), async (req, res) => {
  const { projectId, compress, countId, method, w, h } = req.body;

  try {
    //1 get info
    const queryParams = {
      projectId: projectId,
      isForPreview: true
    };
    const resourceFileInfo = await db.query(sql.getResourceFile(queryParams));
    const resourceFileInfoRow = resourceFileInfo.rows[0];

    //2 calculate image
    const calcParams = Object.assign(resourceFileInfoRow, {
      compress_value: compress,
      schema_tune_id: countId,
      calculate_method: method,
      real_width: w,
      real_height: h
    });
    const calcData = await schemaCalculator.createPreview(calcParams);

    //3 save
    const nextDbParams = {
      userId: req.userId,
      countId: countId,
      compress: compress,
      altTime: calcData.algTime,
      method: method,
      realWidth: w,
      realHeight: h,
      convertedImageData: calcData.nextImage64,
      dataInfo: calcData.coordinatesInfo,
      resourceFileId: resourceFileInfoRow.resource_file_id
    };
    await db.query(sql.saveConvertedFileInfo(nextDbParams));

    //4 return
    const returnParams = {
      projectId: projectId,
      userId: req.userId,
      withConvertedImage: true
    };
    const returnData = await db.query(sql.getImageSchemaData(returnParams));

    return res.json(returnData.rows[0]);
  } catch(err) {
    console.info(err);
    return res.status(500).send('calculate error: ' + err);
  };
});

//***next generation***
router.post('/ajax/create_schema', tokenCheck('project_edit'), async (req, res) => {
  const { projectId } = req.body;

  try {
    //1 get info
    const queryParams = {
      projectId: projectId
    };
    const resourceFileInfo = await db.query(sql.getConvertedImageInfo(queryParams));
    const resourceFileInfoRow = resourceFileInfo.rows[0];

    //2 calculate images
    const calcData = await schemaCalculator.createSchema(resourceFileInfoRow);

    //3 save
    const dbParams = {
      resourceFileId: resourceFileInfoRow.resource_file_id,
      calcResult: calcData.calcResult,
      algSchemaTimeSec: calcData.algTime
    };
    await db.query(sql.saveImageSchemaInfo(dbParams));

    //4 return
    const returnParams = {
      projectId: projectId,
      userId: req.userId,
      withConvertedImage: false
    };
    const returnData = await db.query(sql.getImageSchemaData(returnParams));

    return res.json(returnData.rows[0]);
  } catch(err) {
    console.info('err: ', err);
    return res.status(501).send('create schema error. ' + err);
  };
});

router.post('/ajax/get_data', tokenCheck('project_view'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    const data = await db.query(sql.getSchemaData(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);//'create error. see log');
  }
});

router.post('/ajax/set_schema_next_color', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    resourceFileDataId: req.body.resource_file_data_id,
    nextNesourceFileDataId: req.body.next_resource_file_data_id
  };

  try {
    const data = await db.query(sql.setSchemaNextColor(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err.toString());//'create error. see log');
  }
});

router.post('/ajax/restore_original', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    await db.query(sql.restoreOriginal(queryParams));
    const data = await db.query(sql.getSchemaData(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);//'create error. see log');
  }
});

router.post('/ajax/remove_unused', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    projectId: req.body.projectId,
    userId: req.userId
  };

  try {
    await db.query(sql.removeUnused(queryParams));
    const data = await db.query(sql.getSchemaData(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);//'create error. see log');
  }
});

router.post('/ajax/add_palette_color', tokenCheck('project_edit'), async (req, res) => {
  const queryParams = {
    userId: req.userId,
    projectId: req.body.projectId,
    resourceFileId: req.body.resourceFileId,
    diyResourceId: req.body.diyResourceId
  };

  try {
    const fd = await db.query(sql.addNewPaletteColor(queryParams));
    const data = await db.query(sql.getDMCColors(queryParams));
    return res.json(data.rows);
  } catch(err) {
    console.info(err);
    return res.status(500).send(err);//'create error. see log');
  }
});


module.exports = router;