-- create
-- create new user
insert into users(email, password_hash, refresh_token, last_login_ip)
values(:email, :hash, :rt, :ip)
returning user_id,
          email as full_name,
          refresh_token,
          get_user_roles(user_id) as roles

-- getUserByEmail
select u.user_id,
       u.email,
       u.first_name,
       u.last_name,
       u.password_hash,
       u.refresh_token,
       get_user_full_name(u) as full_name,
       get_user_roles(u.user_id) as roles
  from users u
 where u.email = :email
   and add_info.get_user_is_active(u) = true

-- updateUserLogin
update users
   set refresh_token = :rt,
       last_login_date = CURRENT_TIMESTAMP,
       last_login_ip = :lastIP
 where user_id = :id
returning *

-- updateUserInfo
with res as (update users
                set first_name = :firstName,
                    last_name = :lastName,
                    password_hash = coalesce(:passwordHash, password_hash)
              where user_id = :userId
          returning *)
select res.user_id,
       res.email,
       res.first_name,
       res.last_name,
       get_user_full_name(res) as full_name,
       get_user_roles(res.user_id) as roles
  from res

-- compareRefreshTokens
select u.refresh_token = :rt as is_compared,
       u.refresh_token db_rt,
       :rt as compared_rt,
       u.user_id,
       --u.email,
       u.first_name,
       u.last_name,
       get_user_full_name(u) as full_name,
       get_user_roles(u.user_id) as roles
  from users u
 where u.email = :email

 --closeUser
 update users
    set end_date = CURRENT_TIMESTAMP
  where email = :email