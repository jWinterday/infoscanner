-- getProjectData
-- ***next generation***
select p.name,
       exists(select 1 from resource_file rf where rf.project_id = p.project_id) as is_schema_exists
  from project p
 where p.project_id = :projectId
   and (p.administrator_user_id = :userId
        or
        :userId = (select user_id
                     from user_in_project
                    where project_id = :projectId)
       )

-- getLoadFormData
-- ***next generation***
select rf.original_name,
       rf.resource_file_id,
       rf.image_data
  from project p
  left join resource_file rf on rf.project_id = p.project_id
 where p.project_id = :projectId
   and (p.administrator_user_id = :userId
        or
        :userId = (select user_id
                     from user_in_project
                    where project_id = :projectId)
       )

-- getAllProjectsByUserId
with pData as (select (:userId)::integer as pUserId,
                      (:pageNo)::integer + 1 as pPageNo,--page index begins with 0
                      (:pageSize)::integer as pPageSize)
select q.project_id,
       q.name,
       q.begin_date,
       q.end_date,
       q.note,
       q.is_project_active,
       q.is_own_project as is_own_project,-- as is_editable,
       q.is_project_active,
       ceil(1.0 * q.row_count / q.pPageSize) as pages
  from (select d.*,
               p.project_id,
               p.name,
               to_char(p.begin_date, 'DD.MM.YYYY HH24-MI-SS') as begin_date,
               to_char(p.end_date, 'DD.MM.YYYY HH24-MI-SS') as end_date,
               p.note,
               p.is_own_project,
               p.is_project_active,
               row_number() over (order by p.project_id) as rn,
               count(*) over () as row_count
          from (select p.*,
                       p.administrator_user_id as filter_user_id,
                       true as is_own_project,
                       add_info.get_project_is_active(p) as is_project_active
                  from project p
                 union all
                select p.*,
                       uip.user_id as filter_user_id,
                       false as is_own_project,
                       add_info.get_project_is_active(p) as is_project_active
                  from project p,
                       user_in_project uip
                 where uip.project_id = p.project_id
               ) p,
               pData d
         where p.filter_user_id = d.pUserId
       )q
 where q.rn between ((q.pPageNo - 1) * q.pPageSize + 1)
                and (q.pPageNo * q.pPageSize)

-- getProjectInfo
select p.name,
       case
         when rf.alg_time_sec is not null then rf.original_name || '//' || rf.alg_time_sec || '(sec)'
         else rf.original_name
       end as original_name,
       rf.alg_time_sec,
       to_char(rf.begin_date, 'DD.MM.YYYY HH24-MI-SS') as begin_date,
       rf.resource_file_id,
       rf.schema_tune_id,
       rf.compress_value,
       rf.calculate_method,
       rf.real_width,
       rf.real_height,
       rf.image_data,
       rf.converted_image_data,--case when (isOriginal)::boolean = true then rf.image_data else rf.converted_image_data end as image_data,
       rf.is_calculated
  from project p
  left join resource_file rf on rf.project_id = p.project_id
 where p.project_id = :projectId

-- getProjectInfoWithColors
select p.name,
       case
         when rf.alg_time_sec is not null then rf.original_name || '//' || rf.alg_time_sec || '(sec)'
         else rf.original_name
       end as original_name,
       rf.alg_time_sec,
       to_char(rf.begin_date, 'DD.MM.YYYY HH24-MI-SS') as begin_date,
       rf.resource_file_id,
       rf.schema_tune_id,
       rf.compress_value,
       rf.calculate_method,
       rf.real_width,
       rf.real_height,
       rf.image_data,
       rf.converted_image_data,
       rf.is_calculated,
       (select json_agg(row_to_json(q))
          from (select rfd.diy_resource_id,
                       rfd.coordinates,--rfd.image_data,
                       dr.name,
                       dr.no,
                       dr.color,
                       (udr.diy_resource_id is not null) as in_my_palette
                  from public.resource_file_data rfd
                  join public.diy_resource dr on rfd.diy_resource_id = dr.diy_resource_id
                  left join users_diy_resources udr on udr.user_id = :userId and udr.diy_resource_id = rfd.diy_resource_id
                 where rfd.resource_file_id = rf.resource_file_id
                 order by jsonb_array_length(rfd.coordinates) desc) q
       ) as colors
  from project p
  left join resource_file rf on rf.project_id = p.project_id
 where p.project_id = :projectId

-- getProjectInfoTest
select p.name,
       rf.original_name,
       rf.alg_time_sec,
       to_char(rf.begin_date, 'DD.MM.YYYY HH24-MI-SS') as begin_date,
       rf.resource_file_id,
       rf.schema_tune_id,
       rf.compress_value,
       rf.calculate_method,
       rf.real_width,
       rf.real_height,
       rf.image_data,
       rf.converted_image_data,
       public.get_all_coordinates(rf.resource_file_id) as colors
  from project p
  left join resource_file rf on rf.project_id = p.project_id
 where p.project_id = :projectId

-- updateProject
update project
   set name = :name,
       note = :note,
       end_date = case
                    when :active = true then null
                    else CURRENT_TIMESTAMP
                  end
 where project_id = :projectId
returning *;

-- createProject
insert into project(name, note, administrator_user_id)
values (:name, :note, :userId)
returning *;