-- getResourceFile
-- ***next generation***
select rf.original_name,
       rf.resource_file_id,--begin_date,user_id,project_id,alg_time_sec,
       rf.schema_tune_id,
       rf.compress_value,
       rf.with_optimization,
       rf.calculate_method,
       rf.real_width,
       rf.real_height,
       case
         when (:isForPreview)::boolean then rf.image_data
         else rf.converted_image_data
       end as image_data,
       rf.is_calculated
  from public.resource_file rf
 where rf.project_id = :projectId

-- getConvertedImageInfo
select rf.resource_file_id,
       rf.compress_value as compress,
       rf.converted_image_data as img64,
       (select json_agg(row_to_json(q))
          from (select rfd.diy_resource_id,
                       rfd.coordinates
                  from public.resource_file_data rfd
                 where rfd.resource_file_id = rf.resource_file_id) q
       ) as colors
  from public.resource_file rf
 where rf.project_id = :projectId

-- getDiyResourcesList
select q.*,
       ceil(1.0 * q.row_count / :pageSize) as pages
  from (select udr.users_diy_resources_id,
               dr.diy_resource_id,
               dr.name as diy_resource_name,
               dr.color as diy_resource_color,
               dr.no,
               case when dr.no~E'^\\d+$' then (dr.no::integer) else -1 end as dr_parsed_no,
               udr.amount_type_id,
               amt.name as amount_type_name,
               amt.color as amount_color,
               rt.name as resource_type_name,
               (select array_agg(drip_info.project_id) as projects
                  from diy_resource_in_project drip_info
                 where drip_info.diy_resource_id = dr.diy_resource_id) as used_in_project,
               row_number() over (order by udr.diy_resource_id) as rn,
               count(*) over () as row_count
          from public.diy_resource dr
          join public.resource_type rt on rt.resource_type_id = dr.resource_type_id
          left join public.users_diy_resources udr on udr.diy_resource_id = dr.diy_resource_id
                                                  and udr.user_id = :userId
          left join public.amount_type amt on amt.amount_type_id = udr.amount_type_id
         where (:isAll)::boolean = false and udr.users_diy_resources_id is not null
               or
               (:isAll)::boolean = true) q
 where q.rn between (:pageNo - 1) * :pageSize + 1 and :pageNo * :pageSize::integer
 order by q.dr_parsed_no

-- getAmountTypeList
select rt.amount_type_id as "value",
       rt.name as label,
       rt.is_default
  from public.amount_type rt
 where upper(rt.name) like concat(upper(:val), '%');

-- createUserResource
insert into public.diy_resource_in_project(project_id, diy_resource_id, amount_type_id, add_info)
values(:projectId, :diyResourceId, :amountTypeId, :addInfo)
returning diy_resource_in_project_id,
          diy_resource_id

-- updateUserResource
update public.diy_resource_in_project
  set diy_resource_id = :diyResourceId,
      amount_type_id = :amountTypeId,
      add_info = :addInfo
 where diy_resource_in_project_id = :diyResourceInProjectId
returning diy_resource_in_project_id,
          diy_resource_id,
          amount_type_id

-- addResourceFile
with data as (insert into public.resource_file (original_name, user_id, project_id, image_data)
              values (:originalName, :userId, :projectId, :imageData64)
                  on conflict (project_id)
                  do update set
                     original_name = excluded.original_name,
                     user_id = excluded.user_id,
                     calculate_method = excluded.calculate_method,
                     image_data = excluded.image_data,
                     begin_date = default,
                     schema_tune_id = default,
                     compress_value = default,
                     with_optimization = default,
                     alg_time_sec = default,
                     real_width = default,
                     real_height = default,
                     converted_image_data = default
                  returning *)
select *
  from data;

select rf.converted_image_data,
       json_agg(row_to_json(q)) as colors
  from (select rfd.diy_resource_id,
               rfd.coordinates
          from public.resource_file rf
               public.resource_file_data rfd
         where rfd.resource_file_id = rf.resource_file_id
           and rf.project_id = :projectId) q

-- getAllColorsByType
select dr.diy_resource_id,
       dr.name,
       dr.no,
       dr.color as diy_resource_color
  from public.diy_resource dr
 where dr.resource_type_id = :resourceTypeId
   and dr.no like concat('%', (:searchVal)::text, '%')

-- getDMCColors
with data as (select dr.diy_resource_id
                from public.diy_resource dr
               where dr.resource_type_id = 1
              except
              select rfd.diy_resource_id
                from public.resource_file rf,
                     public.resource_file_data rfd
               where rf.resource_file_id = rfd.resource_file_id
                 and rf.project_id = :projectId)
select dr.diy_resource_id,
       dr.color as diy_resource_color,
       dr.name,
       dr.no,
       (udr.users_diy_resources_id is not null) as in_my_palette
  from public.diy_resource dr
  join data on data.diy_resource_id = dr.diy_resource_id
  left join public.users_diy_resources udr on udr.diy_resource_id = dr.diy_resource_id and udr.user_id = :userId
 order by (dr.hsl->>'L')::real

-- getDMCColorsJSON
select json_object_agg(q.diy_resource_id, to_json(q)) as colors
  from (select dr.*
  				from public.diy_resource dr
  			 where dr.resource_type_id = 1) q;

-- getAmountTypes
select amt.* as amount_types
  from public.amount_type amt
 order by row_order;

-- updateAmountType
select * from public.change_amount_type(:amountTypeId, :usersDiyResourcesId, :userId, :diyResourceId);

-- addUserResource
with data as (insert into public.users_diy_resources(user_id, diy_resource_id, amount_type_id)
              values (:userId, :diyResourceId, :amountTypeId)
           returning *)
select data.diy_resource_id,
       data.amount_type_id,
       data.users_diy_resources_id,
       dr.name,
       dr.no
  from data,
       public.diy_resource dr
 where dr.diy_resource_id = data.diy_resource_id