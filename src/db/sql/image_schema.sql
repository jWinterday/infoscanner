-- getImageSchemaData
-- ***next generation*** get data for converted image
select *
  from get_schema_data(:projectId, :userId, :withConvertedImage)

-- saveImageSchemaInfo
-- ***next generation*** create schema
with data as (update public.resource_file_data
                 set image_data = json_data.schema_data
                from (select key::integer as diy_resource_id,
                             value as schema_data
                        from json_each_text((:calcResult)::json)) json_data
               where resource_file_data.resource_file_id = :resourceFileId
                 and resource_file_data.diy_resource_id = json_data.diy_resource_id
           returning 1)
update public.resource_file
	 set is_calculated = true,
	     alg_schema_time_sec = :algSchemaTimeSec
	from (select count(1) as upd_cnt from data) upd
 where upd.upd_cnt > 0
   and resource_file_id = :resourceFileId
returning *

-- getSchemaCount
-- ***next generation***
select st.schema_tune_id as id,
       st.name,
       st.note
  from schema_tune st
 where st.schema_tune_type_id = 1

-- saveConvertedFileInfo
select data.*
  from save_file_info(:userId, :countId, :compress, :altTime, :method, :realWidth,
                      :realHeight, :convertedImageData, :dataInfo, :resourceFileId) data;

-- getSchemaStatistics
with pInputData as (select (:pageNo)::integer + 1 as pPageNo,--page index begins with 0
                      		 (:pageSize)::integer as pPageSize),
     data as (select rfd.resource_file_data_id,
                     rfd.diy_resource_id,--diy resource
                     dr.name,
                     dr.no,
                     dr.lab,
                     dr.color as diy_resource_color,
                     rfd.next_diy_resource_id,--next diy resource
                     amt.amount_type_id,--amount
                     (amt.amount_type_id is not null) as in_my_palette,
                     case when amt.name is null then '-' else amt.name end as amount_type_name,
                     amt.color as amount_color,
                     jsonb_array_length(rfd.coordinates) as color_cnt
                from resource_file_data rfd
                join resource_file rf on rf.resource_file_id = rfd.resource_file_id
                join diy_resource dr on rfd.diy_resource_id = dr.diy_resource_id
                left join users_diy_resources udr on udr.user_id = :userId and udr.diy_resource_id = case when rfd.next_diy_resource_id is null then rfd.diy_resource_id else rfd.next_diy_resource_id end
               	left join amount_type amt on amt.amount_type_id = udr.amount_type_id
               where rf.project_id = :projectId)
select q.*,
			 ceil(1.0 * q.row_count / pInputData.pPageSize)::integer as pages
	from (select data.*,
	             next_dr.color as next_diy_resource_color,
               next_dr.name as next_name,
               next_dr.no as next_no,
               (select json_agg(row_to_json(q))
                  from (select dense_rank() over (partition by rec.in_my_palette order by color.get_color_delta_e(data.lab, rec.lab)) as rnk,
                               rec.in_my_palette,
                               rec.diy_resource_id,
                               rec.amount_type_id,
                               rec.diy_resource_color,
                               rec.color_cnt,
                               rec.no
                          from data rec
                         where rec.diy_resource_id != data.diy_resource_id) q
                  where q.rnk in (1)) as nearest_colors,
               count(*) over () as row_count,
               row_number() over (order by data.diy_resource_id desc) as rn
          from data
          left join diy_resource next_dr on next_dr.diy_resource_id = data.next_diy_resource_id) q,
       pInputData
 where q.rn between ((pInputData.pPageNo - 1) * pInputData.pPageSize + 1) and (pInputData.pPageNo * pInputData.pPageSize)
 order by q.color_cnt desc

-- getSchemaData
with data as (select q.*,
                     jsonb_array_length(q.coordinates) as color_cnt
                from (select rfd.resource_file_data_id,
                             rfd.diy_resource_id,
                             dr.name,
                             dr.no,
                             case when dr.no~E'^\\d+$' then dr.no::integer else -1 end as dr_parsed_no,
                             dr.color as diy_resource_color,
                             dr.lab,
                             amt.amount_type_id,--amount
                             (amt.amount_type_id is not null) as in_my_palette,
                             case when amt.name is null then '-' else amt.name end as amount_type_name,
                             amt.color as amount_color,
                             get_file_data_coordinates(rfd.resource_file_data_id) as coordinates,
                             rfd.is_custom_add
                        from resource_file_data rfd
                        join resource_file rf on rf.resource_file_id = rfd.resource_file_id
                        join diy_resource dr on rfd.diy_resource_id = dr.diy_resource_id
                        left join users_diy_resources udr on udr.user_id = :userId and udr.diy_resource_id = rfd.diy_resource_id
                        left join amount_type amt on amt.amount_type_id = udr.amount_type_id
                       where rf.project_id = :projectId) q)--and rfd.next_resource_file_data_id is null) q)
select q.*
  from (select data.resource_file_data_id,
               data.diy_resource_id,
               data.name,
               data.no,
               data.dr_parsed_no,
               data.diy_resource_color,
               data.amount_type_id,
               data.in_my_palette,
               data.amount_type_name,
               data.amount_color,
               data.coordinates,
               data.is_custom_add,
               data.color_cnt,
               (select json_agg(row_to_json(q))
                  from (select dense_rank() over (partition by rec.in_my_palette order by color.get_color_delta_e(data.lab, rec.lab)) as rnk,
                               rec.resource_file_data_id,
                               rec.in_my_palette,
                               rec.diy_resource_id,
                               rec.name,
                               rec.amount_type_id,
                               rec.amount_type_name,
                               rec.diy_resource_color,
                               rec.color_cnt,
                               rec.no
                          from data rec
                         where rec.diy_resource_id != data.diy_resource_id) q
                  where q.rnk in (1,2)) as nearest_colors
          from data) q--order by data.dr_parsed_no;--order by data.color_cnt descwhere q.color_cnt <> 0
 order by q.color_cnt desc;--q.dr_parsed_no;

-- setSchemaNextColor
update resource_file_data
   set next_resource_file_data_id = :nextNesourceFileDataId
 where resource_file_data_id = :resourceFileDataId
returning *;

-- restoreOriginal
select * from restore_schema_original(:projectId);

-- addNewPaletteColor
insert into resource_file_data(resource_file_id, diy_resource_id, is_custom_add)
select :resourceFileId,
       :diyResourceId,
       true
returning *;

-- removeUnused
select * from restore_schema_original(:projectId);