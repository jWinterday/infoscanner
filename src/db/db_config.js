module.exports = {
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  max: process.env.DB_MAX_CONN,//max number of connection can be open to database
  idleTimeoutMillis: process.env.DB_IDLE_MILLIS
};