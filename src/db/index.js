const { Pool } = require('pg');
const config = require('./db_config');
const pool = new Pool(config);

const query = (text, params, callback) => {
  return pool.query(text, params, callback);
};

module.exports = {
  query: query
};