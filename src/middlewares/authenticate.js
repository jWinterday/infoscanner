const jwt = require('jsonwebtoken');
const request = require('request');

const targetDir = process.env.TARGET_DIR || '../build';

const db = require('../db');
const sql = require('yesql')(targetDir + '/db/sql/', {type: 'pg'});

//private
const _getTokenFromHeader = (headers) => {
  let token;

  if (headers && headers.authorization) {
    const parts = headers.authorization.split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        return credentials;
      }
    }
  }

  return null;
};

//const failedRequestsByToken = {};

//public
const captchaCheck = (req, res, next) => {
  next();
  /*const captchaKey = process.env.CAPTCHA_KEY;
  
  const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify' +
          '?secret=' + captchaKey +
          "&response=" + req.body['g-recaptcha-response'] +
          "&remoteip=" + req.connection.remoteAddress;
  
  request(verificationUrl, (error, response, body) => {
    body = JSON.parse(body);

    if(body.success !== undefined && !body.success) {
      return res.json({
        'responseCode' : 1,
        'responseDesc' : "Failed captcha verification"
      });
    };
    
    next();
  });*/
};

const tokenCheck = (roleName) => {
  return (req, res, next) =>
  {
    let token = _getTokenFromHeader(req.headers);

    if (!token) {
      return res.status(401).send('unauthorized request');//wrong authorization token format');
    }

    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
      //refresh by refresh token. decoded.user_id or email
      if (err) {
        return res.status(401).send('invalid token');
      }

      if (roleName && !decoded.roles.includes(roleName)) {
        return res.status(403).send('no access');
      }

      req.userId = decoded.user_id;
      next();
    });
  };
};

const refreshTokenCheck = (req, res, next) => {
  let refreshToken = _getTokenFromHeader(req.headers);

  if (!refreshToken) {
    return res.status(401).send('unauthorized request');//wrong authorization token format');
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET_KEY, (err, payload) => {
    //refresh by refresh token. decoded.user_id or email
    if (err) {
      return res.status(401).send('invalid refresh token');
    }

    req.payload = payload;
    req.refreshToken = refreshToken;

    next();
  });
};


module.exports = {
  //failedRequestsByToken: failedRequestsByToken,
  tokenCheck: tokenCheck,
  refreshTokenCheck: refreshTokenCheck,
  captchaCheck: captchaCheck
};