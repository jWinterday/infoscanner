const joi = require('joi');
const { loginSchema, registerSchema, updateUserSchema } = require('../routes/utils/auth_schemas');

//login
const checkLogin = (req, res, next) => {
  const params = {
    email: req.body.email,
    password: req.body.password
  };

  joi.validate(params, loginSchema).then((credentials) => {
    next();
  })
  .catch((error) => {
    return res.status(500).send(error.message);
  });
};

//register
const checkRegister = (req, res, next) => {
  const params = {
    email: req.body.email,
    password: req.body.password,
    confirm_password: req.body.confirm_password,
    g_recaptcha_response: req.body['g-recaptcha-response']
  };

  joi.validate(params, registerSchema).then((credentials) => {
    next();
  })
  .catch((error) => {
    return res.status(500).send(error.message);
  });
};

//update user
const checkUpdateUser = (req, res, next) => {
  const params = {
    first_name: req.body.first_name,
    last_name: req.body.last_name
  };

  joi.validate(params, updateUserSchema).then((credentials) => {
    next();
  })
  .catch((error) => {
    return res.status(500).send(error.message);
  });
};


module.exports = {
  checkLogin: checkLogin,
  checkRegister: checkRegister,
  checkUpdateUser: checkUpdateUser
};