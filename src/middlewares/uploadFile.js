const multer = require('multer');
const path = require('path');
const uuidv4 = require('uuid/v4');

const targetDir = process.env.TARGET_DIR || '../build';
const fileDir = targetDir + '/public/uploads';

const _storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, fileDir);
  },
  filename: (req, file, cb) => {
    //const ext = path.extname(file.originalname).toLowerCase();
    const newFilename = 'doc-' + uuidv4();// + ext;
    cb(null, newFilename);
  }
});

const upload = multer({
  storage: _storage,
  fileFilter: (req, file, cb) => {
    const permittedTypes = ['image/png', 'image/jpeg', 'image/bmp'];
    if (!permittedTypes.includes(file.mimetype)) {
      return cb(new Error('Only .png, .jpeg, .bmp formats allowed'));
    }

    cb(null, true);
  },
  limits:{
    fileSize: 10 * 1024 * 1024//in bytes
  },
});


module.exports = upload;