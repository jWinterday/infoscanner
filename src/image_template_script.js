window.onload = () => {
  process(data);//set in pug template compilation
};

const process = (fileData) => {
  const pImageData = fileData.image;//array of coordinates and theirs colors
  const pData = fileData.data;//common info

  console.info(pImageData);
  console.info(pData);

  const cntX = pImageData.cntX;
  const cntY = pImageData.cntY;

  let coeff = pImageData.coeff;
  let imageWidth = pImageData.imageWidth;
  let imageHeight = pImageData.imageHeight;

  //DOM elements
  const $showGrid = document.getElementById('show_grid');
  const $showSchemaColor = document.getElementById('show_schema_color');
  const $showSchemaData = document.getElementById('show_schema_data');
  const $showGridAreaNumber = document.getElementById('show_grid_area_number');

  const $gridColor = document.getElementById('grid_color');
  const $schemaDataColor = document.getElementById('schema_data_color');
  const $schemaDataGridColor = document.getElementById('schema_data_grid_color');
  const $backgroundColor = document.getElementById('background_color');

  const $imageContainer = document.getElementById('image_container');

  //canvas
  const canvas = document.getElementById('image');
  const ctx = canvas.getContext('2d');

  //variables and constants
  const gridRowCount = 10;

  const cPadding = 20;//px left and top
  let zoom = 1;
  let padding = cPadding * zoom;

  const render = () => {
    //grid settings. values
    const cGridColor = $gridColor.value;
    const cSchemaDataColor = $schemaDataColor.value;
    const cSchemaDataGridColor = $schemaDataGridColor.value;
    const cBackgroundColor = $backgroundColor.value;

    const isShowGrid = $showGrid.checked;
    const isSchemaColor = $showSchemaColor.checked;
    const isSchemaData = $showSchemaData.checked;
    const isShowGridAreaNumber = $showGridAreaNumber.checked;

    imageWidth = pImageData.imageWidth * zoom;
    imageHeight = pImageData.imageHeight * zoom;
    coeff = pImageData.coeff * zoom;
    padding = cPadding * zoom;
    canvas.width = imageWidth + padding;
    canvas.height = imageHeight + padding;

    //console.info('imageWidth:', imageWidth, ', imageHeight:', imageHeight);

    //functions
    //= image_template_parts/grid.js
    //= image_template_parts/clear.js
    //= image_template_parts/background.js
    //= image_template_parts/area_numbers.js
    //= image_template_parts/schema_data.js
    //= image_template_parts/image.js

    //render
    clearAll();
    drawBackground();
    const renderInfo = () => {
      if (isSchemaData) {
        drawSchemaData();
      }
      if (isShowGridAreaNumber) {
        drawAreaNumber();
      }
      if (isShowGrid) {
        drawGrid();
      }
    };

    if (isSchemaColor) {
      drawImage().then(() => { renderInfo(); });
    } else {
      renderInfo();
    }
  };

  render();

  //= image_template_parts/events.js
};