const imageTemplateName = targetDir + '/image_template3.pug';
const fontName = 'CrossStitch3.ttf';
const jsName = 'image_template_script.js';

const pug = require('pug');
const rimraf = require('rimraf');
const minify = require('html-minifier').minify;

const targetDir = process.env.TARGET_DIR || '../build';
const publicDir = targetDir + '/public/';
const sourceFileDir = publicDir + 'uploads/';
const destFileDir = publicDir + 'converted/';

const fs = require('fs');


/*const saveImageToDiscAsync = (image, fileName) => {
  const ext = path.extname(fileName);
  const name = path.basename(fileName, ext);
  const imageProjectDirName = destFileDir + name + '/';

  return new Promise((resolve, reject) => {
    if (!fs.existsSync(destFileDir)) {
      fs.mkdirSync(destFileDir);
    }

    if (!fs.existsSync(imageProjectDirName)) {
      fs.mkdirSync(imageProjectDirName);
      image.write(imageProjectDirName + fileName);
      resolve(name);
    } else {
      rimraf(imageProjectDirName, (err) => {
        if (err) {
          reject('cannot remove file directory');
          return;
        }
        fs.mkdirSync(imageProjectDirName);
        image.write(imageProjectDirName + fileName);
        resolve(name);
      });
    }
  });
};*/

const generateHtmlAsync = (data, dbInfo) => {
  return new Promise((resolve, reject) => {
    const projectDirPath = destFileDir + dbInfo.project_name + '/';

    const script = fs.readFileSync(targetDir + '/' + jsName, (err) => {
      if (err) {
        reject('can not read js template file');
        return;
      }
    });

    let html = pug.renderFile(imageTemplateName, {
      pSchemaInfo: dbInfo,
      pFont: fontName,
      pData: JSON.stringify(data),
      pScript: script
    });

    html = minify(html, {
      ignoreCustomComments: true,
      removeComments: true,
      minifyCSS: true
      //maxLineLength: 500,
      //minifyJS: true
    });

    //html
    fs.writeFileSync(projectDirPath + dbInfo.project_name + '.html', html, 'utf8');
    //font
    fs.createReadStream(targetDir + '/' + fontName).pipe(fs.createWriteStream(projectDirPath + fontName));
    //js
    //fs.createReadStream(targetDir + '/' + jsName).pipe(fs.createWriteStream(projectDirPath + dbInfo.project_name + '.js'));
    //data
    //fs.writeFileSync(projectDirPath + 'data.json', JSON.stringify(data), 'utf8');

    resolve();
  });
};

const saveFileInfoDbAsync = (params, schemaInfoData) => {
  const queryParams = {
    params: JSON.stringify(params),
    schemaInfoData: JSON.stringify(schemaInfoData)
  };

  return db.query(sql.saveConvertedFileInfo(queryParams));
};


const gcd = (x, y) => (x % y === 0) ? y : gcd(y, x%y);//НОД

const scm = (x, y) => (x * y) / gcd(x, y);//НОК