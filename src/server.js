const express = require('express');
const app = express();
const chalk = require('chalk');
const bodyParser = require('body-parser');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const http = require('http');

//set environment. need .env file in root directory
require('dotenv').config();

const port = process.env.PORT || 3001;
const targetDir = process.env.TARGET_DIR || '../build';
app.set('port', port);
app.set("views", targetDir + '/public');//views');
app.set('view engine', 'pug');

//security
app.use(helmet({
  frameguard: {
    action: 'deny'
  },
  noSniff: false
}));
app.use(helmet.noCache());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy({ setTo: 'PHP 4.2.0' }));

//limit requests by IP. Apply to all requests
const limiter = rateLimit({
  windowMs: 1*1000,//in milliseconds
  max: 30//requests per per windowMs for each IP
});
app.use(limiter);

app.use(express.json({
  limit: '10MB'
}));
app.use(cookieParser());
app.use(express.static(targetDir + '/public'));
app.use(bodyParser.urlencoded({
  extended: true
}));

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = err;//req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

//---ROUTES---
const routeConfig = require('./routes/routes_config');
for (const r in routeConfig) {
  const route = require(routeConfig[r]);
  app.use(r, route);
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.info(chalk.red(bind + ' requires elevated privileges'));
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.info(chalk.red(bind + ' is already in use'));
      process.exit(1);
      break;
    default:
      throw error;
  }
}

//start server
const server = http.createServer(app);
server.on('error', onError);

server.listen(app.get('port'), () => {
  console.info(
    chalk.yellow('*****'),
    chalk.green(`InfoScanner:${app.get('port')}`),
    chalk.green(`pid:${process.pid}`),
    chalk.red(`NODE_ENV: ${process.env.NODE_ENV}`),
    chalk.yellow('*****'));
});