const memoryStorage = { };

const get = (key, engine='memory') => {
  return engine == 'storage' ? localStorage.getItem(key) : memoryStorage[key];
};

const getJSON = (key, engine='memory') => {
  try {
    return JSON.parse(get(key, engine));
  } catch(err) {
    console.info('getJSON: ', err);
  }
};

const set = (key, val, engine='memory') => {
  if (engine == 'storage') {
    localStorage.setItem(key, val);
    return;
  }

  memoryStorage[key] = val;
}


export default {
  get,
  getJSON,
  set
};