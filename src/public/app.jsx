const React = require('react');
const ReactDOM = require('react-dom');
const { HashRouter } = require('react-router-dom');

const Layout = require('./components/Layout/App');
import './CommonStyles.min.css';


ReactDOM.render((
  <HashRouter>
    <Layout />
  </HashRouter>
), document.getElementById('app'));