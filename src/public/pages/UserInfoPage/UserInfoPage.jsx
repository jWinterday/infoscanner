import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import UserInfoForm from './UserInfoForm';


class UserInfoPage extends React.Component {
  render() {
    return (
      <div>
        <fieldset>
          <legend>User info</legend>
          <Consumer>
            {ctx => {
              console.info(ctx);
              return (
                <div>
                  { ctx.userLoggedIn && <UserInfoForm ctx={ctx} />}

                  { ctx.userLoggedIn || <p>not logged</p>}
                </div>
              );
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
};


export default UserInfoPage;