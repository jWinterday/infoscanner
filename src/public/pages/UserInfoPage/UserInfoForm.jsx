import React from 'react';
import PropTypes from 'prop-types';

import FlexRow from '../../components/CommonView/FlexRow';


class UserInfoForm extends React.Component {
  constructor(props) {
    super(props);

    //console.info(props);

    const profile = props.ctx.profile || {};

    this.state = {
      first_name: profile.first_name || '',
      last_name: profile.last_name || '',
      message: props.ctx.message
    };
  }

  save = async (e) => {
    const { ajaxWrapper, updateUserProfile } = this.props.ctx.helpers;
    const params = {
      url: '/auth/ajax/updateuser',
      method: 'post',
      data: {
        first_name: this.state.first_name,
        last_name: this.state.last_name
      }
    };

    try {
      const data = await ajaxWrapper(params);
      if ((data || {}).success) {
        localStorage.setItem('token', (data || {}).token);
      }

      this.setState({
        success: data.success,
        message: data.message
      });
      updateUserProfile();
    } catch (err) {
      this.setState({
        success: false,
        message: err.status + ', ' + err.responseText
      });
    }
  }

  handleLogout = () => {
    localStorage.removeItem('token');
    const updateUserProfile = this.props.ctx.helpers.updateUserProfile;
    updateUserProfile();
  }

  onFieldsChanged = (e) => {
    const name = e.target.name;
    const val = e.target.value;

    this.setState((prevState, props) => {
      prevState[name] = val;
      return prevState;
    });
  }

  render() {
    const messageClass = this.state.success ? 'ok' : 'error';
    const profile = this.props.ctx.profile || {};
    const rolesAsStr = `[${(profile.roles || []).join(', ')}]`;

    return (
      <div>
        <FlexRow caption="Roles" value={rolesAsStr} />
        <FlexRow caption="First name" element={<input type="text" name="first_name" value={this.state.first_name} onChange={this.onFieldsChanged} />} />
        <FlexRow caption="Last name" element={<input type="text" name="last_name" value={this.state.last_name} onChange={this.onFieldsChanged} />} />

        {
          this.state.message &&
          <a className={messageClass}>{ this.state.message }</a>
        }

        <br />
        <button onClick={this.save}>save</button>
        <button onClick={this.handleLogout} style={{ float: 'right' }}>logout</button>
      </div>
    );
  }
};

UserInfoForm.propTypes = {
  ctx: PropTypes.object.isRequired
};


export default UserInfoForm;