import React from 'react';
import PropTypes from 'prop-types';
import LoadingOverlay from 'react-loading-overlay';

import SimpleSelect from '../../../components/CommonView/SimpleSelect';
import FlexRow from '../../../components/CommonView/FlexRow';
import CalculateMethod from './CalculateMethod';
import CalculatedImage from './CalculatedImage';

const defaultW = 200;
const defaultH = 300;
const defaultCompress = 10;
const defaultMethod = 'compress';
const defaultCountId = 7;

class ImageSchema extends React.PureComponent {
  notForRenderState = { };

  constructor(props) {
    super(props);

    const schemaInfo = props.schemaInfo;

    this.notForRenderState = {
      w: schemaInfo.real_width || defaultW,
      h: schemaInfo.real_height || defaultH,
      compress: schemaInfo.compress_value || defaultCompress,
      method: schemaInfo.method || defaultMethod,
      countId: schemaInfo.schema_tune_id || defaultCountId
    };

    //console.info('ImageSchema props: ', props);

    this.state = {
      isLoading: false,
      message: '',
      calculateBtnDisable: false,
      algTime: schemaInfo.alg_time_sec,
      convertedImageData: schemaInfo.converted_image_data,
      coordinates: schemaInfo.colors
    };
  }

  onCountChange = (val) => {
    Object.assign(this.notForRenderState, {countId: val});
  }

  onCalculateMethodChange = (params) => {
    Object.assign(this.notForRenderState, params);

    this.setState({
      calculateBtnDisable: !params.correct
    });
  }

  calculateSchema = async (e) => {
    const params = {
      url: '/image_schema/ajax/calculate_schema',
      method: 'post',
      data: {
        projectId: this.props.projectId,
        compress: this.notForRenderState.compress,
        countId: this.notForRenderState.countId,
        method: this.notForRenderState.method,
        w: this.notForRenderState.w,
        h: this.notForRenderState.h
      }
    };

    this.setState({
      isLoading: true
    });

    try {
      const data = await this.props.ajaxWrapper(params);

      this.setState({
        isLoading: false,
        algTime: (data || {}).algTime,
        message: 'successfully calculated',
        convertedImageData: (data || {}).converted_image_data,
        coordinates: (data || {}).colors
      });

    } catch(err) {
      this.setState({
        isLoading: false,
        message: err
      });
    }
  }

  render() {
    const { schemaInfo } = this.props;
    const algTime = this.state.algTime ? `// ${this.state.algTime}(sec)`: '';
    const summaryName = `${schemaInfo.original_name} ${algTime}`;

    return (
      <LoadingOverlay active={this.state.isLoading} spinner text='Calculating image...'>
        <div className="table">
          <FlexRow caption="Name" element={<b>{summaryName}</b>} />
          <FlexRow caption="Count" element={<SimpleSelect items={this.props.data} defaultId={this.notForRenderState.countId} onChange={this.onCountChange} />} />
          <FlexRow caption="Method" element={<CalculateMethod w={this.notForRenderState.w}
                                                              h={this.notForRenderState.h}
                                                              compress={this.notForRenderState.compress}
                                                              method={this.notForRenderState.method}
                                                              onChange={this.onCalculateMethodChange} />}
          />
          <FlexRow caption="" element={<input type="button"
                                              onClick={this.calculateSchema}
                                              disabled={this.state.calculateBtnDisable}
                                              value="Calculate" />}
          />
          {
            this.state.message &&
            <FlexRow caption="" element={<span className="message-info">{this.state.message}</span>} />
          }
        </div>

        { Boolean(this.state.convertedImageData) || <p>Not calculated yet</p> }

        {
          this.state.convertedImageData &&
          <CalculatedImage ajaxWrapper={this.props.ajaxWrapper}
                           compress={this.notForRenderState.compress}
                           coordinates={this.state.coordinates}
                           imageData={this.state.convertedImageData}
                           schemaInfo={schemaInfo} />
        }
      </LoadingOverlay>
    );
  }
}

ImageSchema.defaultProps = {
  onImageCalculated: () => {}
};

ImageSchema.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired,
  schemaInfo: PropTypes.object.isRequired,
  data: PropTypes.array,//count list
  projectId: PropTypes.number.isRequired,
  onImageCalculated: PropTypes.func
};


export default ImageSchema;