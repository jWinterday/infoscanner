import React from 'react';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

import ImageFilterBar from './ImageFilterBar';

const styles = csjs`${ require('./LoadFormStyle.min.css') }`;
insertCss(csjs.getCss(styles));

const defaultX = 5;
const defaultY = 5;
const defaultWidth = 90;
const defaultHeight = 90;

const defaultFilter = {
  contrast: 100,
  grayscale: 0,
  invert: 0,
  opacity: 100,
  saturate: 100,
  brightness: 100,
  hueRotate: 0,
  sepia: 0
};

const getFilterStr = (params) => {
  let filter = '';
  for (let key in params) {
    const unitMeas = key === 'hueRotate' ? 'deg' : '%';
    const prop = key === 'hueRotate' ? 'hue-rotate' : key;
    filter += `${prop}(${params[key]}${unitMeas})`;
  }

  return filter;
}

class LoadForm extends React.Component {
  constructor(props) {
    super(props);

    //console.info(props);

    this.state = {
      crop: {
        x: defaultX,
        y: defaultY,
        width: defaultWidth,
        height: defaultHeight
      },
      pixelCrop: {},
      imgFilter: Object.assign({}, defaultFilter),
      isExistsFileForUpload: false,
      imageData64: props.data.image_data,
      originalName: props.data.original_name,
      message: ''
    };
  }

  handleFileUpload = (e) => {
    const file = e.target.files[0];
    const fr = new FileReader();
    fr.onload = (res) => {
      this.setState({
        //isImageCropped: false,
        isExistsFileForUpload: Boolean(file),
        originalName: file.name,
        imgFilter: Object.assign({}, defaultFilter),
        imageData64: res.target.result
      });
    };
    fr.readAsDataURL(file);
  }

  onUploadClick = async (e) => {
    try {
      const img64 = await this.getCroppedImg();
      const params = {
        method: 'post',
        url: '/image_schema/ajax/upload',
        data: {
          projectId: this.props.projectId,
          originalName: this.state.originalName,
          imageData64: img64
        }
      };
      const data = await this.props.ajaxWrapper(params);

      this.setState({
        imageData64: data.image_data,
        crop: {
          x: defaultX,
          y: defaultY,
          width: defaultWidth,
          height: defaultHeight
        },
        imgFilter: Object.assign({}, defaultFilter),
        message: `successfully uploaded image with ${data.resource_file_id} id`
      });
      this.props.onImageUpload(data);
    } catch(err) {
      this.setState({
        message: err ? err : 'unknown error'
      });
    }
  }

  onChange = (crop, pixelCrop) => {
    this.setState({ crop, pixelCrop });
  }

  getCroppedImg = () => {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.onload = () => {
        const pixelCrop = this.state.pixelCrop;

        const imageW = img.width;
        const imageH = img.height;
        const croppedW = ('width' in pixelCrop) ? pixelCrop.width : (imageW*defaultWidth/100);
        const croppedH = ('height' in pixelCrop) ? pixelCrop.height : (imageH*defaultHeight/100);
        const deltaX = ('x' in pixelCrop) ? pixelCrop.x : (imageW*defaultX/100);
        const deltaY = ('y' in pixelCrop) ? pixelCrop.y : (imageH*defaultY/100);

        const canvas = document.createElement('canvas');
        canvas.width = croppedW;
        canvas.height = croppedH;
        const ctx = canvas.getContext('2d');
        ctx.filter = getFilterStr(this.state.imgFilter);

        ctx.drawImage(
          img,
          deltaX,
          deltaY,
          croppedW,
          croppedH,
          0,
          0,
          croppedW,
          croppedH
        );

        resolve(canvas.toDataURL('image/jpeg'));
      };
      img.onerror = (err) => {
        reject(err);
      };

      img.src = this.state.imageData64;
    });
  }

  onFilterChange = (params) => {
    this.setState({
      imgFilter: params
    });
  }

  onClearClick = (e) => {
    this.setState({
      imgFilter: Object.assign({}, defaultFilter)
    })
  }

  render() {
    const filterStr = getFilterStr(this.state.imgFilter);
    const imgStyle = {filter: filterStr};
    const uploadDisable = !Boolean(this.state.imageData64);

    return (
      <div>
        <input type="file" name="doc" accept="image/*" onChange={this.handleFileUpload} />
        <input type="button" value="upload" onClick={this.onUploadClick} disabled={uploadDisable} />
        {
          this.state.originalName &&
          <p><b>{this.state.originalName}</b></p>
        }
        <br />
        {
          this.state.imageData64 &&
          <div>
            <div className={styles.imageContainer}>
              <ImageFilterBar onChange={this.onFilterChange}
                              onClearClick={this.onClearClick}
                              filter={this.state.imgFilter} />

              <div>
                <ReactCrop src={this.state.imageData64}
                           onChange={this.onChange}
                           crop={this.state.crop}
                           keepSelection={true}
                           maxWidth={100}
                           imageStyle={imgStyle}
                />
              </div>
            </div>
          </div>
        }
        { this.state.message && <p className="message-info">{this.state.message}</p> }
      </div>
    );
  }
}


LoadForm.defaultProps = {
  data: null,
  onImageUpload: () => {}
};
LoadForm.propTypes = {
  data: PropTypes.object,//returned from parent AsyncComponent(data)
  projectId: PropTypes.number.isRequired,
  ajaxWrapper: PropTypes.func.isRequired,
  onImageUpload: PropTypes.func
};

export default LoadForm;