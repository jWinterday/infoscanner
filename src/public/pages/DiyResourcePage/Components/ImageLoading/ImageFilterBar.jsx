import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./ImageFilterBarStyle.min.css') }`;
insertCss(csjs.getCss(styles));

class ImageFilterBar extends React.Component {
  constructor(props) {
    super(props);

    //console.info(props);

    this.state = {
      filter: props.filter//Object.assign({}, props.filter)
    };
  }

  onChange = (e) => {
    const name = e.target.name;
    const val = e.target.value;

    const obj = {};
    obj[name] = val;
    const nextFilter = Object.assign(this.state.filter, obj);

    this.setState({
      filter: nextFilter,
    }, () => {
      this.props.onChange(this.state.filter);
    });
  }

  render() {
    const filter = this.state.filter;

    return (
      <div className={styles.barContainer}>
        <div>
          <div>
            <input name="contrast" type="range" className={styles.buttonRange} min="0" max="200" step="10" value={filter.contrast} onChange={this.onChange} />
            <label htmlFor="contrast">Contrast({filter.contrast}%)</label>
          </div>

          <div>
            <input name="grayscale" type="range" className={styles.buttonRange} min="0" max="100" step="10" value={filter.grayscale} onChange={this.onChange} />
            <label htmlFor="grayscale">Grayscale({filter.grayscale}%)</label>
          </div>

          <div>
            <input name="brightness" type="range" className={styles.buttonRange} min="0" max="200" step="10" value={filter.brightness} onChange={this.onChange} />
            <label htmlFor="brightness">Brightness({filter.brightness}%)</label>
          </div>

          <div>
            <input name="hueRotate" type="range" className={styles.buttonRange} min="0" max="360" step="10" value={filter.hueRotate} onChange={this.onChange} />
            <label htmlFor="hueRotate">Hue({filter.hueRotate}deg)</label>
          </div>

          <div>
            <input name="invert" type="range" className={styles.buttonRange} min="0" max="100" step="10" value={filter.invert} onChange={this.onChange} />
            <label htmlFor="invert">Invert({filter.invert}%)</label>
          </div>

          <div>
            <input name="opacity" type="range" className={styles.buttonRange} min="0" max="100" step="10" value={filter.opacity} onChange={this.onChange} />
            <label htmlFor="opacity">Opacity({filter.opacity}%)</label>
          </div>

          <div>
            <input name="saturate" type="range" className={styles.buttonRange} min="0" max="100" step="10" value={filter.saturate} onChange={this.onChange} />
            <label htmlFor="saturate">Saturate({filter.saturate}%)</label>
          </div>

          <div>
            <input name="sepia" type="range" className={styles.buttonRange} min="0" max="100" step="10" value={filter.sepia} onChange={this.onChange} />
            <label htmlFor="sepia">Sepia({filter.sepia}%)</label>
          </div>
          <input type='button' className={styles.button} value='Default filters' onClick={this.props.onClearClick} />
        </div>
      </div>
    );
  }
}


ImageFilterBar.defaultProps = {
  onChange: () => {},
  onClearClick: () => {},
  filter: {
    contrast: 100,
    grayscale: 0,
    invert: 0,
    opacity: 100,
    saturate: 100,
    brightness: 100,
    hueRotate: 0,
    sepia: 0
  }
};
ImageFilterBar.propTypes = {
  onChange: PropTypes.func,
  onClearClick: PropTypes.func,
  filter: PropTypes.object
};

export default ImageFilterBar;