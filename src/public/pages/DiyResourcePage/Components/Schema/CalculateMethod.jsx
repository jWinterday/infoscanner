import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./CalculateMethodStyle.min.css') }`;
insertCss(csjs.getCss(styles));

const isCorrectVal = (txt='') => {
  return /^\d+$/.test(txt) && Number(txt) > 0;
}

class CalculateMethod extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      w: props.w,
      h: props.h,
      compress: props.compress,
      method: props.method
    };
  }

  onChange = (e) => {
    const target = e.target;
    const val = target.value;
    const name = target.name;

    const param = {};
    param[name] = val;

    const nextState = Object.assign(this.state, param);
    const correct =
      (this.state.method === 'size' && isCorrectVal(nextState.w) && isCorrectVal(nextState.h))
      ||
      (this.state.method === 'compress' && isCorrectVal(nextState.compress));

    this.props.onChange(nextState, correct);
  }

  render() {
    let compressClass = '';
    let sizeClass = '';

    if (this.state.method === 'compress') {
      compressClass = isCorrectVal(this.state.compress) ? 'editableOk' : 'editableError';
    }
    if (this.state.method === 'size') {
      sizeClass = (isCorrectVal(this.state.w) && isCorrectVal(this.state.h)) ? 'editableOk' : 'editableError';
    }

    const compressDisabled = (this.state.method === 'size');
    const sizeDisabled = (this.state.method === 'compress');

    return (
      <div>
        <div className={styles.container}>
          <dl>
            {/*Compress coefficient*/}
            <dt>
              <label>
                <input name="method" type="radio" value="compress" className={styles.method} checked={this.state.method === 'compress'} onChange={this.onChange} />
                <span className={styles.rowCaption}>Compress coefficient</span>
              </label>
            </dt>
            <dd className="table">
              <div className="row">
                <input name="compress" disabled={compressDisabled} type="text" value={this.state.compress} className={styles.editRow + ' ' + compressClass} onChange={this.onChange} />
              </div>
            </dd>

            {/*Width/Height*/}
            <dt>
              <label>
                <input name="method" type="radio" value="size" className={styles.method} checked={this.state.method === 'size'} onChange={this.onChange} />
                <span className={styles.rowCaption}>Width/Height(mm)</span>
              </label>
            </dt>
            <dd className="table">
              <div className="row">
                <input name="w" disabled={sizeDisabled} type="text" value={this.state.w} className={styles.editRow + ' ' + sizeClass} onChange={this.onChange} />
              </div>
              <div className="row">
                <input name="h" disabled={sizeDisabled} type="text" value={this.state.h} className={styles.editRow + ' ' + sizeClass} onChange={this.onChange} />
              </div>
            </dd>
          </dl>
        </div>
      </div>
    );
  }
}


CalculateMethod.defaultProps = {
  w: 300,
  h: 300,
  compress: 5,
  method: 'size',
  onMethodChange: () => {}
};

CalculateMethod.propTypes = {
  w: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  h: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  compress: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  method: PropTypes.oneOf(['compress', 'size']),
  onChange: PropTypes.func
};

export default CalculateMethod;