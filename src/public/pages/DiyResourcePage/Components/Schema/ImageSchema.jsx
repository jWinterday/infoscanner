import React from 'react';
import PropTypes from 'prop-types';
import LoadingOverlay from 'react-loading-overlay';
import csjs from 'csjs';
import insertCss from 'insert-css';

import AsyncComponent from '../../../../components/AsyncComponent/AsyncComponent';
import SimpleSelect from '../../../../components/CommonView/SimpleSelect';
import FlexRow from '../../../../components/CommonView/FlexRow';
import CalculateMethod from './CalculateMethod';
import SchemaSettings from './SchemaSettings';
import CalculatedSchema from './CalculatedSchema';
import PaletteItems from './PaletteItems';

const styles = csjs`${ require('./ImageSchemaStyle.min.css') }`;
insertCss(csjs.getCss(styles));

const defaultW = 200;
const defaultH = 300;
const defaultCompress = 10;
const defaultMethod = 'compress';
const defaultCountId = 7;

class ImageSchema extends React.PureComponent {
  constructor(props) {
    super(props);

    const { data } = props;

    //console.info(props);

    this.state = {
      isLoading: false,
      message: '',

      convertedImageData64: data.converted_image_data,
      realWidth: data.real_width || defaultW,
      realHeight: data.real_height || defaultH,
      compress: data.compress_value || defaultCompress,
      method: data.method || defaultMethod,
      countId: data.schema_tune_id || defaultCountId,

      colors: data.colors,
      selectedColorsInPalette: null,

      schemaSettings: [],

      isExistsPreview: data.is_exists_preview,
      isCalculated: data.is_calculated,
      isSettingsCorrect: true
    };
  };

  //promise for count list
  getCountList = () => {
    return this.props.ajaxWrapper({
      url: '/image_schema/ajax/schema_count',
      method: 'post'
    });
  };

  onCountChange = (val) => {
    this.setState({
      countId: val
    });
  };

  onCalculateMethodChange = (params, correct) => {
    this.setState({
      realWidth: Number(params.w),
      realHeight: Number(params.h),
      compress: Number(params.compress),
      method: params.method,
      isSettingsCorrect: correct
    });
  };

  calculatePreviewSchema = async () => {
    const params = {
      url: '/image_schema/ajax/calculate_preview_schema',
      method: 'post',
      data: {
        projectId: this.props.projectId,
        compress: this.state.compress,
        countId: this.state.countId,
        method: this.state.method,
        w: this.state.realWidth,
        h: this.state.realHeight
      }
    };

    this.setState({
      isLoading: true
    });

    try {
      const data = await this.props.ajaxWrapper(params);

      this.setState({
        isLoading: false,
        //schemaName: data.original_name,
        message: `successfully created preview for ${data.alg_time_sec}sec`,
        convertedImageData64: (data || {}).converted_image_data,
        //coordinates: (data || {}).colors,
        isExistsPreview: data.is_exists_preview,
        isCalculated: data.is_calculated,
        colors: data.colors
      });
    } catch(err) {
      this.setState({
        isLoading: false,
        message: err
      });
    };
  };

  calculateSchema = async () => {
    const params = {
      url: '/image_schema/ajax/create_schema',
      method: 'post',
      data: {
        projectId: this.props.projectId
      }
    };

    this.setState({
      isLoading: true
    });

    try {
      const data = await this.props.ajaxWrapper(params);

      console.info(data);

      this.setState({
        isLoading: false,
        message: data.alg_time_sec ? `successfully created schema for ${data.alg_schema_time_sec}sec` : null,
        isCalculated: data.is_calculated,
        colors: data.colors
      });
    } catch(err) {
      this.setState({
        isLoading: false,
        message: err
      });
    };
  };

  onSchemaSettingsClick = (schemaSettings) => {
    this.setState({
      schemaSettings
    });
  };

  onPaletteItemsClick = (ids) => {
    const colors = this.state.colors.filter(p => ids.includes(p.diy_resource_id));

    this.setState({
      selectedColorsInPalette: colors
    })
  }

  render() {
    const { original_name } = this.props.data;

    const settings = ['move', 'zoom'];
    if (this.state.isCalculated) {
      settings.push('colors');
    }

    return (
      <LoadingOverlay active={this.state.isLoading} spinner text='Calculating image...'>
        <div className="table">
          <FlexRow caption="Name" element={<b>{original_name}</b>} />
          <FlexRow caption="Count" element=
            {
              <AsyncComponent promise={this.getCountList}>
                <SimpleSelect defaultId={this.state.countId}
                              onChange={this.onCountChange} />
              </AsyncComponent>
            }
          />
          <FlexRow caption="Method" element={<CalculateMethod w={this.state.realWidth}
                                                              h={this.state.realHeight}
                                                              compress={this.state.compress}
                                                              method={this.state.method}
                                                              onChange={this.onCalculateMethodChange} />}
          />
          <FlexRow caption="" element={<input type="button"
                                              onClick={this.calculatePreviewSchema}
                                              disabled={!this.state.isSettingsCorrect}
                                              value="Create preview" />}
          />
          <FlexRow caption="" element={<input type="button"
                                              onClick={this.calculateSchema}
                                              disabled={!this.state.isExistsPreview}
                                              value="Create schema" />}
          />
          {
            this.state.message &&
            <FlexRow caption="" element={<span className="message-info">{this.state.message}</span>} />
          }
        </div>

        { !this.state.isExistsPreview && <p><b>Not calculated yet</b></p> }

        { this.state.isExistsPreview && <SchemaSettings names={settings}
                                                        onClick={this.onSchemaSettingsClick} /> }

        <div className={styles.imagesSchemaContainer}>
          {
            this.state.schemaSettings.includes('colors') &&
            <PaletteItems colors={this.state.colors}
                          onClick={this.onPaletteItemsClick}
            />
          }

          {
            this.state.convertedImageData64 &&
            <CalculatedSchema convertedImageData64={this.state.convertedImageData64}
                              selectedColors={this.state.selectedColorsInPalette}
            />
          }
        </div>


        {/*
          this.state.convertedImageData64 && false &&
          <div style={{position: "relative", top: 0, left: 0}}>
            <img src={this.state.convertedImageData64} style={{position: "relative", top: 0, left: 0, backgroundColor: "transparent"}} />
            {
              this.state.calcSchemaImages &&
              <img src={this.state.calcSchemaImages[1381]} style={{position: "absolute", top: 0, left: 0, backgroundColor: "transparent"}} />
            }
          </div>
        }
        <SchemaSettings names={schemaSettings}/>
        {
          this.state.convertedImageData64 &&
          <div style={{position: "relative", top: 0, left: 0}}>
            <img src={this.state.convertedImageData64} style={{position: "relative", top: 0, left: 0, backgroundColor: "transparent"}} />
            {
              this.state.calcSchemaImages &&
              <img src={this.state.calcSchemaImages[1381]} style={{position: "absolute", top: 0, left: 0, backgroundColor: "transparent"}} />
            }
          </div>
        }

        {
          this.state.convertedImageData64 && false &&
          <CalculatedImage ajaxWrapper={this.props.ajaxWrapper}
                           compress={this.notForRenderState.compress}
                           //coordinates={this.state.coordinates}
                           imageData64={this.state.convertedImageData64}
                           schemaInfo={schemaInfo} />
        }*/}
      </LoadingOverlay>
    );
  }
}

ImageSchema.defaultProps = {

};

ImageSchema.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired,
  //schemaInfo: PropTypes.object.isRequired,//=>data
  data: PropTypes.object,//returned from parent AsyncComponent(data)
  projectId: PropTypes.number.isRequired
};


export default ImageSchema;