import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./CalculatedSchemaStyle.min.css') }`;
insertCss(csjs.getCss(styles));


class CalculatedSchema extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps, nextContent) {
    //console.info('CalculatedSchema. nextProps: ', nextProps)
  }

  render() {
    const selectedColors = this.props.selectedColors || [];

    return (
      <div className={styles.calculatedSchemaContainer}>
        <img
          src={this.props.convertedImageData64}
          className={styles.calculatedSchemaImage}
          style={{
            opacity: selectedColors.length === 0 ? 1 : 0.5,
          }}
        />

        {
          selectedColors.map((item, index) => {
            return (
              <img key={`selected_img_${index}`}
                   src={item.image_data}
                   className={styles.selectedImagesSchema}
              />
            )
          })
        }
      </div>
    );
  }
}


CalculatedSchema.defaultProps = {
  selectedColors: []
};

CalculatedSchema.propTypes = {
  convertedImageData64: PropTypes.string.isRequired,
  selectedColors: PropTypes.array
};

export default CalculatedSchema;