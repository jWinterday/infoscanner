import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

import util from '../../../../utils/index';

const styles = csjs`${ require('./PaletteItemsStyle.min.css') }`;
insertCss(csjs.getCss(styles));


class PaletteItems extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      message: null,
      selectedItems: []
    };
  };

  componentWillReceiveProps(nextProps, nextContent) {

  }

  /*addPaletteColorPromise = (params) => {
    const query = {
      url: '/image_schema/ajax/add_palette_color',
      method: 'post',
      data: params
    };

    return new Promise((resolve, reject) => {
      this.props.ajaxWrapper(query)
        .then(data => {
          this.setState({
            data: data,
            currentItem: null,
            currentIndex: -1,
            message: 'color has added'
          });
          resolve(data);
        })
        .catch(err => {
          this.setState({
            message: 'color has not added.Reason: ' + err.response.status + ' ' + err.response.statusText
          });
          reject(err);
        });
    });

    //return this.props.ajaxWrapper(query);
  }

  onCurrentColorItemClick = (item, index) => {
    return (e) => {
      this.setState({
        currentItem: item,
        currentIndex: index
      });
      this.props.onCurrentColorItemClick(item, index);
    };
  }

  //outer add new color
  save() {
    if (this.state.currentItem) {
      return this.addPaletteColorPromise({
        projectId: this.props.projectId,
        resourceFileId: this.props.resourceFileId,
        diyResourceId: this.state.currentItem.diy_resource_id
      });
    } else {
      this.setState({
        message: 'select color before'
      });
    }
  }*/

  onClick = (e) => {
    const id = Number(e.target.dataset.id);

    if (!id) {
      return false;
    }

    const items = this.state.selectedItems;
    let nextItems = [];

    if (e.ctrlKey) {
      nextItems = items.includes(id) ? items.filter(p => p !== id) : [...items, id];
    } else {
      nextItems = items.includes(id) ? [] : [id];
    }

    //console.info(nextItems);

    this.setState({
      selectedItems: nextItems
    })

    this.props.onClick(nextItems);
  }

  render() {
    const colors = this.props.colors || [];
    const selectedItems = this.state.selectedItems || [];

    //console.info('in render selectedItems: ', selectedItems);

    return (
      <div className={styles.paletteContainer}>
        { colors.length > 0 && <p className="caption">{`Count: ${colors.length}`}</p> }
        <p>*Use ctrl for multi select</p>

        <div className={styles.palettes}
             onClick={this.onClick}
        >
          {
            colors.map((item, index) => {
              const borderClass = selectedItems.includes(item.diy_resource_id) ? styles.selectedItem : styles.notSelectedItem;

              return (
                <div
                  key={`schema_color_box_${index}`}
                  data-id={item.diy_resource_id}
                  className={[styles.paletteItem, borderClass].join(' ')}
                  style={{
                    backgroundColor: util.getColorStr(item.color)
                  }}
                />
              )
            })
          }
        </div>
      </div>
    );
  };
}


PaletteItems.defaultProps = {
  colors: [],
  onClick: () => {}
};

PaletteItems.propTypes = {
  colors: PropTypes.array,
  onClick: PropTypes.func
};


module.exports = PaletteItems;