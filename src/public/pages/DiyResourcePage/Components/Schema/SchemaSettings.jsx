import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

import util from '../../../../utils/index';
const styles = csjs`${ require('./SchemaSettingsStyle.min.css') }`;
insertCss(csjs.getCss(styles));


class SchemaSettings extends React.Component {
  constructor(props) {
    super(props);

    const images = [];

    if (props.names.includes('move')) {
      images.push({
        name: 'move',
        path: './images/schema/hand.min.png',
        group: 'moving'
      });
    }

    if (props.names.includes('zoom')) {
      images.push({
        name: 'zoom',
        path: './images/schema/zoom.min.png',
        group: 'moving'
      });
    }

    if (props.names.includes('colors')) {
      images.push({
        name: 'colors',
        path: './images/schema/colors.min.png',
        group: 'colors'
      });
    }

    /*images.push({
      name: 'select',
      path: './images/schema/dot.min.png',
      group: 'select'
    });*/

    /*images.push({
      name: 'zoneOrders',
      path: './images/schema/numbers.min.png',
      group: 'zoneOrders',
      alt: 'order'
    });*/

    this.state = {
      images,
      selectedItems: {}
    };
  }

  onClick = (e) => {
    const target = e.target;
    const name = target.name;

    if (target.tagName !== 'IMG') {
      return false;
    }

    const { images, selectedItems } = this.state;

    if (name in selectedItems) {
      delete selectedItems[name];
    } else {
      const item = images.find(p => p.name === name);
      const groupItems = images.filter(p => p.group === item.group && p.name !== item.name);
      groupItems.map((item, index) => {
        delete selectedItems[item.name];
      });
      selectedItems[name] = true;
    }

    this.setState({
      selectedItems
    });

    this.props.onClick(Object.keys(selectedItems));
  }

  render() {
    const { names } = this.props;
    const { images, selectedItems } = this.state;

    return (
      <div className={styles.settingsContainer} onClick={this.onClick}>
        {
          images.filter(p => (names || []).includes(p.name)).map((item, index) => {
            return (
              <img
                key={`schema_settings_${item.name}`}
                name={item.name}
                src={item.path}
                title={item.alt || item.name}
                alt={item.alt || item.name}
                className={selectedItems.hasOwnProperty(item.name) ? styles.settingsElementSelected : styles.settingsElement}
              />
            )
          })
        }
      </div>
    );
  }
}


SchemaSettings.defaultProps = {
  onClick: () => {},
  names: ['colors', 'move', 'zoom']
};

SchemaSettings.propTypes = {
  onClick: PropTypes.func,
  names: PropTypes.array
};

export default SchemaSettings;