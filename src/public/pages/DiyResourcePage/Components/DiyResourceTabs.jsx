import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import LoadForm from './ImageLoading/LoadForm';
import ImageSchema from './Schema/ImageSchema';
import AsyncComponent from '../../../components/AsyncComponent/AsyncComponent';

import '../../react-tabs.min.css';

class DiyResourceTabs extends React.Component {
  constructor(props) {
    super(props);

    const data = props.data;
    const { is_schema_exists } = data;

    let tab = Number(localStorage.getItem('diyResourceTab')) || 0;
    if (!is_schema_exists) {
      tab = 0;
    }

    this.state = {
      isSchemaExists: is_schema_exists,
      tabIndex: tab
    };
  }

  //promise for tab
  getLoadFormData = () => {
    const { projectId, ajaxWrapper } = this.props;

    return ajaxWrapper({
      url: '/project/ajax/load_form_data',
      method: 'post',
      data: { projectId }
    });
  }

  //promise for tab
  getImageSchemaData = () => {
    const { projectId, ajaxWrapper } = this.props;

    return ajaxWrapper({
      url: '/image_schema/ajax/image_schema_data',
      method: 'post',
      data: { projectId }
    });
  }

  handleTabChange = (tabIndex) => {
    this.setState({
      tabIndex: tabIndex
    });

    localStorage.setItem('diyResourceTab', tabIndex);
  }

  onImageUpload = (data) => {
    this.setState({
      isSchemaExists: Boolean(data.resource_file_id)
    });
  }

  render() {
    return (
      <div>
        <Tabs selectedIndex={ this.state.tabIndex } onSelect={ this.handleTabChange }>
          <TabList>
            <Tab>Image Loading</Tab>
            {
              this.state.isSchemaExists &&
              <Tab>Schema</Tab>
            }
          </TabList>

          <TabPanel>
            <AsyncComponent promise={this.getLoadFormData}>
              <LoadForm ajaxWrapper={this.props.ajaxWrapper}
                        projectId={this.props.projectId}
                        onImageUpload={this.onImageUpload}
              />
            </AsyncComponent>
          </TabPanel>

          {
            this.state.isSchemaExists &&
            <TabPanel>
              <AsyncComponent promise={this.getImageSchemaData}>
                <ImageSchema ajaxWrapper={this.props.ajaxWrapper}
                             projectId={this.props.projectId}
                />
              </AsyncComponent>
            </TabPanel>
          }
        </Tabs>
      </div>
    );
  }
}


DiyResourceTabs.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired,
  projectId: PropTypes.number.isRequired,
  data: PropTypes.object//returned from parent AsyncComponent(data)
};

export default DiyResourceTabs;