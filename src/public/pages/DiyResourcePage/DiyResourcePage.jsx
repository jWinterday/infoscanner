import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import AsyncComponent from '../../components/AsyncComponent/AsyncComponent';
import DiyResourceTabs from './Components/DiyResourceTabs';


class DiyResourcePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      projectId: Number(this.props.match.params.id)
    };
  }

  getProjectData = (ajaxWrapper) => {
    return () => {
      return ajaxWrapper({
        url: '/project/ajax/project_data',
        method: 'post',
        data: {
          projectId: this.state.projectId,
        }
      })
    }
  }

  render() {
    return (
      <div>
        <fieldset>
          <legend>DIY resources</legend>
          <Consumer>
            {ctx => {
              const ajaxWrapper = ctx.helpers.ajaxWrapper;

              return (
                <div>
                  <AsyncComponent promise={this.getProjectData(ajaxWrapper)}>
                    <DiyResourceTabs ajaxWrapper={ajaxWrapper} projectId={this.state.projectId} />
                  </AsyncComponent>
                </div>
              )
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
}


export default DiyResourcePage;