import React from 'react';
import PropTypes from 'prop-types';
import { Stage, Layer, Image, Group } from 'react-konva';
import csjs from 'csjs';
import insertCss from 'insert-css';

import util from '../../../utils/index';
import Tooltip from '../../../components/Konva/Tooltip';
import ImageZoning from '../../../components/Konva/ImageZoning';
import SchemaSettings from '../Components/Schema/SchemaSettings';
import MainColorPanel from '../../../components/Konva/MainColorPanel';
import SelectedColorBox from "../../../components/Konva/SelectedColorBox";
const styles = csjs`${ require('./CalculatedImageStyle.min.css') }`;
insertCss(csjs.getCss(styles));

//panel
const w = window.innerWidth - 150;
const h = window.innerHeight - 150;

const itemsInLine = 5;
const panelColorBoxW = 32;
const panelColorBoxMargin = 3;
const panelMarginX = 25;
const panelMarginY = 18;
const colorBoxWidth = 20;
const panelW = itemsInLine * (panelColorBoxMargin + panelColorBoxW) + panelColorBoxMargin;
const panelH = h - 10;

const zoningCount = 10;

let zoomedImage;

class CalculatedImage extends React.Component {
  getStageRef = (stage) => { this.stage = stage };
  getImageRef = (image) => { this.image = image };

  constructor(props) {
    super(props);

    //console.info('CalculatedImage. props: ', props);

    this.state = {
      message: '',
      coordinates: props.coordinates,
      zoomPlusImage: null,
      zoomMinusImage: null,
      mousePos: {},
      tooltipVisible: false,
      tooltipText: '',
      selectedCoordinates: null,


      isMainColorVisible: false,
      schemaSettings: [],

      imageData64: null,
      imageOriginalWidth: 0,
      imageOriginalHeight: 0,
      imageRotation: 0,
      imagePositionX: panelMarginX,
      imagePositionY: panelMarginY + panelColorBoxMargin,
      imageOffsetX: 0,
      imageOffsetY: 0
    };
  }

  componentDidMount = async () => {
    try {
      const imageData64 = await util.loadImageAsync(this.props.imageData64);
      const zoomPlusImage = await util.loadImageAsync('./images/schema/plus.min.png');
      const zoomMinusImage = await util.loadImageAsync('./images/schema/minus.min.png');

      this.setState({
        zoomPlusImage,
        zoomMinusImage,

        imageData64,
        imageOriginalWidth: imageData64.width,
        imageOriginalHeight: imageData64.height,
        imageOffsetX: imageData64.width / 2,
        imageOffsetY: imageData64.height / 2,
        //imagePositionX: panelMarginX + imageData64.width / 2,
        //imagePositionY: panelMarginY + panelColorBoxMargin + imageData64.height / 2,
      });
    } catch (err) {
      //console.info(err);
    }
  };

  componentWillReceiveProps = async (nextProps) => {
    try {
      const imageData64 = await util.loadImageAsync(nextProps.imageData64);

      this.setState({
        imageOriginalWidth: imageData64.width,
        imageOriginalHeight: imageData64.height,
        imageData64,
        coordinates: nextProps.coordinates,
        selectedCoordinates: null
      });
    } catch (err) {
      //console.info(err);
    }
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.image.cache();
  }

  onWheel = (e) => {
    /*e.evt.preventDefault();

    const scaleBy = 1.1;

    const oldScale = this.image.scaleX();

    const mousePointTo = {
      x: this.stage.getPointerPosition().x / oldScale - this.image.x() / oldScale,
      y: this.stage.getPointerPosition().y / oldScale - this.image.y() / oldScale,
    };

    const newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;
    this.image.scale({ x: newScale, y: newScale });

    const newPos = {
      x: -(mousePointTo.x - this.stage.getPointerPosition().x / newScale) * newScale,
      y: -(mousePointTo.y - this.stage.getPointerPosition().y / newScale) * newScale
    };
    this.image.position(newPos);
    this.stage.batchDraw();*/
  };

  onStageClick = (e) => {
    //const pos = this.stage.getPointerPosition();
    //console.info(pos);
  };

  onLayerMouseOver = (e) => {
    const { schemaSettings } = this.state;
    const isCanMove = schemaSettings.includes('move');

    document.body.style.cursor = isCanMove ? 'move' : 'default';
  };

  onLayerMouseOut = (e) => {
    document.body.style.cursor = 'default';
  };

  onMainColorPanelItemOver = (item) => {
    this.setState({
      tooltipVisible: true,
      tooltipText: item.tooltip,
      mousePos: item.mousePos
    });
  };

  onMainColorPanelItemOut = () => {
    this.setState({
      tooltipVisible: false
    });
  };

  onMainColorPanelItemClick = (id) => {
    //const colors = ((this.props.schemaInfo || {}).colors || []);
    const colors = this.state.coordinates || {};
    const index = colors.findIndex(p => p.diy_resource_id === id);
    if (index === -1) { return false; }

    const { coordinates } = colors[index];

    this.setState({
      selectedCoordinates: coordinates
    });
  };

  onSchemaSettingsClick = (selectedItems) => {
    const isCanMove = selectedItems.includes('move');
    if (isCanMove && zoomedImage) {
      zoomedImage.destroy();
    }

    this.setState({
      schemaSettings: [...selectedItems]
    });
  };

  onFinishSelectArea = (selectedCoordinates) => {
    //console.info('onFinishSelectArea. selectedCoordinates: ', selectedCoordinates);

    if (zoomedImage) {
      zoomedImage.destroy();
    }

    const layer = this.image.getLayer();

    if (selectedCoordinates) {
      const imageGroup = this.stage.findOne('.imageGroup');
      const groupPosition = Object.assign({}, { x: (imageGroup.x()>>0), y: (imageGroup.y()>>0) });
      const imagePosition = Object.assign({}, { x: (this.image.x()>>0), y: (this.image.y()>>0) });

      const clone = this.image.clone({
        x: groupPosition.x + selectedCoordinates.x0,
        y: groupPosition.y + selectedCoordinates.y0,
        width: selectedCoordinates.x1 - selectedCoordinates.x0,
        height: selectedCoordinates.y1 - selectedCoordinates.y0,
        opacity: 1,
        stroke: 'cyan',
        //strokeWidth: 1,
        draggable: true
      });

      zoomedImage = clone.crop({
        name: 'zoomedImage',
        x: -imagePosition.x + selectedCoordinates.x0,
        y: -imagePosition.y + selectedCoordinates.y0,
        width: selectedCoordinates.x1 - selectedCoordinates.x0,
        height: selectedCoordinates.y1 - selectedCoordinates.y0,
      });

      //scaling
      const scaleX = 1.5;
      const scaleY = 1.5;

      zoomedImage.setAttrs({
        //x: w / 2 - scaleX * (selectedCoordinates.x1 - selectedCoordinates.x0) / 2,
        //y: h / 2 - scaleY * (selectedCoordinates.y1 - selectedCoordinates.y0) / 2,
        //x: panelMarginX,
        //y: panelMarginY,
        scaleX: scaleX,
        scaleY: scaleY
      });

      //=====================
      const colors = this.props.coordinates || {};

      let caughtColors = [];
      const realSelectedCoordinates = {
        x0: selectedCoordinates.x0 - panelMarginX,
        y0: selectedCoordinates.y0 - (panelColorBoxMargin + panelMarginY),
        x1: selectedCoordinates.x1 - panelMarginX,
        y1: selectedCoordinates.y1 - (panelColorBoxMargin + panelMarginY)
      };

      for (let i=0; i<colors.length; i++) {
        const arr = colors[i].coordinates;
        for (let j=0; j<arr.length; j++) {
          if (
            arr[j].x >= realSelectedCoordinates.x0
            &&
            arr[j].y >= realSelectedCoordinates.y0
            &&
            arr[j].x < realSelectedCoordinates.x1
            &&
            arr[j].y < realSelectedCoordinates.y1
          ) {
            caughtColors.push(colors[i]);
            break;
          }
        }
      }

      this.setState({
        coordinates: caughtColors
      });

      //console.info('caughtColors: ', caughtColors);
      //=====================
      layer.add(zoomedImage);
      this.image.setAttr('opacity', 0.3);
    }

    layer.batchDraw();
  };

  render() {
    const {
      coordinates,
      imageData64,
      imagePositionX,
      imagePositionY,
      imageOriginalWidth,
      imageOriginalHeight,
      schemaSettings,
      selectedCoordinates,
      message,
      mousePos,
      tooltipVisible,
      tooltipText
    } = this.state;

    const isCanZoom = schemaSettings.includes('zoom');
    const isCanMove = schemaSettings.includes('move');
    const isShowColors = schemaSettings.includes('colors');
    //const isShowZoneOrders = schemaSettings.includes('zoneOrders');

    const imageOpacity = (selectedCoordinates && isShowColors || isCanZoom) ? 0.3 : 1;


    return (
      <div style={{width: w + 20, height: h + 100}} className={styles.imageContainer}>
        { coordinates && <p>Color count: {coordinates.length}</p> }

        {/*schema settings*/}
        <SchemaSettings
          x={panelMarginX}
          y={0}
          width={w}
          height={panelMarginY}
          onClick={this.onSchemaSettingsClick}
        />

        <Stage ref={this.getStageRef} width={w} height={h} fill="red">
          <Layer onMouseOver={this.onLayerMouseOver} onMouseOut={this.onLayerMouseOut} fill="red">
            {/*image and selected colors*/}
            <Group draggable={isCanMove}
                   name="imageGroup"
                   //rotation={this.state.imageRotation}
                   //offsetX={this.state.imageOriginalWidth - 200}
                   //offsetY={this.state.imageOriginalHeight}
                   //x={this.state.imagePositionX}
                   //y={this.state.imagePositionY}
                   onDragStart={e => {}}
                   onDragEnd={e => {}}
            >
              <Image
                ref={this.getImageRef}
                name="image"
                image={imageData64}
                x={imagePositionX}
                y={imagePositionY}
                width={imageOriginalWidth}
                height={imageOriginalHeight}
                opacity={imageOpacity}
                onWheel={this.onWheel}
                onDragStart={e => {}}
                onDragEnd={e => {}}
              />

              {
                (this.image && isShowColors) &&
                <SelectedColorBox
                  data={selectedCoordinates}
                  width={this.props.compress}
                  height={this.props.compress}
                  offsetX={this.image.getAttr('x')}
                  offsetY={this.image.getAttr('y')}
                />
              }

              {
                this.image
                &&
                (Math.ceil(imageOriginalWidth/(zoningCount*this.props.compress)) > 1)
                &&
                <ImageZoning
                  isCanZoomImage={isCanZoom}
                  //isShowZoneOrders={isShowZoneOrders}
                  //isCanZoomImage={false}
                  offsetX={imagePositionX}
                  offsetY={imagePositionY}
                  width={imageOriginalWidth}
                  height={imageOriginalHeight}
                  stepX={this.props.compress}
                  stepY={this.props.compress}
                  zoningCount={zoningCount}
                  onFinishSelectArea={this.onFinishSelectArea}
                />
              }
            </Group>

            {/*main color panel*/}
            {
              (coordinates || []).length > 0 &&
              <MainColorPanel
                colors={coordinates}
                colorBoxWidth={colorBoxWidth}
                panelMarginX={panelMarginX}
                panelMarginY={panelMarginY + panelColorBoxMargin}
                itemsInLine={itemsInLine}
                //panelH={panelH-panelMarginY}
                //panelH={Math.max(panelH-panelMarginY, 500)}
                panelH={Math.round(panelH/2)}
                panelColorBoxMargin={panelColorBoxMargin}
                panelColorBoxW={panelColorBoxW}
                onItemMouseOver={this.onMainColorPanelItemOver}
                onItemMouseOut={this.onMainColorPanelItemOut}
                onColorBoxClick={this.onMainColorPanelItemClick}
                visible={isShowColors}
              />
            }
          </Layer>

          {/*tooltip*/}
          <Layer>
            <Tooltip mousePos={mousePos}
                     visible={tooltipVisible}
                     text={tooltipText}
            />
          </Layer>
        </Stage>

        { message && <span className='message-info'>{message}</span> }
      </div>
    );
  }
}

CalculatedImage.defaultProps = {
  coordinates: []
};

CalculatedImage.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired,
  schemaInfo: PropTypes.object.isRequired,//DEPRECATED

  imageData64: PropTypes.string,
  //coordinates: PropTypes.array,
  coordinates: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  compress: PropTypes.number
};

export default CalculatedImage;