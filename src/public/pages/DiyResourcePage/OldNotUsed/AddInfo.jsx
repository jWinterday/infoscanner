const React = require('react');
const { Link } = require('react-router-dom');


class AddInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  renderLinkArray(arr) {
    arr = arr || [];

    return (
      <div>
        <span>Used in projects: [</span>
        {
          arr.map((id, index) => {
            const link = '/project/' + id;
            return (
              <Link key={ id } to={ link } className='link' target='_blank'>
                { id }
              </Link>
            )
          })
        }
        <span>]</span>
      </div>
    )
  }

  render() {
    const usedInProjects = this.renderLinkArray(this.props.used_in_projects);

    return (
      <div className='table'>
        <a className='row'>Resource ID: { this.props.diy_resource_id || '-' }</a>
        <a className='row'>Add info: { this.props.add_info || '-' }</a>
        <div className='row'>{ usedInProjects }</div>
      </div>
    );
  }
}


module.exports = AddInfo;