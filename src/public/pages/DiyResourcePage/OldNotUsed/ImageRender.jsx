import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';

import PaletteItems from '../Components/Schema/PaletteItems';
import DiyResourceTable from '../../../components/DiyResourceTable/DiyResourceTable';
import ColorBoxRegion from './ColorBoxRegion';

const buildRgbText = (obj) => {
  return 'rgb(' + obj.R + ',' + obj.G + ',' + obj.B + ')';
}

class ImageRender extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    //console.info(props);

    //view mode
    let viewMode = localStorage.getItem('viewMode');
    if(!['none', 'no', 'cnt'].includes(viewMode)) {
      viewMode = 'none';
      localStorage.setItem('viewMode', viewMode);
    }

    this.state = {
      viewMode: viewMode,
      message: '',
      data: this.props.data,
      curSelectedColorItem: null,
      withBorder: localStorage.getItem('withBorder') === 'true',
      showImage: true//localStorage.getItem('showImage') === 'true'
    };

    this.addColorRef = React.createRef();
    this.colorBoxRegionRef = React.createRef();
  }

  componentDidMount() {
    const gridCount = 10;
    const schema = this.props.schema_info;
    const W = schema.image_width;
    const H = schema.image_height;

    console.info('W: ', W, ', H:', H);

    const compressWidth = schema.compress_width;
    const compressHeight = schema.compress_height;

    const canvas = this.staticCanvas;
    const ctx = canvas.getContext('2d');

    canvas.width = schema.image_width;
    canvas.height = schema.image_height;

    //image
    this.loadImagePromise()
      .then(img => {
        ctx.drawImage(img, 0, 0);

        ctx.beginPath();
        ctx.fillStyle = 'blue';
        ctx.font = '14pt Arial';
        ctx.strokeStyle = 'red';

        //vertical
        let i = 0;
        let counter = 0;
        let gridTxt = 0;
        while(counter < W) {
          if (i%gridCount === 0) {
            ctx.moveTo(counter, 0);
            ctx.lineTo(counter, H);
            if (counter !== 0) {
              ctx.fillText(gridTxt, counter, 20);
            }
            gridTxt += gridCount;
          }
          counter += compressWidth;
          i++;
        }

        //horizontal
        i=0;
        counter = 0;
        gridTxt = 0;
        while(counter < H) {
          if (i%gridCount === 0) {
            ctx.moveTo(0, counter);
            ctx.lineTo(W, counter);
            if (counter !== 0) {
              ctx.fillText(gridTxt, 0, counter);
            }
            gridTxt += gridCount;
          }
          counter += compressHeight;
          i++;
        }
        ctx.stroke();
        ctx.closePath();
      })
      .catch(err => {
        this.setState({
          message: 'failed to load image'
        });
      });

    //=================================================
    //var gkhead = new Image;
    //gkhead.src = this.props.path;
    //let x = 1;

    //gkhead.onload = () => {
      //setInterval(draw, 100);
      //ctx.setTransform(0.3, 0, 1, 1, 0, 0);
      //ctx.scale(0.5, 1);
    //  ctx.drawImage(gkhead,0,0);
    //};

    const draw = () => {
      //console.info(x);
      //ctx.setTransform(0, 1, x, 0, 0, 0);
      ctx.scale(x, x);
      //ctx.drawImage(gkhead,0,0);
      x+=0.1;
      if (x > 1.5) {
        x=1;
      }
    };


    /*var gkhead = new Image;

    //window.onload = function(){

      //var ctx = canvas.getContext('2d');
      trackTransforms(ctx);

      function redraw(){

        // Clear the entire canvas
        var p1 = ctx.transformedPoint(0,0);
        var p2 = ctx.transformedPoint(canvas.width,canvas.height);
        ctx.clearRect(p1.x,p1.y,p2.x-p1.x,p2.y-p1.y);

        ctx.save();
        ctx.setTransform(1,0,0,1,0,0);
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();

        ctx.drawImage(gkhead,0,0);

      }
      redraw();

      var lastX=canvas.width/2, lastY=canvas.height/2;

      var dragStart,dragged;

      canvas.addEventListener('mousedown',function(evt){
        document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
        lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        dragStart = ctx.transformedPoint(lastX,lastY);
        dragged = false;
      },false);

      canvas.addEventListener('mousemove',function(evt){
        lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        dragged = true;
        if (dragStart){
          var pt = ctx.transformedPoint(lastX,lastY);
          ctx.translate(pt.x-dragStart.x,pt.y-dragStart.y);
          redraw();
        }
      },false);

      canvas.addEventListener('mouseup',function(evt){
        dragStart = null;
        if (!dragged) zoom(evt.shiftKey ? -1 : 1 );
      },false);

      var scaleFactor = 1.1;

      var zoom = function(clicks){
        var pt = ctx.transformedPoint(lastX,lastY);
        ctx.translate(pt.x,pt.y);
        var factor = Math.pow(scaleFactor,clicks);
        ctx.scale(factor,factor);
        ctx.translate(-pt.x,-pt.y);
        redraw();
      }

      var handleScroll = function(evt){
        var delta = evt.wheelDelta ? evt.wheelDelta/40 : evt.detail ? -evt.detail : 0;
        if (delta) zoom(delta);
        return evt.preventDefault() && false;
      };

      canvas.addEventListener('DOMMouseScroll',handleScroll,false);
      canvas.addEventListener('mousewheel',handleScroll,false);
    //};

    gkhead.src = this.props.path;

    // Adds ctx.getTransform() - returns an SVGMatrix
    // Adds ctx.transformedPoint(x,y) - returns an SVGPoint
    function trackTransforms(ctx){
      var svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
      var xform = svg.createSVGMatrix();
      ctx.getTransform = function(){ return xform; };

      var savedTransforms = [];
      var save = ctx.save;
      ctx.save = function(){
        savedTransforms.push(xform.translate(0,0));
        return save.call(ctx);
      };

      var restore = ctx.restore;
      ctx.restore = function(){
        xform = savedTransforms.pop();
        return restore.call(ctx);
      };

      var scale = ctx.scale;
      ctx.scale = function(sx,sy){
        xform = xform.scaleNonUniform(sx,sy);
        return scale.call(ctx,sx,sy);
      };

      var rotate = ctx.rotate;
      ctx.rotate = function(radians){
        xform = xform.rotate(radians*180/Math.PI);
        return rotate.call(ctx,radians);
      };

      var translate = ctx.translate;
      ctx.translate = function(dx,dy){
        xform = xform.translate(dx,dy);
        return translate.call(ctx,dx,dy);
      };

      var transform = ctx.transform;
      ctx.transform = function(a,b,c,d,e,f){
        var m2 = svg.createSVGMatrix();
        m2.a=a; m2.b=b; m2.c=c; m2.d=d; m2.e=e; m2.f=f;
        xform = xform.multiply(m2);
        return transform.call(ctx,a,b,c,d,e,f);
      };

      var setTransform = ctx.setTransform;
      ctx.setTransform = function(a,b,c,d,e,f){
        xform.a = a;
        xform.b = b;
        xform.c = c;
        xform.d = d;
        xform.e = e;
        xform.f = f;
        return setTransform.call(ctx,a,b,c,d,e,f);
      };

      var pt  = svg.createSVGPoint();
      ctx.transformedPoint = function(x,y){
        pt.x=x; pt.y=y;
        return pt.matrixTransform(xform.inverse());
      }
    }*/
  }

  loadImagePromise = () => {
    const img = new Image;
    img.src = this.props.path;

    return new Promise((resolve, reject) => {
      img.onload = () => {
        resolve(img);
      }
      img.onerror = (err) => {
        reject(err);
      }
    });
  }

  getSchemaDataPromise = () => {
    return this.props.ajaxWrapper({
      url: '/image_schema/ajax/get_data',
      method: 'post',
      data: {
        projectId: this.props.projectId
      }
    });
  }

  getSchemaCanvasRef = (cnv) => { this.schemaCanvas = cnv };
  getStaticCanvasRef = (cnv) => { this.staticCanvas = cnv };
  getAddColorRef = (comp) => { this.addColor = comp };

  onCurrentColorItemClick = (item, index) => {
    const curItem = this.state.data[index];
    this.setState({
      curSelectedColorItem: curItem
    });

    this.highlightCoordinates(curItem, false);
  }

  highlightCoordinates = (curItem=this.state.curSelectedColorItem, isOnlyImage=false) => {
    const schema = this.props.schema_info;
    const W = schema.image_width;
    const H = schema.image_height;

    const withBorder = this.state.withBorder;
    const data = curItem.coordinates;
    const compressWidth = schema.compress_width;
    const compressHeight = schema.compress_height;
    const color = curItem.next ? buildRgbText(curItem.next.diy_resource_color) : buildRgbText(curItem.diy_resource_color);

    const canvas = this.schemaCanvas;
    const ctx = canvas.getContext('2d');

    canvas.width = schema.image_width;
    canvas.height = schema.image_height;

    const drawColorRect = (x, y, w, h, color) => {
      ctx.beginPath();
      //ctx.rect((0.5 + x)<<0, (0.5 + y)<<0, w, h);
      ctx.rect(x, y, w, h);
      ctx.fillStyle = color;//withBorder ? color: 'green';

      //if (withBorder) {
      //  ctx.strokeStyle = 'black';
      //  ctx.stroke();
      //}
      ctx.fill();
      ctx.closePath();
    };

    ctx.clearRect(0, 0, W, H);

    data.map((item, index) => {
      drawColorRect(item.x, item.y, compressWidth, compressHeight, color);
    });

    /*this.imageLoadPromise()
      .then(img => {
        if (isOnlyImage) {
          ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
          return;
        };

        const withBorder = this.state.withBorder;
        const data = curItem.coordinates;
        const compressValue = schema.compress_value;
        const color = curItem.next ? buildRgbText(curItem.next.diy_resource_color) : buildRgbText(curItem.diy_resource_color);

        if (this.state.showImage) {
          ctx.save();
          ctx.globalAlpha = 0.1;
          ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
          ctx.restore();
        } else {
          ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        }

        data.map((item, index) => {
          drawColorRect(item.x, item.y, compressValue, compressValue, color, withBorder);
        });
      });*/
  }

  hideColorBoxRegion = (e) => {
    this.setState({
      curSelectedColorItem: null
    });
  }

  withBorderClick = (e) => {
    const nextVal = !this.state.withBorder;
    this.setState({
      withBorder: nextVal
    });

    localStorage.setItem('withBorder', String(nextVal));

    //this.highlightCoordinates();
  }

  showImageClick = (e) => {
    const nextVal = !this.state.showImage;
    this.setState({
      showImage: nextVal
    });

    //localStorage.setItem('showImage', String(nextVal));
    //this.highlightCoordinates();
  }

  onAddNextColor = async (e) => {
    const schemaInfo = this.props.schema_info;

    const callPopup = (data) => {
      Popup.create({
        title: 'Add color',
        content: <PaletteItems ref={ this.addColorRef }
                               data={ data }
                               isAddNext={ false }
                               asDialog={ true }
                               viewMode='no'
                               projectId={ this.props.projectId }
                               resourceFileId={ schemaInfo.resource_file_id }
                               ajaxWrapper={ this.props.ajaxWrapper } />,
        buttons: {
          left: [
            { text: 'Exit', action: async () => {
                try {
                  const data = await this.getSchemaDataPromise();
                  this.setState({
                    data: [...data],
                    message: 'palette has updated'
                  });
                } catch(err) {
                  this.setState({ message: 'failed to refresh palette' });
                } finally { Popup.close(); }
            }},
            { text: 'Add',
              id: 'ctrl+enter',
              className: 'success',
              action: async () => {
                try {
                  await this.addColorRef.current.save();
                } catch(err) { }
              }
            }]
        }
      });
    };

    const params = {
      url: '/diy_resources/ajax/get_all_palette',
      method: 'post',
      data: { projectId: this.props.projectId }
    };

    try {
      const data = await this.props.ajaxWrapper(params);
      callPopup(data);
    } catch(e) {
      this.setState({ message: 'failed to get next colors' });
    }
  }

  viewModeClick = (e) => {
    const mode = e.currentTarget.value;
    localStorage.setItem('viewMode', mode);

    this.setState({
      viewMode: mode
    });
  }

  restoreOriginalClick = async (e) => {
    const params = {
      url: '/image_schema/ajax/restore_original',
      method: 'post',
      data: {
        projectId: this.props.projectId
      }
    };

    try {
      const data = await this.props.ajaxWrapper(params);
      this.setState({
        data: [...data],
        message: 'successfully restored original image'
      });
      console.info(data);
    } catch(e) {
      this.setState({ message: e.response.data });
    }
  }

  removeUnusedClick = async (e) => {
    const params = {
      url: '/image_schema/ajax/remove_unused',
      method: 'post',
      data: {
        projectId: this.props.projectId
      }
    };

    try {
      const data = await this.props.ajaxWrapper(params);
      this.setState({
        data: [...data],
        message: 'successfully removed unused colors'
      });
      console.info(data);
    } catch(e) {
      this.setState({ message: e.response.data });
    }
  }

  onChangeColor = async (e) => {
    try {
      const data = await this.getSchemaDataPromise();
      this.setState({
        data: [...data],
        message: 'palette has updated'
      });
    } catch(err) {
      this.setState({ message: 'failed to refresh palette' });
    }
  }

  render() {
    const isCustomExists = this.state.data.filter(p => p.is_custom_add === true).length > 0;

    return (
      <div>
        <div className='canvas-container' style={{ width: this.props.schema_info.image_width, height: this.props.schema_info.image_height }}>
          {/*<canvas ref={ this.getBgCanvasRef } className='bg-canvas' />*/}
          {/*<img src={ this.props.path } className='bg-image' style={{ opacity: this.state.showImage ? 0.6 : 0.1}} />*/}
          <canvas ref={ this.getStaticCanvasRef } className='static-canvas' />
          <canvas ref={ this.getSchemaCanvasRef } className='dynamic-canvas' />
        </div>

        {
          this.state.message &&
          <p className='error'>{ this.state.message }</p>
        }

        <fieldset>
          <legend>Palette</legend>

          <PaletteItems isAddNext={ true }
                        data={ this.state.data }
                        style={{}}
                        viewMode={ this.state.viewMode }
                        onCurrentColorItemClick={ this.onCurrentColorItemClick }
                        onAddNextColor={ this.onAddNextColor } />

          <div style={{ paddingTop: '15px', paddingBottom: '15px' }}>
            <label className='pointer'>
              <input name="view_mode" type="radio" value='none' checked={ this.state.viewMode === 'none' } onChange={ this.viewModeClick }/>
              None(compact)
            </label>
            <label className='pointer'>
              <input name="view_mode" type="radio" value='no' checked={ this.state.viewMode === 'no' } onChange={ this.viewModeClick }/>
              DMC number
            </label>
            <label className='pointer'>
              <input name="view_mode" type="radio" value='cnt' checked={ this.state.viewMode === 'cnt' } onChange={ this.viewModeClick }/>
              Color count
            </label>
          </div>

          {
            isCustomExists &&
            <div>
              <input type='button' value='Restore original' onClick={ this.restoreOriginalClick } />
              {/*<input type='button' value='Remove unused' onClick={ this.removeUnusedClick } />*/}
            </div>
          }

          {
            this.state.curSelectedColorItem &&
            <div style={{paddingTop: '15px', paddingBottom: '15px'}}>
              <label className='pointer'>
                <input type='checkbox' checked={this.state.withBorder} onChange={this.withBorderClick}/>
                Border
              </label>
              <label className='pointer'>
                <input type='checkbox' checked={this.state.showImage} onChange={this.showImageClick}/>
                Show image
              </label>
            </div>
          }

          {
            this.state.curSelectedColorItem &&
            <ColorBoxRegion ref={ this.colorBoxRegionRef } item={ this.state.curSelectedColorItem } viewMode={ this.state.viewMode }
                            ajaxWrapper={ this.props.ajaxWrapper } hideColorBoxRegion={ this.hideColorBoxRegion }
                            projectId={ this.props.projectId } onChangeColor={ this.onChangeColor }/>
          }
        </fieldset>
      </div>
    );
  }
}


ImageRender.propTypes = {
  data: PropTypes.array,
  schema_info: PropTypes.object.isRequired,
  path: PropTypes.string.isRequired,
  ajaxWrapper: PropTypes.func.isRequired,
  projectId: PropTypes.number.isRequired
};


module.exports = ImageRender;