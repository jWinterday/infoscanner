import React from 'react';

import AsyncComponent from '../../../components/AsyncComponent/AsyncComponent';

class HtmlRender extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    this.getSchemaInfo = this.getSchemaInfo.bind(this);
  }

  getSchemaInfo() {
    const projectId = this.props.projectId;

    return new Promise((resolve, reject) => {
      this.props.ajaxWrapper({
        url: '/image_schema/ajax/get_statistics',
        method: 'post',
        data: {projectId: projectId}
      })
      .then(data => {
        //console.info(data);
        resolve(data);
      })
      .catch(e => {
        //console.info(e);
        reject(e);
      });
    });
  }

  render() {
    return (
      <div>
        <AsyncComponent promise={ this.getSchemaInfo }>
          <HtmlImage />
        </AsyncComponent>
        {/*<table style={{border: 'none'}} cellSpacing="0" cellPadding="0">
          <colgroup>
            <col width="40px"/>
            <col width="40px"/>
            <col width="40px"/>
          </colgroup>
          <tbody>
          <tr style={{height: "40px"}}>
            <td className='test-cell cell-1'>
              <div>11</div>
            </td>
            <td className='test-cell cell-2'>
              <div>12</div>
            </td>
            <td className='test-cell cell-1'>
              <div>13</div>
            </td>
          </tr>
          <tr style={{height: "40px"}}>
            <td className='test-cell cell-1'>
              <div>21</div>
            </td>
            <td className='test-cell cell-2'>
              <div>22</div>
            </td>
            <td className='test-cell cell-2'>
              <div>23</div>
            </td>
          </tr>
          </tbody>
        </table>*/}
      </div>
    );
  }
}


class HtmlImage extends React.Component {
  state = {};

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <table>
          <thead>
          <tr>
            <th>Resource ID</th>
            <th>Name</th>
            <th>No</th>
            <th>DMC color</th>
            <th>Color count</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          {
            this.props.data.map((item, index) => {
              const dmcColor = item.diy_resource_color;
              const dmcRGB = 'rgb(' + dmcColor.R + ',' + dmcColor.G + ',' + dmcColor.B + ')';

              return (
                <tr key={ index } className='schema-statistics-row'>
                  <td className='schema-statistics-cell'>{ item.diy_resource_id }</td>
                  <td className='schema-statistics-cell'>{ item.name }</td>
                  <td className='schema-statistics-cell'>{ item.no }</td>
                  <td style={{backgroundColor: dmcRGB}}>{ dmcRGB }</td>
                  <td className='schema-statistics-cell'>{ item.color_cnt }</td>
                  <td className='schema-statistics-cell'>{ item.amount_type_name || '-' }</td>
                </tr>
              );
            })
          }
          </tbody>
        </table>
      </div>
    );
  }
}


module.exports = HtmlRender;