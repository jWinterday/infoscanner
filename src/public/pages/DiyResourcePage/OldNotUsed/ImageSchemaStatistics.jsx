const React = require('react');

import AsyncComponent from '../../../components/AsyncComponent/AsyncComponent';


class ImageSchemaStatistics extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    this.getStatistics = this.getStatistics.bind(this);
  }

  getStatistics() {
    const projectId = this.props.projectId;

    return new Promise((resolve, reject) => {
      this.props.ajaxWrapper({
        url: '/image_schema/ajax/get_statistics',
        method: 'post',
        data: {projectId: projectId}
      })
      .then(data => {
        //console.info(data);
        resolve(data);
      })
      .catch(e => {
        //console.info(e);
        reject(e);
      });
    });
  }


  render() {
    return (
      <div style={{paddingTop: '40px', paddingBottom: '40px'}}>
        <fieldset>
          <AsyncComponent promise={ this.getStatistics }>
            <StatisticsRenderer />
          </AsyncComponent>
        </fieldset>
      </div>
    );
  }
}

class StatisticsRenderer extends React.Component {
  state = {};

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Resource ID</th>
              <th>Name</th>
              <th>No</th>
              <th>DMC color</th>
              <th>Color count</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.data.map((item, index) => {
                const dmcColor = item.diy_resource_color;
                const dmcRGB = 'rgb(' + dmcColor.R + ',' + dmcColor.G + ',' + dmcColor.B + ')';

                return (
                  <tr key={ index } className='schema-statistics-row'>
                    <td className='schema-statistics-cell'>{ item.diy_resource_id }</td>
                    <td className='schema-statistics-cell'>{ item.name }</td>
                    <td className='schema-statistics-cell'>{ item.no }</td>
                    <td style={{backgroundColor: dmcRGB}}>{ dmcRGB }</td>
                    <td className='schema-statistics-cell'>{ item.color_cnt }</td>
                    <td className='schema-statistics-cell'>{ item.amount_type_name || '-' }</td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}


module.exports = ImageSchemaStatistics;