const React = require('react');


class ChooseSchemaNextColor extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    const item = props.data;
    const nearestColors = item.nearest_colors;
    const nextDiyResourceId = item.next_diy_resource_id;
    const matchInColors = nearestColors.filter(p => p.diy_resource_id === item.next_diy_resource_id);
    const mess = (Boolean(nextDiyResourceId) && matchInColors.length === 0) ?
                 'There are the best color in palette' : '';

    this.state = {
      curId: props.data.next_diy_resource_id || props.data.diy_resource_id,
      message: mess
    };

    this.onChange = this.onChange.bind(this);
    this.save = this.save.bind(this);
  }

  save() {
    const ajaxWrapper = this.props.ajaxWrapper;
    return ajaxWrapper({
      url: '/image_schema/ajax/set_schema_next_color',
      method: 'post',
      data: {
        resource_file_data_id: this.props.data.resource_file_data_id,
        diy_resource_id: this.state.curId
      }
    });
  }

  onChange(e) {
    this.setState({
      curId: Number(e.target.value)
    });
  }

  render() {
    const item = this.props.data;
    const color = item.diy_resource_color;
    const nearestColors = item.nearest_colors;

    return (
      <div>
        <div className='table'>
          <div className='row'>
            <span className='cell caption'>ID</span>
            <span>{ this.props.data.resource_file_data_id }</span>
          </div>

          <div className='row'>
            <span className='cell caption'>Color</span>
            <RenderSelect value={ item.diy_resource_id } checked={ item.diy_resource_id === this.state.curId }
                          color={ color } info={ item.no + ', ' + item.color_cnt + ' count ' }
                          add_info='(original)' onChange={ this.onChange } />
            {
              nearestColors &&
              nearestColors.map((elem, index) => {
                return (
                  <RenderSelect key={ index } value={ elem.diy_resource_id } checked={ elem.diy_resource_id === this.state.curId }
                                color={ elem.diy_resource_color } info={ elem.no + ', ' + elem.color_cnt + ' count ' }
                                add_info={ (elem.in_my_palette ? '(my palette)' : '(schema)') } onChange={ this.onChange } />
                );
              })
            }
            {
              this.state.message &&
              <div style={{ paddingTop: '15px' }} className='error'>
                <span>{ this.state.message }</span>
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

class RenderSelect extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    this.colorObjToTxt = this.colorObjToTxt.bind(this);
  }

  colorObjToTxt(obj) {
    return 'rgb(' + obj.R + ',' + obj.G + ',' + obj.B + ')';
  }

  render() {
    return (
      <label style={{ display: 'flex', paddingTop: '20px' }}>
        <input type='radio' name='next_color'
               value={ this.props.value } checked={ this.props.checked }
               onChange={ this.props.onChange }/>

        <div className='color-info' style={{ backgroundColor: this.colorObjToTxt(this.props.color) }} />
        <b style={{ paddingLeft: '5px' }}>{ this.props.info }</b>
        <span>{ this.props.add_info }</span>
      </label>
    );
  }
}


module.exports = ChooseSchemaNextColor;