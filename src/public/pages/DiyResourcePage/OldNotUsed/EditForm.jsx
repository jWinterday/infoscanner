const React = require('react');
//import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';

class EditForm extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    //console.info(props);

    const params = props.cell || {};



    this.state = {
      project_id: props.projectId,
      diy_resource_value: {
        value: params.diy_resource_id,
        label: params.no,
        color: params.color
      },
      amount_type_value: {
        value: params.amount_type_id,
        label: params.amount_type_name
      },
      add_info: params.add_info || '',
      used_in_project: params.used_in_project,
      diy_resource_in_project_id: params.diy_resource_in_project_id//id in db table
    };

    //console.info(this.state);

    this.getResourcesList = this.getResourcesList.bind(this);
    this.getAmountTypeList = this.getAmountTypeList.bind(this);

    this.doCreate = this.doCreate.bind(this);
    this.doUpdate = this.doUpdate.bind(this);

    this.handleAddInfoChange = this.handleAddInfoChange.bind(this);
    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleResourceChange = this.handleResourceChange.bind(this);

    this.renderArray = this.renderArray.bind(this);

    this.init();
  }

  init() {
    //create
    if (!this.state.diy_resource_in_project_id) {
      this.getAmountTypeList()
        .then(data => {
          const amountDefault = data.find(p => p.is_default === true);

          this.setState({
            amount_type_value: amountDefault
          });
        })
        .catch(e => {
          console.info(e);
        });
    }
  }

  renderArray(val) {
    let joinedStr;

    if (val) {
      joinedStr = '[' + val.join(', ') + ']';
    }

    return (
      <span>{ joinedStr }</span>
    );
  }

  doCreate() {
    //console.info(this.state);
    const params = {
      project_id: this.state.project_id,
      amount_type_value: this.state.amount_type_value,
      diy_resource_value: this.state.diy_resource_value,
      add_info: this.state.add_info
    };

    return this.props.ajaxWrapper({
      url: '/diy_resources/ajax/create',
      method: 'post',
      data: params
    });
  }

  doUpdate() {
    //console.info(this.state);
    const params = {
      project_id: this.state.project_id,
      version_id: this.state.version_id,
      diy_resource_in_project_id: this.state.diy_resource_in_project_id,
      amount_type_value: this.state.amount_type_value,
      diy_resource_value: this.state.diy_resource_value,
      add_info: this.state.add_info
    };

    return this.props.ajaxWrapper({
      url: '/diy_resources/ajax/update',
      method: 'post',
      data: params
    });
  }

  getResourcesList(inputValue) {
    const ajaxWrapper = this.props.ajaxWrapper;

    return new Promise((resolve, reject) => {
      ajaxWrapper({
        url: '/diy_resources/ajax/resources_list',
        method: 'post',
        data: {
          project_id: this.state.project_id,
          //diy_resource_value: this.state.diy_resource_value,
          val: inputValue
        }
      })
      .then(data => {
        resolve(data);
      });
    });
  }

  getAmountTypeList(inputValue) {
    const ajaxWrapper = this.props.ajaxWrapper;

    return new Promise((resolve, reject) => {
      ajaxWrapper({
        url: '/diy_resources/ajax/amount_type_list',
        method: 'post',
        data: { val: inputValue }
      })
      .then(data => {
        resolve(data);
      });
    });
  }

  handleResourceChange(e) {
    //console.info(e);
    this.setState({
      diy_resource_value: {
        value: e.value,
        label: e.label,
        color: e.color
      }
    });
  }

  handleAmountChange(e) {
    //console.info(e);
    this.setState({
      amount_type_value: {
        value: e.value,
        label: e.label
      }
    });
  }

  handleAddInfoChange(e) {
    this.setState({
      add_info: e.target.value
    });
  }

  render() {
    return (
      <div className='table'>
        <div className='row'>
          <span className='cell caption'>Resource</span>
          <AsyncSelect cacheOptions defaultOptions
                       value={ this.state.diy_resource_value }
                       onChange={ this.handleResourceChange }
                       loadOptions={ this.getResourcesList } />
        </div>
        <div className='row'>
          <span className='cell caption'>Amount Type</span>
          <AsyncSelect cacheOptions defaultOptions
                       value={ this.state.amount_type_value }
                       onChange={ this.handleAmountChange }
                       loadOptions={ this.getAmountTypeList } />
        </div>
        {
          this.state.used_in_project &&
          <div className='row'>
            <span className='cell caption'>Projects</span>
            <p>{ this.renderArray(this.state.used_in_project) }</p>
          </div>
        }
        <div className='row'>
          <span className='cell caption'>Add info</span>
          <input type='text' value={ this.state.add_info } onChange={ this.handleAddInfoChange } placeholder='add info'/>
        </div>
      </div>
    );
  }
};

/*const dot = (color = '#ccc') => ({
      alignItems: 'center',
      display: 'flex',

      ':before': {
        backgroundColor: color,
        borderRadius: 10,
        content: ' ',
        display: 'block',
        marginRight: 8,
        height: 10,
        width: 10,
      },
    });

    const colourStyles = {
      option: (base, state) => {
        let colorObj;
        if (state && state.data && state.data.color) {
          colorObj = 'RGB(' + state.data.color.r + ',' + state.data.color.g + ',' + state.data.color.b + ')';
        }

        const nextStyle = {
          //opacity: state.children === 'yellow' ? 0.5 : 1,
          backgroundColor: colorObj//state.children === 'yellow' ? 'yellow' : 'white',
          //color: 'black'//state.children === 'yellow' ? 'black' : 'e',
        }

        Object.assign(base, nextStyle);

        //console.info(base);
        //state.data.label += <div style={{backgroundColor: colorObj}}></div>;

        return base;
      },

      container   : (base, state) => {
       console.info(base);

       //base.zIndex = 999999;
        base.zIndex = 9999999;
        //base.backgroundColor = 'cyan';

       return base;
      },

      singleValue: (base, state) => {
        let colorObj;
        if (state && state.data && state.data.color) {
          colorObj = 'RGB(' + state.data.color.r + ',' + state.data.color.g + ',' + state.data.color.b + ')';
        }

        const nextStyle = {
          //opacity: state.children === 'yellow' ? 0.5 : 1,
          backgroundColor: colorObj//state.children === 'yellow' ? 'yellow' : 'white',
          //color: 'black'//state.children === 'yellow' ? 'black' : 'e',
        }

        Object.assign(base, nextStyle);

        //state.data.label += '   ggg';


        return base;
      }
    };*/

//console.info(this.state);

const dot = (color = '#ccc') => ({
  alignItems: 'center',
  display: 'flex',

  ':before': {
    backgroundColor: color,
    borderRadius: 10,
    content: ' ',
    display: 'block',
    marginRight: 8,
    height: 10,
    width: 10,
  },
});

const colourStyles = {
  option: (base, state) => {
    //console.info(state);

    //base.':active' = {backgroundColor: 'red'};
    /*let colorObj;
    if (state && state.data && state.data.color) {
      colorObj = 'RGB(' + state.data.color.r + ',' + state.data.color.g + ',' + state.data.color.b + ')';
    }

    const nextStyle = {
      //opacity: state.children === 'yellow' ? 0.5 : 1,
      backgroundColor: colorObj//state.children === 'yellow' ? 'yellow' : 'white',
      //color: 'black'//state.children === 'yellow' ? 'black' : 'e',
    }

    Object.assign(base, nextStyle);*/

    //console.info(base);
    //state.data.label += <div style={{backgroundColor: colorObj}}></div>;

    return base;
  },
  /*input: (base, state) => {
    //Object.assign(base, dot());
    base.alignItems = 'center';
    base.display = 'flex';
    base.backgroundColor = '#ccc';
    base.borderRadius = 10;
    //base.after.backgroundColor = 'red';

    console.info(state);

    return base;
  }*/
  //input: styles => ({ ...styles, ...dot() }),
  //placeholder: styles => ({ ...styles, ...dot() }),
  //singleValue: (styles, { data }) => ({ ...styles, ...dot(data.color) }),
};


module.exports = EditForm;