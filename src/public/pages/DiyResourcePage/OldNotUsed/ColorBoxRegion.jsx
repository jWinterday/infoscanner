import React from 'react';
import PropTypes from 'prop-types';

import ColorBoxRender from '../../../components/CommonView/ColorBoxRender';

const getItemCountMess = (item) => {
  return item.color_cnt === 0 ? 'Can not change color, while his length is 0' : ''
}

class ColorBoxRegion extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    console.info(props);

    this.state = {
      message: getItemCountMess(props.item),
      curNearestItem: null
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      message: getItemCountMess(nextProps.item),
      curNearestItem: null
    });
  }

  onClick = (item, index) => {
    return (e) => {
      this.setState({
        curNearestItem: item
      });
    }
  }

  applyColor = async (e) => {
    const params = {
      url: '/image_schema/ajax/set_schema_next_color',
      method: 'post',
      data: {
        resource_file_data_id: this.props.item.resource_file_data_id,
        next_resource_file_data_id: this.state.curNearestItem.resource_file_data_id
      }
    };

    try {
      await this.props.ajaxWrapper(params);
      this.setState({
        message: 'successfully changed current color'
      });

      this.props.onChangeColor();
    } catch(e) {
      this.setState({
        message: e.response.data
      });
    }
  }


  render() {
    return (
      <div style={{ border: '1px solid #dbcccc' }}>
        <div className='container-flex'>
          <input type='button' value='Hide' onClick={ this.props.hideColorBoxRegion } className='button'/>

        </div>

        {
          this.state.message &&
          <p className='error'>{ this.state.message }</p>
        }

        <div className='container-flex'>
          <div className='container-flex-with-border'  style={{ width: '50%' }}>
            <div className='table'>
              <div className='row'>
                <span className='cell caption'>Color</span>
                <ColorBoxRender item={ this.props.item } viewMode={ this.props.viewMode } />
              </div>
              <div className='row'>
                <span className='cell caption'>ID</span>
                <b>{ this.props.item.resource_file_data_id }</b>
              </div>
              <div className='row'>
                <span className='cell caption'>Name</span>
                <b>{ this.props.item.name }</b>
              </div>
              <div className='row'>
                <span className='cell caption'>No</span>
                <b>{ this.props.item.no }</b>
              </div>
              <div className='row'>
                <span className='cell caption'>Color count</span>
                <b>{ this.props.item.color_cnt }</b>
              </div>
              <div className='row'>
                <span className='cell caption'>Amount</span>
                <b>{ this.props.item.amount_type_name }</b>
              </div>
            </div>
          </div>

          <div className='container-flex-with-border' style={{ width: '50%' }}>
            <div className='table'>
              <div className='row'>
                <span className='cell caption'>Nearest</span>
                <div style={{display: 'flex'}}>
                  {
                    this.props.item.nearest_colors.map((item, index) => {
                      const curNearestItem = this.state.curNearestItem;
                      const isCurNearestItem = Boolean(curNearestItem) &&
                        (curNearestItem.diy_resource_id === item.diy_resource_id);
                      const outline = isCurNearestItem ? '3px solid red' : '';

                      return (
                        <ColorBoxRender key={ index } item={ item } viewMode={ this.props.viewMode }
                                        outline={ outline } onClick={ this.onClick } />
                      )
                    })
                  }
                </div>
              </div>

              {
                this.state.curNearestItem &&
                <div className='row'>
                  <span className='cell caption'>ID</span>
                  <b>{ this.state.curNearestItem.resource_file_data_id }</b>
                </div>
              }
              {
                this.state.curNearestItem &&
                <div className='row'>
                  <span className='cell caption'>Name</span>
                  <b>{ this.state.curNearestItem.name }</b>
                </div>
              }
              {
                this.state.curNearestItem &&
                <div className='row'>
                  <span className='cell caption'>No</span>
                  <b>{ this.state.curNearestItem.no }</b>
                </div>
              }
              {
                this.state.curNearestItem &&
                <div className='row'>
                  <span className='cell caption'>Color count</span>
                  <b>{ this.state.curNearestItem.color_cnt }</b>
                </div>
              }
              {
                this.state.curNearestItem &&
                <div className='row'>
                  <span className='cell caption'>Amount</span>
                  <b>{ this.state.curNearestItem.amount_type_name }</b>
                </div>
              }
              {
                this.state.curNearestItem && this.props.item.color_cnt > 0 &&
                <div className='row'>
                  <input type='button' value='Apply' onClick={ this.applyColor } />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ColorBoxRegion.defaultProps = {
  viewMode: 'none',
  onChangeColor: () => {}
};

ColorBoxRegion.propTypes = {
  item: PropTypes.object.isRequired,
  viewMode: PropTypes.string,
  ajaxWrapper: PropTypes.func,
  hideColorBoxRegion: PropTypes.func,
  projectId: PropTypes.number.isRequired,
  onChangeColor: PropTypes.func
  //onClick: PropTypes.func
};


module.exports = ColorBoxRegion;