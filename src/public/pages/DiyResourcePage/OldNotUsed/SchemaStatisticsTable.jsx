const React = require('react');
import ReactTable, { ReactTableDefaults } from 'react-table';
import Popup from 'react-popup';

//import Dropdown from '../../components/Dropdown/Dropdown';
import ChooseSchemaNextColor from './ChooseSchemaNextColor';
import '../../react-table.css';

class SchemaStatisticsTable extends React.Component {
  state = {}
  tableState = {};

  constructor(props) {
    super(props);

    Object.assign(ReactTableDefaults, {
      pageSizeOptions: [3, 5, 10, 20, 25, 50, 100],
      defaultPageSize: 25,
      manual: true,
      getTrProps: (state, rowInfo, column) => {
        let color;
        if (rowInfo) {
          color = rowInfo.row.amount_color;
        }

        return {
          style: {
            background: color
          }
        };
      },
      /*getTdProps: (state, rowInfo, column) => {
        let bgColor;

        if (column.id === 'diy_resource_color' && rowInfo) {

          try {
            const val = rowInfo.row.diy_resource_color;
            if (val) {
              bgColor = 'RGB(' + val.R + ',' + val.G + ',' + val.B + ')';
            }
          } catch(e) { }
        };

        return {
          style: {
            background: bgColor
          }
        };
      }*/
    });

    //caption
    const captions = [
      { Header: 'ID', accessor: 'resource_file_data_id', width: 100, Cell: (p) => <b className='number'>{ p.value }</b> },
      //{ Header: 'DIY resource ID', accessor: 'diy_resource_id', width: 130, Cell: (p) => <b className='number'>{ p.value }</b> },
      { Header: 'Name', accessor: 'name', width: 160 },
      { Header: 'No', accessor: 'no', width: 80 },
      { Header: 'Color count', accessor: 'color_cnt', width: 130 },
      { Header: 'Original Color', accessor: 'diy_resource_color', width: 100, Cell: (p) => this.renderColor(p, p.value, true) },
      { Header: 'Next Color', accessor: 'next_diy_resource_color', width: 100, Cell: (p) => this.renderColor(p, p.value, false) },
      { Header: 'Next No', accessor: 'next_no', width: 80 },
      { Header: 'Amount color', accessor: 'amount_color', width: 130, show: false },
      { Header: 'Amount', accessor: 'amount_type_name' }
    ];

    this.state = {
      message: '',
      columns: captions
    };

    this.tableState = {
      projectId: this.props.projectId
    };

    //ref
    this.ChooseSchemaColorRef = React.createRef();

    this.fetchData = this.fetchData.bind(this);
    this.dropDownClick = this.dropDownClick.bind(this);
    this.refreshTable = this.refreshTable.bind(this);
    this.removeSingle = this.removeSingle.bind(this);
  }

  renderColor(cell, color, isEditable) {
    //const color = cell.original.diy_resource_color;
    let colorTxt;
    if (color) {
      colorTxt = 'rgb(' + color.R + ',' + color.G + ',' + color.B + ')';
    }
    //const colorTxt = 'rgb(' + color.R + ',' + color.G + ',' + color.B + ')';
    return (
      <div>
        {
          isEditable &&
          <div className='pointer' style={{ display: 'flex', width: '100%' }} onClick={ this.dropDownClick(cell.original) }>
            <div style={{ backgroundColor: colorTxt, width: '100%' }} ></div>
            <img src='/images/arrow_down.min.png' title={ 'id: ' + cell.original.diy_resource_id }/>
          </div>
        }
        {
          !isEditable &&
          <div style={{ backgroundColor: colorTxt, height: '100%' }} ></div>
        }
      </div>
    );
  }

  dropDownClick(data) {
    return (e) => {
      Popup.create({
        title: 'Next color',
        content: <ChooseSchemaNextColor ref={ this.ChooseSchemaColorRef }
                                        data={ data }
                                        ajaxWrapper = { this.props.ctx.helpers.ajaxWrapper }/>,
        buttons: {
          left: [
            { text: 'Cancel', action: () => { Popup.close(); } },
            { text: 'Save',
              id: 'ctrl+enter',
              className: 'success',
              action: () => {
                this.ChooseSchemaColorRef.current.save()
                  .then(data => {
                    //console.info(data);
                    Popup.close();
                    this.refreshTable();
                  })
                  .catch(err => {
                    Popup.close();
                  });
              }
            }]
        }
      });
    };

  }

  refreshTable() {
    this.fetchData();//fire fetch data
  }

  fetchData(state=this.tableState, instance) {
    const { page, pageSize, sorted, filtered } = state;
    const params = { page, pageSize, sorted, filtered };

    Object.assign(this.tableState, params);

    const ajaxWrapper = this.props.ctx.helpers.ajaxWrapper;

    this.setState({
      loading: true
    });

    ajaxWrapper({
      url: '/image_schema/ajax/get_statistics',
      method: 'post',
      data: this.tableState
    })
    .then(data => {
      //console.info(data);
      this.setState({
        rows: data,
        pages: data.length > 0 ? data[0].pages : 1,
        loading: false
      });
    })
    .catch(err => {
      //console.info(err);
      this.setState({
        loading: false,
        message: err.response.status + ' ' + err.response.statusText
      });
    });
  }

  removeSingle(e) {

  }

  render() {
    return (
      <div>
        <div className='filter-container'>
          <div className='nav-buttons-container'>
            <span className='pointer nav-buttons' onClick={ this.refreshTable }>
              <img src='/images/refresh.min.png' title='refresh DIY resource list' />
            </span>
          </div>

          <fieldset>
            <span className='pointer nav-buttons' onClick={ this.refreshTable }>
              <img src='/images/refresh.min.png' title='refresh DIY resource list' />
              <span>Remove single</span>
            </span>
          </fieldset>

          <ReactTable style={{ paddingTop: '10px' }}
                      data={ this.state.rows } pages={ this.state.pages }
                      columns={ this.state.columns } loading={ this.state.loading }
                      onFetchData={ (state, instance) => this.fetchData(state, instance) }/>
        </div>
        <p className='message-info'>{ this.state.message }</p>
      </div>
    );
  }
}


module.exports = SchemaStatisticsTable;