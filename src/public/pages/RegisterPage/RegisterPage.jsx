import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import RegisterForm from './RegisterForm';


class RegisterPage extends React.Component {
  render() {
    return (
      <div>
        <fieldset>
          <legend>Register</legend>
          <Consumer>
            {ctx => {
              return (
                <RegisterForm ctx={ctx} />
              )
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
};


export default RegisterPage;