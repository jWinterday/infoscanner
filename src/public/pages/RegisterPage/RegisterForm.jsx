import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import EmailField from '../../components/Login/EmailField.jsx';
import PasswordField from '../../components/Login/PasswordField.jsx';
import FlexRow from '../../components/CommonView/FlexRow';


class RegisterForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: { value: 'jwinterday@mail.ru', valid: true },
      password: { value: 'R%5rty', valid: true },
      confirm_password: { value: 'R%5rty', valid: true },
      //email: { value: '', valid: false },
      //password: { value: '', valid: false },
      //confirm_password: { value: '', valid: false },
      message: null
    };
  }

  onFieldsChanged = (field, value) => {
    this.setState((prevState, props) => {
      prevState[field] = value;
      return prevState;
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const updateUserProfile = this.props.ctx.helpers.updateUserProfile;
    const params = {
      url: '/auth/ajax/register',
      method: 'post',
      data: {
        email: this.state.email.value,
        password: this.state.password.value,
        confirm_password: this.state.confirm_password.value
      }
    };

    try {
      const response = await axios(params);
      const data = response.data || {};
      if (data.success) {
        data.token && localStorage.setItem('token', data.token);
        data.refreshToken && localStorage.setItem('refreshToken', data.refreshToken);
      }
      this.setState({
        success: data.success,
        message: data.message
      });
      updateUserProfile();
    } catch (err) {
      let mess;
      if (err.response) {
        mess = err.response.status + ' ' + err.response.data;
      } else if (err.request) {
        mess = err.request;
      } else {
        mess = err.message;
      }

      this.setState({
        success: false,
        message: mess
      });
    }
  }

  render() {
    //Passwords do not match
    const infoColorClass = this.state.success ? 'ok' : 'error';
    const email = this.state.email;
    const password = this.state.password;
    const confirmPassword =  this.state.confirm_password;
    const submitDisabled = !(email.valid && password.valid && confirmPassword.valid);

    return (
      <div>
        <form method="post" onSubmit={this.handleSubmit}>
          <p>password must contains minimum six characters, at least one uppercase letter, one lowercase letter, one number and one special character</p>

          <FlexRow caption="Email" element={<EmailField value="jwinterday@mail.ru" onChange={this.onFieldsChanged} />} />
          <FlexRow caption="Password" element={<PasswordField value="R%5rty" name="password" onChange={ this.onFieldsChanged } />} />
          <FlexRow caption="Confirm password" element={<PasswordField value="R%5rty" name="confirm_password" onChange={this.onFieldsChanged }/>} />
          {
            this.state.message &&
            <p className={infoColorClass}>{this.state.message}</p>
          }

          <br/>
          <input type="submit" value="Register" disabled={submitDisabled} />
        </form>
      </div>
    );
  }
};

RegisterForm.propTypes = {
  ctx: PropTypes.object.isRequired
};


module.exports = RegisterForm;