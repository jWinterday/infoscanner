import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import ProjectTable from './ProjectTable';
import LoginPage from "../LoginPage/LoginPage";

class ProjectPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <fieldset>
          <legend>Projects</legend>
          <Consumer>
            {ctx => {
              return (
                <div>
                  <ProjectTable ctx={ ctx } />
                </div>
              );
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
};


export default ProjectPage;