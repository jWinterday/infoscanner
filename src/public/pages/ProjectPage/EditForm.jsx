const React = require('react');

class EditForm extends React.Component {
  state = {
    name: 'new project',
    note: ''
  };

  constructor(props) {
    super(props);

    const cell = props.cell;

    if (cell) {
      this.state = {
        name: cell.original.name,
        note: cell.original.note,
        active: cell.original.is_project_active,
        project_id: cell.original.project_id,
        begin_date: cell.original.begin_date,
        end_date: cell.original.end_date
      };
    }

    this.doCreate = this.doCreate.bind(this);
    this.doUpdate = this.doUpdate.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {//TODO autofocus...
      this.refs.name.focus();
      this.refs.name.select();
    }, 0)
  }

  doCreate() {
    const params = {
      name: this.refs.name.value,
      note: this.refs.note.value
    };

    return this.props.ajaxWrapper({
      url: '/project/ajax/create',
      method: 'post',
      data: params
    });
  }

  doUpdate() {
    const params = {
      project_id: this.state.project_id,
      name: this.refs.name.value,
      note: this.refs.note.value,
      active: this.refs.active.checked
    };

    return this.props.ajaxWrapper({
      url: '/project/ajax/update',
      method: 'post',
      data: params
    });
  }

  render() {
    return (
      <div className='table'>
        {
          this.state.project_id &&
          <div className='row'>
            <span className='cell caption'>ID</span>
            <p>{ this.state.project_id }</p>
          </div>
        }
        <div className='row'>
          <span className='cell caption'>Name</span>
          <input ref='name'
                 type='text'
                 defaultValue={ this.state.name }
                 placeholder='project name' />
        </div>
        <div className='row'>
          <span className='cell caption'>Note</span>
          <input ref='note'
                 type='text'
                 defaultValue={ this.state.note }
                 placeholder='note' />
        </div>
        {
          this.state.active !== undefined &&
          <div className='row'>
            <span className='cell caption'>Active</span>
            <input ref='active'
                   type='checkbox'
                   defaultChecked={ this.state.active }/>
          </div>
        }
        {
          this.state.begin_date &&
          <div className='row'>
            <span className='cell caption'>Begin Date</span>
            <p>{ this.state.begin_date }</p>
          </div>
        }
        {
          this.state.end_date &&
          <div className='row'>
            <span className='cell caption'>End Date</span>
            <p>{ this.state.end_date }</p>
          </div>
        }
      </div>
    );
  }
};

module.exports = EditForm;