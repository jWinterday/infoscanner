import Popup from "react-popup/dist/index";
import { Link } from 'react-router-dom';

const React = require('react');
import ReactTable, { ReactTableDefaults } from 'react-table';

import '../react-table.css';
import EditForm from './EditForm';

Object.assign(ReactTableDefaults, {
  pageSizeOptions: [3, 5, 10, 20, 25, 50],
  defaultPageSize: 5,
  manual: true
});

class ProjectTable extends React.Component {
  state = {};
  tableState = {};

  constructor(props) {
    super(props);

    //can edit/view
    const isCanEdit = this.props.ctx.profile.roles.includes('project_edit');

    //caption
    const captions = [
      { Header: '', accessor: 'project_id', width: 24, sortable: false, Cell: (p) => this.renderCanView(p) },
      { Header: 'Id', accessor: 'project_id', width: 40, Cell: (p) => <b className='number'>{ p.value }</b> },
      { Header: 'Name', accessor: 'name', width: 150 },
      { Header: 'Own', accessor: 'is_own_project', width: 40, Cell: (p) => this.renderCheckboxCell(p) },
      { Header: 'BeginDate', accessor: 'begin_date', width: 150 },
      //{ Header: 'EndDate', accessor: 'end_date', width: 150 },
      { Header: 'Active', accessor: 'is_project_active', width: 60, Cell: (p) => this.renderCheckboxCell(p) },
      { Header: 'Note', accessor: 'note' }
    ];

    if (isCanEdit) {
      captions.unshift({ Header: '', accessor: 'project_id', width: 24, sortable: false, Cell: (p) => this.renderCanEdit(p) });
    }

    this.state = {
      isCanEdit: isCanEdit,
      message: '',
      columns: captions
    };

    //ref
    this.editFormRef = React.createRef();

    //event
    this.refreshProject = this.refreshProject.bind(this);
    this.createProject = this.createProject.bind(this);
    this.doCreate = this.doCreate.bind(this);
    this.updateProject = this.updateProject.bind(this);
    this.doUpdate = this.doUpdate.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }

  //cell render
  renderWithTooltip(cell) {
    //<div className='tooltip'>
    return (
      <div>
        { cell.value }
        <span>{ cell.value }</span>
      </div>
    );
  }

  renderCanEdit(cell) {
    //console.info(cell);
    return (
      <div>
        {
          cell.original.is_own_project &&
          <span className='pointer' onClick={ this.updateProject(cell) }>
            <img src="/images/grid_edit.min.png" title={ 'edit ' + cell.original.name } />
          </span>
        }
      </div>
    );
  }

  renderCanView(cell) {
    return (
      <div>
        <Link to={ '/project/' + cell.original.project_id }>
          <img src="/images/view.min.png" title={ 'view ' + cell.original.name } />
        </Link>
      </div>
    );
  }

  renderCheckboxCell(cell) {
    return (
      <div>
        <input type='checkbox' checked={ cell.value } onChange={() => { return false; } } />
      </div>
    );
  };

  renderEditableCell(cell) {
    return (
      <div className='cell-container' contentEditable={ cell.original.is_editable } suppressContentEditableWarning
        onBlur={e => {
          const data = [...this.state.rows];
          const oldVal = data[cell.index][cell.column.id];
          const newVal = e.target.innerHTML;

          if (oldVal === newVal) { return; }

          data[cell.index][cell.column.id] = newVal;
          this.setState({ data });
          this.doUpdate(data[cell.index]);
        }}
        dangerouslySetInnerHTML={{
          __html: this.state.rows[cell.index][cell.column.id]
        }}
      />
    );
  };

  //data manipulating
  refreshProject() {
    this.fetchData(this.tableState);//fire fetch data
  }

  doCreate() {
    this.editFormRef.current.doCreate()
      .then(data => {
        this.fetchData(this.tableState);//fire fetch data

        this.setState({
          message: 'successfully created project ' + data[0].name + ' with id: ' + data[0].project_id
        });
        Popup.close();
      })
      .catch(err => {
        this.setState({
          message: err.status + ' ' + err.statusText
        });
        Popup.close();
      });
  }

  createProject() {
    Popup.create({
      title: 'Create project',
      content: <EditForm ref={ this.editFormRef } ajaxWrapper={ this.props.ctx.helpers.ajaxWrapper } />,
      buttons: {
        left: [
          { text: 'Cancel', action: () => { Popup.close(); } },
          { text: 'Save',
            id: 'ctrl+enter',
            className: 'success',
            action: () => {
              this.doCreate();
            }
          }]
      }
    });
  }

  doUpdate() {
    this.editFormRef.current.doUpdate()
      .then(data => {
        //console.info(data);
        this.fetchData(this.tableState);//fire fetch data

        this.setState({
          message: 'successfully updated project ' + data[0].name + ' with id: ' + data[0].project_id
        });
        Popup.close();
      })
      .catch(err => {
        this.setState({
          message: err.status + ' ' + err.statusText
        });
        Popup.close();
      });
  }

  updateProject(cell) {
    return () => {
      Popup.create({
        title: 'EditForm project',
        content: <EditForm ref={ this.editFormRef }
                           ajaxWrapper={ this.props.ctx.helpers.ajaxWrapper }
                           cell={ cell }/>,
        buttons: {
          left: [
            { text: 'Cancel', action: () => { Popup.close(); } },
            { text: 'Save',
              id: 'ctrl+enter',
              className: 'success',
              action: () => {
                this.doUpdate();
              }
            }]
        }
      });
    }
  }

  fetchData(state, instance) {
    const { page, pageSize, sorted, filtered } = state;
    const params = { page, pageSize, sorted, filtered };

    Object.assign(this.tableState, params);
    const ajaxWrapper = this.props.ctx.helpers.ajaxWrapper;
    this.setState({
      loading: true
    });

    ajaxWrapper({
      url: '/project/ajax/list',
      method: 'post',
      data: params
    })
    .then(data => {
      this.setState({
        rows: data.rows,
        pages: (data.hasOwnProperty('rows') && data.rows.length > 0) ? data.rows[0].pages : null,
        isEdtable: data.rows[0].is_editable,
        loading: false
      });
    })
    .catch(err => {
      //console.info(err);
      this.setState({
        loading: false,
        message: err.status + ' ' + err.statusText
      });
    });
  }

  render() {
    return (
      <div>
        <div className='nav-buttons-container'>
          <span className='pointer nav-buttons' onClick={ this.refreshProject }>
            <img src='/images/refresh.min.png' title='refresh projects list' />
          </span>
          {
            this.state.isCanEdit &&
            <div className='nav-buttons'>
              <span className='pointer' onClick={ this.createProject }>
                <img src='/images/plus.min.png' title='create new project' />
              </span>
            </div>
          }
        </div>
        <br />

        <ReactTable ref='project_table' data={ this.state.rows } pages={ this.state.pages }
                    columns={ this.state.columns } loading={ this.state.loading }
                    onFetchData={ (state, instance) => this.fetchData(state, instance) }/>
        <p className='message-info'>{ this.state.message }</p>
      </div>
    );
  }
};

module.exports = ProjectTable;