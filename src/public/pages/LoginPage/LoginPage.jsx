import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import LoginForm from './LoginForm';


class LoginPage extends React.Component {
  render() {
    return (
      <div>
        <fieldset>
          <legend>Login</legend>
          <Consumer>
            {ctx => {
              return (
                <LoginForm ctx={ctx}/>
              )
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
};


export default LoginPage;