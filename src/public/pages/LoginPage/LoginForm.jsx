import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import EmailField from '../../components/Login/EmailField';
import PasswordField from '../../components/Login/PasswordField';
import FlexRow from '../../components/CommonView/FlexRow';


class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: { value: 'jwinterday@mail.ru', valid: true },
      password: { value: 'R%5rty', valid: true },
      //email: { value: '', valid: false },
      //password: { value: '', valid: false },
      message: null
    };
  }

  onFieldsChanged = (field, state) => {
    this.setState((prevState, props) => {
      prevState[field] = state;
      return prevState;
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const updateUserProfile = this.props.ctx.helpers.updateUserProfile;
    const params = {
      url: '/auth/ajax/login',
      method: 'post',
      data: {
        email: this.state.email.value,
        password: this.state.password.value
      }
    };

    try {
      const response = await axios(params);
      const data = response.data || {};
      if (data.success) {
        data.token && localStorage.setItem('token', data.token);
        data.refreshToken && localStorage.setItem('refreshToken', data.refreshToken);
      }
      this.setState({
        success: data.success,
        message: data.message
      });
      updateUserProfile();
    } catch (err) {
      console.info('LoginForm. success: false. err: ', err);
      let mess;
      if (err.response) {
        mess = err.response.status + ' ' + err.response.data;
      } else if (err.request) {
        mess = err.request;
      } else {
        mess = err.message;
      }

      this.setState({
        success: false,
        message: mess
      });
    }
  }

  render() {
    const infoColorClass = this.state.success ? 'ok' : 'error';
    const submitDisabled = !(this.state.email.valid && this.state.password.valid);
    const email = this.state.email.value;
    const password = this.state.password.value;

    return (
      <div>
        <form method="post" onSubmit={this.handleSubmit}>
          <FlexRow caption="Email" element={<EmailField value={email} onChange={this.onFieldsChanged} />}/>
          <FlexRow caption="Password" element={<PasswordField value={password} onChange={this.onFieldsChanged} />}/>
          {
            this.state.message &&
            <p className={infoColorClass}>{this.state.message}</p>
          }
          <br />
          <input type="submit" value="Login" disabled={submitDisabled} />
        </form>

        <div>
          <a href = "/#register">Register</a>
          <a href = "/#remindpass" style = {{ paddingLeft: '10px' }}>
            Forgot password
          </a>
        </div>
      </div>
    );
  }
};

LoginForm.propTypes = {
  ctx: PropTypes.object.isRequired
};


export default LoginForm;