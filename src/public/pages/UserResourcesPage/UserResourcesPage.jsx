import React from 'react';

import Consumer from '../../components/Layout/ConfigProvider';
import DiyResourceTable from '../../components/DiyResourceTable/DiyResourceTable';


class UserResourcesPage extends React.Component {
  render() {
    return (
      <div>
        <fieldset>
          <legend>User Resources</legend>
          <Consumer>
            {ctx => {
              return (
                <DiyResourceTable ajaxWrapper={ ctx.helpers.ajaxWrapper } />
              );
            }}
          </Consumer>
        </fieldset>
      </div>
    );
  }
};


export default UserResourcesPage;