import React from 'react';


class UnauthorizedPage extends React.Component {
  render() {
    return (
      <div>
        <p>User not authorized</p>
        <p>401</p>
      </div>
    );
  }
}


export default UnauthorizedPage;