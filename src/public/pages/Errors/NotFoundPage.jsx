import React from 'react';


class NotFoundPage extends React.Component {
  render() {
    return (
      <div>
        <p>Page not found</p>
        <p>404</p>
      </div>
    );
  }
}


export default NotFoundPage;