const loadImageAsync = (data) => {
  const image = new window.Image();
  image.src = data;
  return new Promise((resolve, reject) => {
    image.onload = () => {
      resolve(image);
    };
    image.onerror = (err) => {
      reject(err);
    };
  });
}

const getColorStr = (obj={}) => {
  return `rgb(${obj.R}, ${obj.G}, ${obj.B})`;
}

const debounce = (fn, delay) => {
  let timeoutID = null;

  return () => {
    clearTimeout(timeoutID);

    timeoutID = setTimeout(() => {
      fn.apply(this, arguments)
    }, delay);
  }
}

export { debounce };

export default {
  loadImageAsync,
  getColorStr
};