import React from 'react';
import PropTypes from 'prop-types';
import { Rect, Group } from 'react-konva';

class SelectedColorBox extends React.PureComponent {
  getGroupDataRef = (groupData) => { this.groupData = groupData };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if ((this.props.data || []).length > 0) {
      this.groupData.cache();
    }
  }

  render() {
    const { width, height, borderWidth, data, offsetX, offsetY } = this.props;

    const w = Math.max((width - 2*borderWidth), 1);
    const h = Math.max((height - 2*borderWidth), 1);

    return (
      <Group ref={this.getGroupDataRef}>
        {
          (data || []).map((point, index) => {
            return (
              <Rect
                key={index}
                x={offsetX + point.x + borderWidth}
                y={offsetY + point.y + borderWidth}
                width={w}
                height={h}
                fill='red'
              />
            )
          })
        }
      </Group>
    );
  }
}


SelectedColorBox.defaultProps = {
  data: [],
  offsetX: 0,
  offsetY: 0,
  width: 5,
  height: 5,
  borderWidth: 1
};

SelectedColorBox.propTypes = {
  data: PropTypes.array,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  borderWidth: PropTypes.number,
};

export default SelectedColorBox;