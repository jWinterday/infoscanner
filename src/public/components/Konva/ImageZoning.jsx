import React from 'react';
import PropTypes from 'prop-types';
import { Group, Rect, Image, Text } from 'react-konva';


let isPaint = false;
let lastPointerPosition = {};
let nextPointerPosition = {};

class ImageZoning extends React.PureComponent {
  getImageRef = (image) => { this.image = image };
  getImageZoningRef = (zoneGroup) => { this.zoneGroup = zoneGroup };

  constructor(props) {
    super(props);

    //console.info(props);

    const canvas = document.createElement('canvas');
    canvas.width = props.width;
    canvas.height = props.height;
    const context = canvas.getContext('2d');

    this.state = {
      canvas,
      context,
      selectedRectangles: []
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.zoneGroup.cache();
  }

  onMouseDown = (e) => {
    if(!this.props.isCanZoomImage) {
      return false;
    }

    const node = e.target;
    isPaint = true;
    const { x, y } = node.getStage().getPointerPosition();
    lastPointerPosition = Object.assign({}, { x: (x>>0), y: (y>>0) });

    //this.zoneGroup.clearCache();
  }

  onMouseUp = (e) => {
    if(!this.props.isCanZoomImage) {
      return false;
    }

    const node = e.target;
    const layer = node.getLayer();
    const stage = node.getStage();
    //const { x, y } = stage.getPointerPosition();
    const imageGroup = stage.findOne('.imageGroup');
    const imagePosition = Object.assign({}, { x: (imageGroup.x()>>0), y: (imageGroup.y()>>0) });

    const old = {
      x: -imagePosition.x + lastPointerPosition.x,
      y: -imagePosition.y + lastPointerPosition.y
    };
    const next = {
      x: -imagePosition.x + nextPointerPosition.x,
      y: -imagePosition.y + nextPointerPosition.y
    };

    isPaint = false;

    const { context } = this.state;
    context.clearRect(
      this.image.x(),
      this.image.y(),
      this.image.width(),
      this.image.height()
    );

    layer.batchDraw();

    //get selected rectangles coordinates
    const { offsetX, offsetY, stepX, stepY, zoningCount, width, height } = this.props;

    const rectWidth = zoningCount * stepX;
    const rectHeight = zoningCount * stepY;
    const countX = Math.floor(width/(zoningCount * stepX));
    const countY = Math.floor(height/(zoningCount * stepY));

    const selectedRectangles = [];
    [...Array(countX * countY).keys()].map(index => {
      const origX = index%countX;
      const origY = Math.floor(index/countX);

      const x = offsetX + origX * zoningCount * stepX;
      const y = offsetY + origY * zoningCount * stepY;

      const isSelected = (
        x < Math.max(next.x, old.x)
        &&
        y < Math.max(next.y, old.y)
        &&
        x > Math.min(next.x, old.x) - rectWidth
        &&
        y > Math.min(next.y, old.y) - rectHeight
      );

      if (isSelected) {
        selectedRectangles.push({ x, y });
      }
    });

    //this.zoneGroup.cache();

    this.setState({
      selectedRectangles
    });

    if (selectedRectangles && selectedRectangles.length > 0) {
      const allX = selectedRectangles.map(item => { return item.x; });
      const allY = selectedRectangles.map(item => { return item.y; });

      const coordinates = {
        x0: Math.min( ...allX ),
        y0: Math.min( ...allY ),
        x1: Math.max( ...allX ) + rectWidth,
        y1: Math.max( ...allY ) + rectHeight
      };

      const isCorrect = Object.values(coordinates).filter(p => Number.isFinite(p) === false).length === 0;

      this.props.onFinishSelectArea(isCorrect ? coordinates : null);
    } else {
      this.props.onFinishSelectArea(null);
    }
  }

  onMouseMove = (e) => {
    if(!this.props.isCanZoomImage) { return false; }

    if (!isPaint) { return false; }

    const { context } = this.state;
    const node = e.target;
    const stage = node.getStage();
    const layer = node.getLayer();
    const { x, y } = stage.getPointerPosition();
    nextPointerPosition = Object.assign({}, { x: (x>>0), y: (y>>0) });
    const imageGroup = stage.findOne('.imageGroup');
    const imagePosition = Object.assign({}, { x: (imageGroup.x()>>0), y: (imageGroup.y()>>0) });

    //console.info('imagePosition: ', imagePosition);

    context.clearRect(
      this.image.x(),
      this.image.y(),
      this.image.width(),
      this.image.height()
    );

    context.beginPath();
    context.strokeStyle = this.props.cursorColor;
    context.lineWidth = 2;
    context.rect(
      -imagePosition.x + lastPointerPosition.x,
      -imagePosition.y + lastPointerPosition.y,
      nextPointerPosition.x - lastPointerPosition.x,
      nextPointerPosition.y - lastPointerPosition.y
    );
    context.closePath();

    context.stroke();
    //layer.draw();
    layer.batchDraw();
  }

  render() {
    const {
      isCanZoomImage,
      //isShowZoneOrders,
      offsetX,
      offsetY,
      stepX,
      stepY,
      zoningCount,
      width,
      height,
      strokeColor,
      selectedStrokeColor
    } = this.props;

    lastPointerPosition = {};
    nextPointerPosition = {};

    const rectWidth = zoningCount * stepX;
    const rectHeight = zoningCount * stepY;
    const countX = Math.floor(width/(zoningCount * stepX));
    const countY = Math.floor(height/(zoningCount * stepY));

    return (
      <Group
        onMouseDown={this.onMouseDown}
        onMouseUp={this.onMouseUp}
        onMouseMove={this.onMouseMove}
      >
        <Group ref={this.getImageZoningRef}>
          {/*all rectangles*/}
          {
            [...Array(countX * countY).keys()].map(index => {
              const origX = index%countX;
              const origY = Math.floor(index/countX);

              const x = offsetX + origX * zoningCount * stepX;
              const y = offsetY + origY * zoningCount * stepY;

              return (
                <Group key={`selected_zone_${index}`}>
                  <Rect
                    x={x}
                    y={y}
                    width={rectWidth}
                    height={rectHeight}
                    stroke={strokeColor}
                  />
                  {
                    (origX === 0 || origY === 0) &&//isShowZoneOrders &&
                    <Text
                      x={x - 15}
                      y={y - 15}
                      fontSize={15}
                      //fontFamily="Calibri"
                      fill="red"
                      text={`${origX === 0 ? origY : (origY === 0 ? origX : '')}`}
                    />
                  }
                </Group>
              )
            })
          }

          {/*selected rectangles*/}
          {
            isCanZoomImage &&
            <Group>
              {
                this.state.selectedRectangles.map((item, index) => {
                  return (
                    <Rect
                      key={`selectedZooming_${index}`}
                      x={item.x}
                      y={item.y}
                      width={rectWidth}
                      height={rectHeight}
                      stroke={selectedStrokeColor}
                    />
                  )
                })
              }
            </Group>
          }
        </Group>

        <Image
          image={this.state.canvas}
          ref={this.getImageRef}
          width={this.props.width}
          height={this.props.height}
        />
      </Group>
    );
  }
}


ImageZoning.defaultProps = {
  isCanZoomImage: false,
  //isShowZoneOrders: false,
  offsetX: 0,
  offsetY: 0,
  cursorColor: 'white',
  strokeColor: 'gray',
  selectedStrokeColor: 'red',
  onFinishSelectArea: () => {}
};

ImageZoning.propTypes = {
  isCanZoomImage: PropTypes.bool,
  //isShowZoneOrders: PropTypes.bool,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  stepX: PropTypes.number.isRequired,//compress
  stepY: PropTypes.number.isRequired,//compress
  zoningCount: PropTypes.number.isRequired,//count squares in rect(10 default)
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  cursorColor: PropTypes.string,
  strokeColor: PropTypes.string,
  selectedStrokeColor: PropTypes.string,
  onFinishSelectArea: PropTypes.func
};

export default ImageZoning;