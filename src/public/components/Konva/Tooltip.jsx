import React from 'react';
import PropTypes from 'prop-types';
import { Text, Tag, Label } from 'react-konva';

import util from '../../utils';

class Tooltip extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Label opacity={0.9}
             x={this.props.mousePos.x}
             y={this.props.mousePos.y}
             visible={this.props.visible}>
        <Tag fill="black"
             pointerDirection="left"
             pointerWidth={10}
             pointerHeight={10}
             lineJoin="round"
             shadowColor="black"
             shadowBlur={10}
             shadowOffset={10}
             shadowOpacity={0.9}
        />
        <Text text={this.props.text}
              fontFamily="Calibri"
              fontSize={18}
              padding={5}
              fill={this.props.fillTextColor}
        />
      </Label>
    );
  }
}


Tooltip.defaultProps = {
  mousePos: {},
  visible: false,
  text: '',
  fillTextColor: 'white'
};

Tooltip.propTypes = {
  mousePos: PropTypes.object,
  visible: PropTypes.bool,
  text: PropTypes.string,
  fillTextColor: PropTypes.string
};

export default Tooltip;