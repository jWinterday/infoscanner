import React from 'react';
import PropTypes from 'prop-types';
import { Rect, Image, Group } from 'react-konva';

import util from '../../utils';

const navigatorH = 30;

class MainColorPanel extends React.Component {
  constructor(props) {
    super(props);

    const {
      panelW,
      itemsInLine,
      itemsInColumn,
      totalColorBoxesCount,
      totalItemsCount,
      totalItemsPages,
      curItemsPage,
      downArrowAvailable,
      items
    } = this.getInitPageInfo(props);

    this.state = {
      upArrowImg: null,
      downArrowImg: null,
      inMyPaletteImg: null,
      upArrowAvailable: false,
      curSelectedColorBoxId: null,
      tooltip: '',


      panelW,
      itemsInLine,
      itemsInColumn,
      totalColorBoxesCount,
      totalItemsCount,
      totalItemsPages,
      curItemsPage,
      downArrowAvailable,
      items
    };
  }

  getInitPageInfo = (params) => {
    const colors = params.colors || [];

    const { itemsInLine, panelH, panelColorBoxMargin, panelColorBoxW } = params;
    const panelW = itemsInLine * (panelColorBoxMargin + panelColorBoxW) + panelColorBoxMargin;
    const itemsInColumn = Math.floor((panelH - 2 * navigatorH) / (panelColorBoxW + panelColorBoxMargin));
    const totalColorBoxesCount = itemsInLine * itemsInColumn;
    const totalItemsCount = colors.length;
    const totalItemsPages = Math.ceil(totalItemsCount / totalColorBoxesCount);
    const curItemsPage = 0;

    return {
      panelW,
      itemsInLine,
      itemsInColumn,
      totalColorBoxesCount,
      totalItemsCount,
      totalItemsPages,
      curItemsPage,
      downArrowAvailable: (totalColorBoxesCount <= totalItemsCount),
      items: colors.slice(curItemsPage, totalColorBoxesCount),
    };
  }

  componentDidMount = async () => {
    try {
      const upArrowImg = await util.loadImageAsync('./images/schema/arrow_up.min.png');
      const downArrowImg = await util.loadImageAsync('./images/schema/arrow_down.min.png');
      const inMyPaletteImg = await util.loadImageAsync('./images/schema/ok.min.png');
      this.setState({
        upArrowImg,
        downArrowImg,
        inMyPaletteImg: inMyPaletteImg
      });
    } catch (err) {
      //console.info(err);
    }
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      items: nextProps.colors
    });
  }

  /*componentWillReceiveProps = (nextProps) => {
    console.info('MainColorPanel. componentWillReceiveProps')
    const {
      panelW,
      itemsInLine,
      itemsInColumn,
      totalColorBoxesCount,
      totalItemsCount,
      totalItemsPages,
      curItemsPage,
      downArrowAvailable,
      items
    } = this.getInitPageInfo(nextProps);

    this.setState({
      panelW,
      itemsInLine,
      itemsInColumn,
      totalColorBoxesCount,
      totalItemsCount,
      totalItemsPages,
      curItemsPage,
      downArrowAvailable,
      items
    });
  }*/

  //shouldComponentUpdate(nextProps, nextState){
  //  console.log("shouldComponentUpdate(). nextProps: ", nextProps);
  //  return true;
  //}

  componentDidUpdate(){

  }

  onSliderOver = (e) => {
    const node = e.target;
    const name = node.getName();

    if (node) {
      const { totalColorBoxesCount, totalItemsCount, curItemsPage } = this.state;
      const nextCurItemsPage = (name === 'up') ? curItemsPage - 1 : curItemsPage + 1;
      const from = nextCurItemsPage * totalColorBoxesCount + 1;
      const to = (from + totalColorBoxesCount) > totalItemsCount ? totalItemsCount : (from + totalColorBoxesCount);

      this.props.onItemMouseOver({
        //name: node.getName(),
        tooltip: `${from} - ${to} / ${totalItemsCount}`,
        mousePos: node.getStage().getPointerPosition()
      });

      document.body.style.cursor = 'pointer';
    }
  }

  onSliderOut = (e) => {
    const node = e.target;

    this.props.onItemMouseOut({});

    document.body.style.cursor = 'default';
  }

  onColorBoxOver = (e) => {
    const node = e.target;
    const parent = node.parent;
    const parentName = parent.getName();

    if (parentName !== 'boxGroup') {
      return false;
    }

    const item = parent.getAttr('item');
    const inMyPalette = item.in_my_palette ? '\u2713' : '\u2A09';

    this.props.onItemMouseOver({
      tooltip: `${item.no} / ${item.name}\ncount: ${item.coordinates.length}\nin my palette: ${inMyPalette}`,
      mousePos: node.getStage().getPointerPosition()
    });

    document.body.style.cursor = 'pointer';
  }

  onColorBoxOut = (e) => {
    this.props.onItemMouseOut({});

    document.body.style.cursor = 'default';
  }

  onClick = (e) => {
    const node = e.target;
    const name = node.getName();
    const parent = node.parent;

    if (['up', 'down'].includes(name)) {
      //slider
      const { totalColorBoxesCount, totalItemsCount, curItemsPage } = this.state;
      const nextCurItemsPage = (name === 'up') ? curItemsPage - 1 : curItemsPage + 1;
      const from = nextCurItemsPage * totalColorBoxesCount;
      const to = (from + totalColorBoxesCount) > totalItemsCount ? totalItemsCount : (from + totalColorBoxesCount);

      this.setState({
        upArrowAvailable : (nextCurItemsPage > 0),
        downArrowAvailable: (to < totalItemsCount),
        curItemsPage: nextCurItemsPage,
        items: this.props.colors.slice(from, to)
      });
    } else if (parent.getName() === 'boxGroup') {
      //color box
      const item = parent.getAttr('item');
      this.setState({
        curSelectedColorBoxId: item.diy_resource_id
      });

      this.props.onColorBoxClick(item.diy_resource_id);
    }
  }

  render() {
    const {
      itemsInLine,
      panelMarginX,
      panelMarginY,
      panelH,
      panelColorBoxMargin,
      panelColorBoxW,
      colorBoxWidth,
      visible
    } = this.props;

    const { panelW } = this.state;

    if (!panelW) {
      return <Group />;
    }

    return (
      <Group name="board"
             visible={visible}
             onMouseOver={this.onColorBoxOver}
             onMouseOut={this.onColorBoxOut}
             onClick={this.onClick}>
        <Rect
          x={panelMarginX}
          y={panelMarginY}
          width={panelW}
          height={panelH}
          stroke="black"
          fill="#636844"
        />

        {/*navigator*/}
        <Group name="slider" opacity={1}>
          <Image
            name="up"
            image={this.state.upArrowImg}
            x={this.state.panelW/2 + panelColorBoxMargin - 12}
            y={panelColorBoxMargin + panelMarginY}
            width={30}
            height={navigatorH - panelColorBoxMargin}
            visible={this.state.upArrowAvailable}
            onMouseOver={this.onSliderOver}
            onMouseOut={this.onSliderOut}
          />
          <Image
            name="down"
            image={this.state.downArrowImg}
            filters={[Konva.Filters.Brighten]}
            Brighten={200}
            x={this.state.panelW/2 + panelColorBoxMargin - 12}
            y={panelH - navigatorH + panelColorBoxMargin + panelMarginY}
            width={30}
            height={navigatorH - panelColorBoxMargin}
            visible={this.state.downArrowAvailable}
            onMouseOver={this.onSliderOver}
            onMouseOut={this.onSliderOut}
          />
        </Group>

        {
          (this.state.items || []).map((item, index) => {
            const curX = (panelColorBoxW + panelColorBoxMargin) * (index%itemsInLine) + (panelColorBoxMargin + panelMarginX);
            const curY = (panelColorBoxW + panelColorBoxMargin) * Math.floor(index / itemsInLine) + (panelColorBoxMargin + panelMarginY + navigatorH);

            return (
              <Group key={item.diy_resource_id} name="boxGroup" item={item}>
                <Rect
                  x={curX}
                  y={curY}
                  width={panelColorBoxW}
                  height={panelColorBoxW}
                  stroke={this.state.curSelectedColorBoxId === item.diy_resource_id ? 'red' : 'black'}
                  fill={util.getColorStr(item.color)}
                  //tension={0.1}
                  //fillLinearGradientStartPoint={{ x: -50, y: -50 }}
                  //fillLinearGradientEndPoint={{ x: 50, y: 50 }}
                  //fillLinearGradientColorStops={[0, 'red', 1, 'black']}
                />

                <Image
                  image={this.state.inMyPaletteImg}
                  x={curX}
                  y={curY}
                  //x={Math.round(curX + panelMarginX + panelColorBoxW/2 - colorBoxWidth/2)}
                  //y={Math.round(curY + panelMarginY + panelColorBoxW/2 - colorBoxWidth/2)}
                  width={colorBoxWidth}
                  height={colorBoxWidth}
                  visible={item.in_my_palette}
                />
              </Group>
            );
          })
        }
      </Group>
    );
  }
}


MainColorPanel.defaultProps = {
  colorBoxWidth: 20,
  colors: [],
  panelMarginX: 5,
  panelMarginY: 5,
  itemsInLine: 2,
  panelH: 500,
  panelColorBoxMargin: 3,
  panelColorBoxW: 55,
  visible: true,
  onItemMouseOver: () => {},
  onItemMouseOut: () => {},
  onColorBoxClick: () => {}
};

MainColorPanel.propTypes = {
  colorBoxWidth: PropTypes.number,
  colors: PropTypes.array,
  panelMarginX: PropTypes.number,
  panelMarginY: PropTypes.number,
  itemsInLine: PropTypes.number,
  panelH: PropTypes.number,
  panelColorBoxMargin: PropTypes.number,
  panelColorBoxW: PropTypes.number,
  visible: PropTypes.bool,
  onItemMouseOver: PropTypes.func,
  onItemMouseOut: PropTypes.func,
  onColorBoxClick: PropTypes.func
};

export default MainColorPanel;