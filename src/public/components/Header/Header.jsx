import React from 'react';
import { NavLink } from 'react-router-dom';

import Consumer from '../Layout/ConfigProvider';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Consumer>
        {ctx => {
          const isProjectView = ctx.userLoggedIn && ((ctx.profile || {}).roles || []).includes('project_view');

          return (
            <header>
              <div className='link-container'>
                <div className='link'>
                  <NavLink exact to='/' activeClassName='current-link'>Home</NavLink>
                </div>
                <div className='link'>
                  <NavLink to='/login' activeClassName='current-link'>Login</NavLink>
                </div>
                <div className='link'>
                  <NavLink to='/register' activeClassName='current-link'>Register</NavLink>
                </div>

                {
                  isProjectView &&
                  <div className='link'>
                    <NavLink to='/user_resources' activeClassName='current-link'>User Resource</NavLink>
                  </div>
                }
                {
                  isProjectView &&
                  <div className='link'>
                    <NavLink to='/project' activeClassName='current-link'>Project</NavLink>
                  </div>
                }
              </div>
            </header>
          );
        }}
      </Consumer>
    );
  }
}

module.exports = Header;