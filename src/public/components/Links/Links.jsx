import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Consumer from '../Layout/ConfigProvider';

import IndexPage from '../../pages/IndexPage/IndexPage';
import LoginPage from '../../pages/LoginPage/LoginPage';
import UserInfoPage from '../../pages/UserInfoPage/UserInfoPage';
import RegisterPage from '../../pages/RegisterPage/RegisterPage';
import ProjectPage from '../../pages/ProjectPage/ProjectPage';
import DiyResourcePage from'../../pages/DiyResourcePage/DiyResourcePage';
import UserResourcesPage from '../../pages/UserResourcesPage/UserResourcesPage';
import NotFoundPage from '../../pages/Errors/NotFoundPage';
import UnauthorizedPage from '../../pages/Errors/UnauthorizedPage';


class Links extends React.Component {
  render() {
    return (
      <Consumer>
        {ctx => {
          const isProjectView = ctx.userLoggedIn && ((ctx.profile || {}).roles || []).includes('project_view');

          return (
            <Switch>
              <Route exact path="/" component={IndexPage} />
              <Route exact path="/login" component={LoginPage} />
              <Route exact path="/userinfo" component={UserInfoPage} />
              <Route exact path="/register" component={RegisterPage} />

              <Route exact path="/user_resources" component={isProjectView && UserResourcesPage || UnauthorizedPage} />
              <Route exact path="/project" component={isProjectView && ProjectPage || UnauthorizedPage} />
              <Route exact path="/project/:id(\d+)" component={isProjectView && DiyResourcePage || UnauthorizedPage} />

              <Route component={NotFoundPage} />
              {/*<Redirect to="/login"/>*/}
            </Switch>
          );
        }}
      </Consumer>
    );
  }
}

module.exports = Links;