import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./SimpleSelectStyle.min.css') }`;
insertCss(csjs.getCss(styles));

class SimpleSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentId: this.props.defaultId
    };
  }

  onChange = (e) => {
    const val = e.target.value;
    this.setState({
      currentId: val
    });

    this.props.onChange(val);
  }

  render() {
    return (
      <select value={this.state.currentId} onChange={this.onChange}>
        {
          this.props.data.map((item, index) => {
            const id = item[this.props.id];
            const name = item[this.props.name];

            return (
              <option key={id} value={id}>
                {name}
              </option>

            )
          })
        }
      </select>
    );
  }
}


SimpleSelect.defaultProps = {
  onChange: () => {},
  //items: [],
  data: [],
  defaultId: '',
  id: 'id',
  name: 'name',
  note: 'note'
};

SimpleSelect.propTypes = {
  onChange: PropTypes.func,
  //items: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  defaultId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  id: PropTypes.string,
  name: PropTypes.string,
  note: PropTypes.string
};

export default SimpleSelect;