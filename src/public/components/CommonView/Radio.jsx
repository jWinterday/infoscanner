import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./RadioStyle.min.css') }`;
insertCss(csjs.getCss(styles));

class Radio extends React.Component {
  constructor(props) {
    super(props);

    //try detect default value
    //const defItem = props.data.filter(p => p.hasOwnProperty('is_default') && p.is_default === true)[0];
    //this.props.onChange(defItem[this.props.id]);

    this.state = {
      currentId: this.props.defaultId// ? this.props.defaultId : defItem[this.props.id]
    };
  }

  onChange = (e) => {
    const val = e.target.value;
    this.setState({
      currentId: val
    });

    this.props.onChange(val);
  }

  render() {
    return (
      <div>
        {
          this.props.items.map((item, index) => {
            const id = item[this.props.id];
            const name = item[this.props.name];

            return (
              <label key={id} className={styles.radio}>
                <input name={this.props.radioGroupName}
                       type="radio"
                       value={id}
                       checked={id == this.state.currentId}
                       onChange={this.onChange} />
                {name}
              </label>
            )
          })
        }
      </div>
    );
  }
}


Radio.defaultProps = {
  onChange: () => {},
  radioGroupName: 'radio_item',
  items: [],
  defaultId: null,
  id: 'amount_type_id',
  name: 'name'
};

Radio.propTypes = {
  onChange: PropTypes.func,
  radioGroupName: PropTypes.string,
  items: PropTypes.array,
  defaultId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  id: PropTypes.string,
  name: PropTypes.string
};

export default Radio;