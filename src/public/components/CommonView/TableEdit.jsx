import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import csjs from 'csjs';
import insertCss from 'insert-css';

import Consumer from '../Layout/ConfigProvider';
import EditForm from '../DiyResourceTable/Components/EditForm';
const styles = csjs`${ require('./TableEditStyle.min.css') }`;
insertCss(csjs.getCss(styles));

class TableEdit extends React.Component {
  constructor(props) {
    super(props);

    this.editFormRef = React.createRef();
  }

  onEditClick = (e) => {
    Popup.create({
      title: 'EditForm',
      content:
        <Consumer>
          {ctx => {
            return (
              <EditForm ref={ this.editFormRef } cell={this.props.cell} ajaxWrapper={ctx.helpers.ajaxWrapper}/>
            )
          }}
        </Consumer>,
      buttons: {
        left: [
          { text: 'Exit', action: async () => { Popup.close(); } },
          {
            text: 'Save',
            id: 'ctrl+enter',
            className: 'success',
            action: async () => {
              try {
                await this.editFormRef.current.saveAmount();
                Popup.close();
                this.props.onAfterSave();
              } catch(err) { }
            }
          }]
      }
    });
  }

  render() {
    return (
      <span className={styles.editButton} onClick={this.onEditClick}>
        <img src="/images/grid_edit.min.png" />
      </span>
    );
  }
}


TableEdit.defaultProps = {
  onClick: () => { },
  onAfterSave: () => { }
};

TableEdit.propTypes = {
  onClick: PropTypes.func,
  onAfterSave: PropTypes.func
};


export default TableEdit;