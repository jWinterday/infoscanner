const React = require('react');
import PropTypes from 'prop-types';

const buildRgbText = (obj) => {
  return 'rgb(' + obj.R + ',' + obj.G + ',' + obj.B + ')';
}

class ColorBoxRender extends React.Component {
  state = {};

  constructor(props) {
    super(props);
  }

  render() {
    const item = this.props.item;
    const mode = this.props.viewMode;
    const index = this.props.index;
    const color = item ? buildRgbText(item.diy_resource_color) : '#eee';
    const outline = this.props.outline;

    const style = {
      backgroundColor: color,
      outline: outline
    };
    const addNextClass = (mode === 'none') ? 'palette-item-compact' : 'palette-item';
    const isAddNext = this.props.isAddNext;

    return (
      <div>
        {
          !isAddNext && (mode === 'none') &&
          <div className='pointer palette-item-compact' style={style} onClick={ this.props.onClick(item, index) } >
            <div className='palette-item-info-color'>
              {
                (item.in_my_palette || this.props.text) &&
                <b>{ this.props.text || '\u2713' }</b>
              }
            </div>
          </div>
        }

        {
          isAddNext &&
          <div className={ addNextClass + ' pointer' } style={style} onClick={ this.props.onClick } >
            <img className='palette-item-info' src='/images/plus.min.png'/>
          </div>
        }

        {
          !isAddNext && (mode === 'no' || mode === 'cnt') &&
          <div className='pointer palette-item'
               style={style}
               onClick={ this.props.onClick(item, index) } >
            <div className='palette-item-info-color'>
              {
                mode === 'no' &&
                <span>{ item.no }</span>
              }
              {
                mode === 'cnt' &&
                <span>{ item.color_cnt }</span>
              }
              <b>{ item.in_my_palette ? '\u2713' : '' }</b>
            </div>
          </div>
        }
      </div>
    );
  }
}

ColorBoxRender.defaultProps = {
  viewMode: 'none',
  isAddNext: false,
  outline: '',
  onClick: () => { return (e) => {} },
  text: null
};

ColorBoxRender.propTypes = {
  isAddNext: PropTypes.bool,
  item: PropTypes.object.isRequired,
  viewMode: PropTypes.string,
  index: PropTypes.number,
  outline: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string
};


module.exports = ColorBoxRender;