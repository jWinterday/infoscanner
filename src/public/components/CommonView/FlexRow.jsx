import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';

const styles = csjs`${ require('./FlexRowStyle.min.css') }`;
insertCss(csjs.getCss(styles));


class FlexRow extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value
    };
  }

  onChange = (e) => {
    const value = e.target.value;
    this.setState({
      value: value
    });
    this.props.onChange(value);
  }

  render() {
    let editFieldClass;
    switch (this.props.editViewMode) {
      case 'ok':
        editFieldClass = styles.okEditField;
        break;
      case 'bad':
        editFieldClass = styles.notOkEditField;
        break;
      default:
        editFieldClass = styles.neutralEditField;
        break;
    }

    return (
      <div className='row'>
        <div className='cell caption'>{this.props.caption}</div>
        {
          !this.props.editable && !this.props.element &&
          <div>
            <p>{this.props.value}</p>
            {
              this.props.hint &&
              <label className={styles.hint}>{this.props.hint}</label>
            }
          </div>
        }
        {
          this.props.editable && !this.props.element &&
          <div>
            <input type='text'
                   value={this.state.value}
                   placeholder='type value'
                   onChange={this.onChange}
                   onKeyPress={this.props.onKeyPress}
                   className={editFieldClass} />
            {
              this.props.hint &&
              <label className={styles.hint}>{this.props.hint}</label>
            }
            {
              this.props.isLoading &&
              <img src='/images/loading.min.png' className={styles.loading} alt='loading' />
            }
          </div>
        }
        {
          this.props.element &&
          React.cloneElement(this.props.element, {
            data: this.props.data,
          })
        }
      </div>
    );
  }
}


FlexRow.defaultProps = {
  caption: '',
  editable: false,
  onChange: () => {},
  onKeyPress: () => {},
  element: null,
  value: '',
  data: [],
  editViewMode: 'neutral',
  hint: null,
  isLoading: false
};

FlexRow.propTypes = {
  caption: PropTypes.string,
  editable: PropTypes.bool,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  element: PropTypes.element,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  data: PropTypes.array,
  editViewMode: PropTypes.oneOf(['neutral', 'ok', 'bad']),
  hint: PropTypes.string,
  isLoading: PropTypes.bool
};


export default FlexRow;