import React from 'react';
import PropTypes from 'prop-types';


const TableColor = ({userClass, text, color}) => (
  <div className={userClass} style={{backgroundColor: `rgb(${color.R},${color.G},${color.B})`}} >
    {text}
  </div>
);


TableColor.defaultProps = {
  userClass: '',
  text: '',
  color: { }
};
TableColor.propTypes = {
  userClass: PropTypes.object,
  text: PropTypes.string,
  color: PropTypes.object
};


export default TableColor;