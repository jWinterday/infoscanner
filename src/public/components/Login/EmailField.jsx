import React from 'react';
import PropTypes from 'prop-types';

const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class EmailField extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isChanged: false,
      value: props.value,
      valid: this._validate(props.value)
    };
  }
  
  _validate = (val) => {
    if (!val)
      return false;

    return pattern.test(val);
  }
  
  onChange = (e) => {
    const val = e.target.value;
    const name = e.target.name;

    this.setState((prevState, props) => {
      const nextState = {
        isChanged: true,
        value: val,
        valid: this._validate(val)
      };
      this.props.onChange(name, nextState);
      return nextState;
    });
  }
  
  render() {
    const inputClass = this.state.valid ? 'editableOk' : 'editableError';

    return (
      <div>
        <input type="text"
               name="email"
               value={this.state.value}
               onChange={this.onChange}
               className={inputClass}
        />
        {
          (this.state.isChanged && !this.state.valid) &&
          <label htmlFor="email" className="error">
            Wrong E-mail format
          </label>
        }
      </div>
    );
  };
};


EmailField.defaultProps = {
  value: '',
  onChange: () => {}
};

EmailField.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
};

module.exports = EmailField;