import React from 'react';
import PropTypes from 'prop-types';

class PasswordField extends React.PureComponent {
  constructor(props) {
    super(props);
    
    this.state = {
      isChanged: false,
      value: props.value,
      valid: this._validate(props.value)
    };
  }
  
  _validate(val) {
    return Boolean(val);
  }
  
  onChange = (e) => {
    const val = e.target.value;
    const name = e.target.name;
    
    const isValid = this._validate(val);
    
    this.setState((prevState, props) => {
      const curState = {
        isChanged: true,
        value: val,
        valid: isValid
      };
      this.props.onChange(name, curState);
      return curState;
    });
  }
  
  render() {
    const inputClass = this.state.valid ? 'editableOk' : 'editableError';

    return (
      <div>
        <input type="password"
               name={this.props.name}
               value={this.state.value}
               onChange={this.onChange}
               className={inputClass}
        />
        {
          (this.state.isChanged && !this.state.valid) &&
          <label htmlFor={this.props.name} className="error">
            Enter password
          </label>
        }
      </div>
    );
  };
};

PasswordField.defaultProps = {
  value: '',
  name: 'password',
  onChange: () => {}
};

PasswordField.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func
};

module.exports = PasswordField;