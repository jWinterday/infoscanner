const React = require('react');

class Dropdown extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    this.dropDownClick = this.dropDownClick.bind(this);
    this.colorObjToTxt = this.colorObjToTxt.bind(this);
    this.selectNextColor = this.selectNextColor.bind(this);
  }

  dropDownClick(e) {
    console.info(e);
  }

  /*dropDownClick(e) {
    //console.info(e.clientX, ', ', e.clientY);
    const element = e.target;

    Popup.registerPlugin('popover', function (content, target) {
      this.create({
        content: content,
        className: 'popover',
        //noOverlay: true,
        position: function (box) {
          let bodyRect = document.body.getBoundingClientRect();
          let btnRect = target.getBoundingClientRect();
          let btnOffsetTop = btnRect.top - bodyRect.top;
          let btnOffsetLeft = btnRect.left - bodyRect.left;
          let scroll = document.documentElement.scrollTop || document.body.scrollTop;

          box.style.top  = (btnOffsetTop - box.offsetHeight - 10) - scroll + 'px';
          box.style.left = (btnOffsetLeft + (target.offsetWidth / 2) - (box.offsetWidth / 2)) + 'px';
          box.style.margin = 0;
          box.style.opacity = 1;
        }
      });
    });

    Popup.plugins().popover(<div>'This popup will be displayed right above this button.'</div>, element);
  }*/

  selectNextColor(e) {
    const ajaxWrapper = this.props.ajaxWrapper;

    ajaxWrapper({
      url: '/image_schema/ajax/set_schema_next_color',
      method: 'post',
      data: {
        resource_file_data_id: this.props.resource_file_data_id,
        diy_resource_id: e.target.value
      }
    })
    .then(data => {
      console.info(data[0]);
      this.props.onChange();
    })
    .catch(err => {
      console.info(err);
    });
  }

  colorObjToTxt(obj) {
    return 'rgb(' + obj.R + ',' + obj.G + ',' + obj.B + ')';
  }

  render() {
    //<optgroup label='my palette'>
    const color = this.props.color;
    const colorTxt = this.colorObjToTxt(color);// style={{ backgroundColor: colorTxt }}
    const nearestColors = this.props.nearest_colors;
    console.info(nearestColors);

    return (
      <div>
        <select className='select-arrow' style={{ backgroundColor: colorTxt }}>
          <option value={ this.props.id } style={{ backgroundColor: colorTxt }}>{ this.props.diy_resource_id }</option>
          {
            nearestColors.map((item, index) => {
              const colorTxt = this.colorObjToTxt(item.diy_resource_color);
              const id = item.diy_resource_id;
              const no = item.no;
              return (
                <optgroup key={ index } label={ item.in_my_palette ? 'in my palette' : 'in scheme' }>
                  <option value={ id } style={{ backgroundColor: colorTxt }} onClick={ this.selectNextColor }>{ id }</option>
                </optgroup>
              )
            })
          }
        </select>
      </div>
    );
  }
};

module.exports = Dropdown;