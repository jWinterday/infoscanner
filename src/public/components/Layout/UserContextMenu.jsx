import React from 'react';
//import Popup from "react-popup/dist/index";

import Consumer from './ConfigProvider';

class UserContextMenu extends React.Component {
  state = {};

  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className='user-content-menu'>
        <img src="/info-logo.min.png" width='22px' height='22px' />
        <a href='#/' className='header-logo'>InfoScanner</a>
        <Consumer>
          {ctx => {
            return (
              <div className='user-info'>
                <UserInfo profile = { ctx.profile }
                          logged = { ctx.userLoggedIn } />
              </div>
            );
          }}
        </Consumer>
      </div>
    );
  }
};

class UserInfo extends React.Component {
  state = {};

  constructor(props) {
    super(props);

    this.state = {
      logged: props.logged,
      name: this.getUserFullName(props)
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      logged: nextProps.logged,
      name: this.getUserFullName(nextProps)
    });
  }

  getUserFullName(props) {
    let name = 'not logged';

    if (props && props.logged && props.profile) {
      name = props.profile.full_name;
    }
    return name;
  }

  render () {
    return (
      <div style = {{ float: 'right' }}>
        <u>
          <a style={{ paddingRight: '5px', color: '#1f7a9e' }}>{ this.state.name }</a>
          { !this.state.logged && <a href="#login" style={{ paddingRight: '5px' }}>login</a> }
          { !this.state.logged && <a href="#register">register</a> }
          { this.state.logged && <a href = "#userinfo">user info</a> }
        </u>
      </div>
    );
  }
};


module.exports = UserContextMenu;