import React, { Component, createContext } from 'react';
import axios from 'axios';

const { Provider, Consumer } = createContext();


class ConfigProvider extends Component {
  constructor(props) {
    super(props);

    const userProfile = this._parseToken();
    this.state = {
      success: true,
      userLoggedIn: Boolean(userProfile),
      profile: userProfile,
      userLogout: this.userLogout,
      userLogin: this.userLogin
    };
  }

  updateUserProfile = (err) => {
    const profile = this._parseToken();

    this.setState({
      userLoggedIn: Boolean(profile),
      profile: profile
    });

    return err;
  }

  _parseToken(token=localStorage.getItem('token')) {
    let profile;
    
    if (token) {
      try {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        profile = JSON.parse(window.atob(base64));
      } catch(e) {
        profile = null;
      }
    }
    
    return profile;
  }

  //ajax request wrapper
  //return promise that tries to request with token and if fail with 401 status tries request with refresh token
  ajaxWrapper = (ajaxParams) => {
    return new Promise(async (resolve, reject) => {
      const currentTime = new Date().getTime() / 1000;
      let token = localStorage.getItem('token');
      const profile = this._parseToken(token) || {};

      //check valid token
      if (!token || currentTime > (profile.exp || 0)) {
        try {
          const data = await this._getRefreshTokenAsync();
          token = (data || {}).token;
        } catch (err) {
          return reject(this.updateUserProfile(err));
        }
      }

      if (!token) {
        return reject(this.updateUserProfile('invalid token'));
      }

      //get data
      const headers = ajaxParams.headers || {};
      const nextHeaders = Object.assign(headers, { Authorization: 'Bearer ' + token });
      ajaxParams.headers = nextHeaders;
      try {
        const response = await axios(ajaxParams);
        resolve(response.data);
      } catch (err) {
        let mess;
        if (err.response) {
          mess = err.response.status + ' ' + err.response.data;
        } else if (err.request) {
          mess = err.request;
        } else {
          mess = err.message;
        }

        //if((err.response || {}).status === 401) { }
        return reject(this.updateUserProfile(mess));
      }
    });
  }


  _getRefreshTokenAsync = () => {
    return new Promise(async (resolve, reject) => {
      const params = {
        url: '/auth/ajax/refreshtoken',
        method: 'post'
      };

      const currentTime = new Date().getTime() / 1000;
      const refreshToken = localStorage.getItem('refreshToken');
      const profile = this._parseToken(refreshToken);

      if (!refreshToken || currentTime > profile.exp) {
        localStorage.removeItem('token');
        localStorage.removeItem('refreshToken');
        return reject('invalid refresh token');
      }

      params.headers = { Authorization: 'Bearer ' + refreshToken };

      try {
        let response = await axios(params);
        const data = (response || {}).data || {};
        if (data.success) {
          localStorage.setItem('token', data.token);
          localStorage.setItem('refreshToken', data.refreshToken);
          return resolve(data);
        } else {
          localStorage.removeItem('token');
          localStorage.removeItem('refreshToken');
          return reject('error on getting refresh token');
        }
      } catch (err) {
        reject(err);
      }
    });
  }

  render() {
    const params = {
      helpers: {
        updateUserProfile: this.updateUserProfile,
        ajaxWrapper: this.ajaxWrapper
      },
      profile: this.state.profile,
      userLoggedIn: this.state.userLoggedIn,
      success: this.state.success,
      message: this.state.message
    };

    return (
      <Provider value={params}>
        { this.props.children }
      </Provider>
    );
  }
};

export { ConfigProvider };
export default Consumer;