import React from 'react';
import Popup from 'react-popup';

const Header = require('../Header/Header');
const Links = require('../Links/Links');
const Footer = require('../Footer/Footer');
const UserContextMenu = require('./UserContextMenu');
const { ConfigProvider } = require('./ConfigProvider');

class App extends React.Component {
  state = {};
  
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <ConfigProvider>
        <div className='header'>
          <UserContextMenu />
        </div>
        <Header />
        <Links />
        <div className='footer' >
          <Footer />
        </div>
        <Popup />
      </ConfigProvider>
    );
  }
};

module.exports = App;