import React from 'react';
import PropTypes from 'prop-types';


class AsyncComponent extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      resolvedError: false,
      resolvedSuccess: false,
      resolvedNoData: false,
      data: '',
      error: '',
    };
  }

  async componentDidMount() {
    try {
      const result = await this.props.promise();
      const data = Array.isArray(result) ? result : (result.hasOwnProperty('rows') ? result.rows : result);
      this.setState({
        resolvedSuccess: true,
        resolvedNoData: Array.isArray(result) && data.length === 0,
        data: data
      });
    } catch(err) {
      this.setState({
        resolvedError: true,
        error: err
      })
    }
  }

  renderChildren = () => {
    return React.Children.map(this.props.children, child => (
      child && React.cloneElement(child, {
        data: this.state.data,
      })
    ));
  }

  render() {
    if (this.state.resolvedError) {
      return <span>Error Encountered</span>;
    } else if (this.state.resolvedSuccess) {
      if (this.state.resolvedNoData) {
        return '';
      }
      return <div>{ this.renderChildren() }</div>;
    } else {
      return <span>Loading...</span>;
    }
  }
}

AsyncComponent.propTypes = {
  promise: PropTypes.func.isRequired
};


export default AsyncComponent;