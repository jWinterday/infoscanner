import React from 'react';
import PropTypes from 'prop-types';

import CacheUtil from '../../../CacheUtil';
import FlexRow from '../../CommonView/FlexRow';
import ColorBoxRender from '../../CommonView/ColorBoxRender';
import Radio from '../../CommonView/Radio';
import AsyncComponent from '../../AsyncComponent/AsyncComponent';

const defaultAmountId = 3;

class AddForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      amountId: defaultAmountId,
      note: '',
      foundItem: null,
      numberEditViewMode: 'bad',
      isLoading: false,
      message: null
    };

    this.numberFieldRef = React.createRef();
  }

  getAmountTypes = () => {
    return new Promise(async (resolve, reject) => {
      try {
        let data = CacheUtil.get('amountTypes');
        if (!data) {
          data = await this.props.ajaxWrapper({
            url: '/diy_resources/ajax/get_amount_types',
            method: 'post'
          });
          CacheUtil.set('amountTypes', data);
        }
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  }

  onNumberChange = async (val) => {
    let data = CacheUtil.get('allDmc');//array
    const params = {
      url: '/diy_resources/ajax/get_all_dmc',
      method: 'post'
    };
    if (!data) {
      this.setState({ isLoading: true });
      try {
        data = await this.props.ajaxWrapper(params);
        CacheUtil.set('allDmc', data);//in memory
      } catch(err) {

      } finally {
        this.setState({ isLoading: false });
      }
    }
    const items = data.filter(p => p.no.toUpperCase() === val.toUpperCase());

    this.setState({
      numberEditViewMode: items.length === 0 ? 'bad' : 'ok',
      foundItem: items[0]
    });
  }

  onNumberKeyPress = async (e) => {
    if (e.which === 13) {
      this.save();
    }
  }

  save = async () => {
    const params = {
      url: '/diy_resources/ajax/add_user_resource',
      method: 'post',
      data: {
        amount_type_id: this.state.amountId,
        diy_resource_id: this.state.foundItem.diy_resource_id
      }
    };

    try {
      const data = await this.props.ajaxWrapper(params);
      this.numberFieldRef.current.clear();
      this.setState({
        foundItem: null,
        message: `successfully added color no: ${data.no} with ID: ${data.users_diy_resources_id}`
      });
    } catch(err) {
      this.setState({
        message: err.response.data
      });
    }
  }

  render() {
    return (
      <AsyncComponent promise={this.getAmountTypes}>
        <FlexRow ref={this.numberFieldRef}
                 caption='No'
                 editViewMode={this.state.numberEditViewMode}
                 onChange={this.onNumberChange}
                 onKeyPress={this.onNumberKeyPress}
                 hint='*Press Enter for add'
                 isLoading={this.state.isLoading}
                 editable
        />
        <FlexRow caption='Note' editViewMode='neutral' onChange={val => this.setState({note: val})} editable />
        <FlexRow caption='Amount' element={<Radio defaultId={defaultAmountId} onChange={val => this.setState({amountId: val})} />} />
        {
          this.state.foundItem &&
          <FlexRow caption='Color' element={<ColorBoxRender item={this.state.foundItem} />} />
        }
        {
          this.state.foundItem &&
          <FlexRow caption='Name' value={this.state.foundItem.name} />
        }
        {
          this.state.message &&
          <p className='message-info'>{this.state.message}</p>
        }
      </AsyncComponent>
    );
  }
}


AddForm.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired
};


export default AddForm;