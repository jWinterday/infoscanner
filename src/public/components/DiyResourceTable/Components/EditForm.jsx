import React from 'react';
import PropTypes from 'prop-types';

import FlexRow from '../../CommonView/FlexRow';
import ColorBoxRender from '../../CommonView/ColorBoxRender';
import Radio from '../../CommonView/Radio';
import AsyncComponent from '../../AsyncComponent/AsyncComponent';

class EditForm extends React.Component {
  constructor(props) {
    super(props);

    const cell = props.cell.original;
    this.state = {
      radioValue: cell.amount_type_id
    };
  }

  getAmountTypes = () => {
    return this.props.ajaxWrapper({
      url: '/diy_resources/ajax/get_amount_types',
      method: 'post'
    });
  }

  onRadioChange = (radioValue) => {
    this.setState({
      radioValue: radioValue
    });
  }

  saveAmount = () => {
    const cell = this.props.cell.original;

    return this.props.ajaxWrapper({
      url: '/diy_resources/ajax/update_amount_type',
      method: 'post',
      data: {
        diy_resource_id: cell.diy_resource_id,
        amount_type_id: this.state.radioValue,
        users_diy_resources_id: cell.users_diy_resources_id
      }
    })
  }

  render() {
    const cell = this.props.cell.original;

    return (
      <AsyncComponent promise={this.getAmountTypes}>
        <FlexRow caption='Color' element={<ColorBoxRender item={cell}/>} />
        <FlexRow caption='ID' value={cell.users_diy_resources_id} />
        <FlexRow caption='No' value={cell.no} />
        <FlexRow caption='Name' value={cell.diy_resource_name} />
        <FlexRow caption='Amount' element={<Radio defaultId={cell.amount_type_id} onChange={this.onRadioChange}/>} />
      </AsyncComponent>
    );
  }
}


EditForm.defaultProps = {
  cell: {}
};

EditForm.propTypes = {
  cell: PropTypes.object,
  ajaxWrapper: PropTypes.func.isRequired
};


export default EditForm;