import React from 'react';
import PropTypes from 'prop-types';
import Popup from 'react-popup';
import csjs from 'csjs';
import insertCss from 'insert-css';

import Consumer from '../../Layout/ConfigProvider';
import AddForm from './AddForm';
const styles = csjs`${ require('./TableMenuStyle.min.css') }`;
insertCss(csjs.getCss(styles));

class TableMenu extends React.Component {
  constructor(props) {
    super(props);

    this.addFormRef = React.createRef();
  }

  onCreate = () => {
    Popup.create({
      title: 'AddForm',
      content:
        <Consumer>
          {ctx => {
            return (
              <AddForm ref={this.addFormRef} ajaxWrapper={ctx.helpers.ajaxWrapper} />
            )
          }}
        </Consumer>,
      buttons: {
        left: [
          { text: 'Exit', action: async () => { Popup.close(); } },
          {
            text: 'Add',
            id: 'ctrl+enter',
            className: 'success',
            action: async () => {
              //Popup.close();

              this.addFormRef.current.save();

              /*try {
                await this.addFormRef.current.saveAmount();
                Popup.close();
                this.props.onAfterSave();
              } catch(err) { }*/
            }
          }]
      }
    });

    //this.props.onCreate();
  }

  render() {
    return (
      <div className={styles.menuContainer}>
        <span className={styles.menuElement} onClick={this.props.onRefresh}>
          <img src='/images/refresh.min.png' title='refresh' />
        </span>

        <span className={styles.menuElement} onClick={this.onCreate}>
          <img src='/images/plus.min.png' title='create' />
        </span>
      </div>
    );
  }
}

TableMenu.defaultProps = {
  onRefresh: () => { }
  //onCreate: () => { }
};
TableMenu.propTypes = {
  onRefresh: PropTypes.func
  //onCreate: PropTypes.func
};


export default TableMenu;