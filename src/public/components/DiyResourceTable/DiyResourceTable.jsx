import React from 'react';
import PropTypes from 'prop-types';
import csjs from 'csjs';
import insertCss from 'insert-css';
import ReactTable, { ReactTableDefaults } from 'react-table';

import TableEdit from '../CommonView/TableEdit';
import TableColor from '../CommonView/TableColor';
import TableMenu from './Components/TableMenu';


import '../react-table.css';
const styles = csjs`${ require('./Style.min.css') }`;
insertCss(csjs.getCss(styles));

class DiyResourceTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      rows: [],
      pages: 0,
      message: '',
      isShowAll: (localStorage.getItem('usersResource.showAll') === 'true') || false
    };

    Object.assign(ReactTableDefaults, {
      pageSizeOptions: [5, 10, 20, 30, 50, 100, 200, 300, 500],
      defaultPageSize: Number(localStorage.getItem('usersResource.pageSize')) || 10,// 10,
      manual: true,
      getTrProps: (state, rowInfo, column) => {
        let color;
        if (rowInfo) {
          color = rowInfo.original.amount_color;
          color = color ? `rgb(${color.R},${color.G},${color.B})`: null;
        }

        return {
          style: {
            background: color
          }
        };
      }
    });
  }

  columns = [
    { Header: '', accessor: 'diy_resource_id', width: 24, sortable: false , Cell: (p) => <TableEdit cell={p} onAfterSave={this.reloadTable} /> },
    { Header: 'ID', accessor: 'users_diy_resources_id', width: 50 },
    { Header: 'Name', accessor: 'diy_resource_name', width: 150 },
    { Header: 'No', accessor: 'no', width: 100 },
    { Header: 'Type', accessor: 'resource_type_name', width: 80 },
    { Header: 'Amount', accessor: 'amount_type_name', width: 80},
    { Header: 'Resource Color', accessor: 'color', Cell: (p) => <TableColor userClass={styles.tableColor} color={p.original.diy_resource_color} /> }
  ];

  fetchData = async (state, instance) => {
    const { page, pageSize, sorted, filtered } = state;
    localStorage.setItem('usersResource.pageSize', pageSize);

    //this.setState({
    //  loading: true
    //});

    const query = {
      url: '/diy_resources/ajax/list',
      method: 'post',
      data: {
        page: page+1,
        pageSize: pageSize,
        isAll: this.state.isShowAll
      }
    };

    try {
      const data = await this.props.ajaxWrapper(query);
      this.setState({
        rows: data.rows,
        pages: data.rows[0] ? data.rows[0].pages : 0
      });
    } catch(err) {
      console.info('table err: ', err);
      this.setState({
        message: err
      });
    }// finally {
     // this.setState({
     //   loading: false
     // });
    //}
  }

  reloadTable = () => {
    this.table.fireFetchData();
  }

  isShowAllChange = (e) => {
    const isShowAll = e.target.checked;

    this.setState({
      isShowAll: isShowAll
    }, () => {
      localStorage.setItem('usersResource.showAll', isShowAll);
      this.reloadTable();
    });
  }

  render() {
    return (
      <div>
        <TableMenu onRefresh={this.reloadTable}/>
        <label className={styles.checkboxContainer}>
          <input className={styles.checkbox} type='checkbox' checked={this.state.isShowAll} onChange={this.isShowAllChange}/>
          show all palette
        </label>
        {
          this.state.message &&
          <p className="message-info">{this.state.message}</p>
        }
        <ReactTable ref={(table) => {this.table = table;}}
                    data={this.state.rows}
                    pages={this.state.pages}
                    columns={this.columns}
                    loading={this.state.loading}
                    onFetchData={(state, instance) => this.fetchData(state, instance)}
        />
      </div>
    );
  }
}

DiyResourceTable.propTypes = {
  ajaxWrapper: PropTypes.func.isRequired
};


export default DiyResourceTable