const React = require('react');

class Footer extends React.Component {
  render () {
    return (
      <footer className='footer-content' >
        <div>jwd, { new Date().getFullYear() }</div>
      </footer>
    );
  }
};

module.exports = Footer;