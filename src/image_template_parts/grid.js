const drawGrid = () => {
  ctx.beginPath();
  ctx.fillStyle = cGridColor;
  //ctx.setLineDash([1, 1]);
  ctx.font = coeff / 3 + 'pt Arial';//CrossStitch3'
  ctx.strokeStyle = cGridColor;

  let i = 0;
  //let middle = 0;
  for (let x = 0; x < imageWidth; x += coeff * gridRowCount) {
    ctx.moveTo(x + padding, 0);
    ctx.lineTo(x + padding, imageHeight);
    ctx.fillText(i, x, coeff*0.4);
    i += gridRowCount;

    /*if (middle%2 === 0) {
      ctx.moveTo(x + padding, 0);
      ctx.lineTo(x + padding, imageHeight);
      ctx.fillText(i, x, coeff*0.4);
      i += gridRowCount;
    } else {
      ctx.moveTo(x + padding, coeff * gridRowCount/2 + padding - coeff);
      ctx.lineTo(x + padding, coeff * gridRowCount/2 + padding + coeff);
    }*/

    //middle++;
  }

  i = 0;
  //middle = 0;
  for (let y = 0; y < imageHeight; y += coeff * gridRowCount) {
    ctx.moveTo(0, y + padding);
    ctx.lineTo(imageWidth, y + padding);
    if (y !== 0) {
      ctx.fillText(i, 0, y + padding * 0.8);
    }
    i += gridRowCount;

    /*if (middle%2 === 0) {
      ctx.moveTo(0, y + padding);
      ctx.lineTo(imageWidth, y + padding);
      if (y !== 0) {
        ctx.fillText(i, 0, y + padding * 0.8);
      }
      i += gridRowCount;
    } else {
      ctx.moveTo(coeff * gridRowCount/2 + padding - coeff, y + padding);
      ctx.lineTo(coeff * gridRowCount/2 + padding + coeff, y + padding);
    }*/

    //middle++;
  }

  ctx.stroke();
  ctx.closePath();
};