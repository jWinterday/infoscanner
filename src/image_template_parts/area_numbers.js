const drawAreaNumber = () => {
  ctx.beginPath();
  ctx.fillStyle = cGridColor;
  ctx.font = coeff + 'pt Arial';
  //ctx.strokeStyle = cGridColor;

  //imgData
  let i=1;
  for (let y = 0; y < cntY/gridRowCount; y++) {
    for (let x = 0; x < cntX/gridRowCount; x++) {
      ctx.fillText(i, x*coeff*gridRowCount + gridRowCount*coeff/3 + padding, y*coeff*gridRowCount + gridRowCount*coeff*0.8);
      i++;
    }
  }

  ctx.stroke();
  ctx.closePath();
};