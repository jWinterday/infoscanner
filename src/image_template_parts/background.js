const drawBackground = () => {
  ctx.beginPath();
  ctx.rect(0, 0, imageWidth + padding, imageHeight + padding);
  ctx.fillStyle = cBackgroundColor;
  ctx.fill();
  ctx.closePath();
};