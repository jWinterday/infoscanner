//checkboxes, inputs
Array.of($showGrid, $showSchemaColor, $showSchemaData, $showGridAreaNumber,
         $gridColor, $schemaDataColor, $schemaDataGridColor, $backgroundColor).forEach(function (elem) {
  elem.addEventListener('change', function () {
    //console.info('changed', this, this.value);
    render();
  });
});

//zoom
$imageContainer.addEventListener('wheel', (e) => {
  e.preventDefault();

  if (e.deltaY > 0 && zoom > 1) {
    zoom--;
    render();
  }

  if (e.deltaY < 0 && zoom < 10) {
    zoom++;
    render();
  }
});

//image moving by mouse
let curYPos = 0;
let curXPos = 0;
let curDown = false;
window.addEventListener('mousemove', (e) => {
  if(curDown === true) {
    window.scrollTo(document.body.scrollLeft + (curXPos - e.pageX), document.body.scrollTop + (curYPos - e.pageY));
  }
});

window.addEventListener('mousedown', function (e) {
  curDown = true;
  curYPos = e.pageY;
  curXPos = e.pageX;
});

window.addEventListener('mouseup', function (e) {
  curDown = false;
});