//const uniqRes = !{JSON.stringify(uniqueDiyResourceObj)};
const drawSchemaData = () => {
  ctx.beginPath();
  ctx.fillStyle = cSchemaDataColor;
  ctx.font = coeff * 5 / 6 + 'pt CrossStitch3';
  ctx.strokeStyle = cSchemaDataGridColor;

  const symbolsTxt = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¤¥¦§¨©ª«®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùœŠšŸúûüýþÿ';
  const schemaSymbols = symbolsTxt.split('');

  for (let i = 0; i < pData.length; i++) {
    const item = pData[i];
    //const indexInSchemaSymbos = uniqRes[item.id].uniqCnt
    ctx.rect(item.x * zoom + padding, item.y * zoom + padding, coeff, coeff);
    ctx.fillText(schemaSymbols[item.order], item.x * zoom + padding, item.y * zoom + coeff * 5 / 8 + padding);
    //ctx.fillText(schemaSymbols[indexInSchemaSymbos], item.x * zoom + padding, item.y * zoom + coeff * 5 / 8 + padding);
  }

  ctx.stroke();
  ctx.closePath();
};