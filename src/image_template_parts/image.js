const drawImage = () => {
  return new Promise(resolve => {
    const convertedImage = new Image();
    convertedImage.src = pImageData.imageFileName;
    convertedImage.onload = () => {
      ctx.drawImage(convertedImage, padding, padding, imageWidth, imageHeight);
      resolve();
    };
  });
};