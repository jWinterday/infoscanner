const reqwest = require('reqwest');


window.onload = () => {
  console.info('login.js');
  document.getElementById('login_form').onsubmit = (e) => {
    let params = {};
    const elements = document.getElementById("login_form").elements;
    
    Array.from(elements).forEach((data) => {
      params[data.name] = data.value;
    });
    
    const $formInfo = document.getElementById('form_info');
    
    reqwest({
      url: '/login/ajax/login',
      method: 'post',
      data: params
      //headers: {
      //  'token': localStorage.getItem('token')
      //}
    })
    .then((data) => {
      if (data.success) {
        localStorage.setItem('token', data['token']);
        localStorage.setItem('refreshToken', data['refreshToken']);
        $formInfo.text = data.message;
        //window.location.href = data.redirect || '/';
      } else {
        $formInfo.text = data.message;
      }
    })
    .fail((err, msg) => {
      $formInfo.text = err.status + ', ' + err.responseText;
    });
    
    return false;
  };
};